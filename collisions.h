#ifndef _COLLISIONS_H_
#define _COLLISIONS_H_

#include "game.h"


void
Apply_tilemap_collision(Physics *phy);

bool
Point_on_collisionable_tile(Vec2 point);

bool
Is_collisionable(u32 tile_x, u32 tile_y);


#endif
