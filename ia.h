#ifndef _IA_H_
#define _IA_H_

#include "game.h"

void
Simple_enemy_update(u32 e_idx);

void
Smart_enemy_update(u32 e_idx);

u32
Get_number_of_patrols();

enum {
    IA_SPAWN,
    IA_PATROL,
    IA_RELOCATE
} IAState;

#endif
