#ifndef SCENE_IN_GAME_MENU_H
#define SCENE_IN_GAME_MENU_H

typedef enum {
	IN_GAME_MENU_OPTION_CONTINUE,
    IN_GAME_MENU_OPTION_BACK_TO_MENU,
    IN_GAME_MENU_OPTION_COUNT,
} InGameMenuOption;

void
Scene_in_game_menu();


#endif // SCENE_IN_GAME_MENU_H
