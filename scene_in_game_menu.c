#include "scene_in_game_menu.h"

void
Scene_in_game_menu() {
	Render_gameplay(0.0f);

	{
		f32 rect_w = GAMEPLAY_WIDTH;
		f32 rect_h = GAMEPLAY_HEIGHT * 1.0f;
		f32 rect_x = (GAMEPLAY_WIDTH-rect_w) * 0.5f;
		f32 rect_y = (GAMEPLAY_HEIGHT-rect_h) * 0.5f;
		Draw_rect(HMM_Vec3(rect_x, rect_y, 0.0f), rect_w, rect_h, HMM_Vec4(0.0f, 0.0f, 0.0f, 0.9f));
		Flush_triangles();
	}

	if (Show_menu(
			GLOBALS.in_game_menu_options,
			IN_GAME_MENU_OPTION_COUNT,
			GLOBALS.in_game_menu_selected_option,
			&GLOBALS.in_game_menu_selected_option)) {

		if (GLOBALS.in_game_menu_selected_option == IN_GAME_MENU_OPTION_CONTINUE) {
			GLOBALS.current_scene = SCENE_GAME;
			Mixer_resume_track(&GLOBALS.gameplay_music_handle);
		}
		else if (GLOBALS.in_game_menu_selected_option == IN_GAME_MENU_OPTION_BACK_TO_MENU) {
			GLOBALS.current_scene = SCENE_MAIN_MENU;
			GLOBALS.current_level = START_LEVEL;
			Init_level(&GLOBALS.levels[START_LEVEL]);
			Mixer_remove_track(&GLOBALS.intro_music_handle);
			Mixer_remove_track(&GLOBALS.gameplay_music_handle);
			Mixer_play_sound(GLOBALS.intro_music, PLAY_SOUND_FLAG_LOOP, MAX_VOLUME, &GLOBALS.intro_music_handle);
		}

		GLOBALS.in_game_menu_selected_option = IN_GAME_MENU_OPTION_CONTINUE;
	}
}



