#include "game.h"


void
Scene_main_menu() {

	Render_gameplay(0.0f);
	
	{ // Draws a black rectangle on the 75% on the screen
		f32 rect_w = GAMEPLAY_WIDTH;
		f32 rect_h = GAMEPLAY_HEIGHT * 0.75;
		f32 rect_x = (GAMEPLAY_WIDTH-GAMEPLAY_WIDTH) * 0.5f;
		f32 rect_y = GAMEPLAY_HEIGHT-rect_h;
		Draw_rect(
			HMM_Vec3(rect_x, rect_y, 0.0f),
			rect_w,
			rect_h,
			HMM_Vec4(0.0f, 0.0f, 0.0f, 1.0f));
		Flush_triangles();
	}

	{// Draws the title
		const char text[] = GAME_TITLE;
		f32 text_h = 18.0f;
		f32 text_w = Get_text_width_with_height(&GLOBALS.font, text, text_h);
		f32 text_x = (GAMEPLAY_WIDTH - text_w) * 0.5f;
		f32 text_y = (GAMEPLAY_HEIGHT) * 0.85f;

		Draw_text_with_height(&GLOBALS.font, text, text_h, text_x+1.0f, text_y-1.0f, 1.0f, 0.0f, 0.0f, 1.0f);
		Draw_text_with_height(&GLOBALS.font, text, text_h, text_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);

	}

	if (Show_menu(
			GLOBALS.main_menu_options,
			MAIN_MENU_OPTION_COUNT,
			GLOBALS.main_menu_selected_option,
			&GLOBALS.main_menu_selected_option)) {

		if (GLOBALS.main_menu_selected_option == MAIN_MENU_OPTION_PLAY) {
			GLOBALS.current_scene = SCENE_GAME;
			Mixer_remove_track(&GLOBALS.intro_music_handle);
		}
		else if (GLOBALS.main_menu_selected_option == MAIN_MENU_OPTION_CREDITS) {
			GLOBALS.current_scene = SCENE_CREDITS;
		}
        else if (GLOBALS.main_menu_selected_option == MAIN_MENU_OPTION_HELP) {
			GLOBALS.current_scene = SCENE_HELP;
        }
		else if (GLOBALS.main_menu_selected_option == MAIN_MENU_OPTION_EXIT) {
#if !defined(PLATFORM_WASM)
			GLOBALS.quit = true;
#endif
		}
	}
}

