#ifndef MAIN_MENU_H
#define MAIN_MENU_H

typedef enum {
	MAIN_MENU_OPTION_PLAY,
    MAIN_MENU_OPTION_HELP,
	MAIN_MENU_OPTION_CREDITS,
	MAIN_MENU_OPTION_EXIT,
	MAIN_MENU_OPTION_COUNT,
} MainMenuOption;

void
Scene_main_menu();


#endif

