#ifndef PARTICLES_H
#define PARTICLES_H

#include "engine/madmath.h"

typedef struct {
	f32 source_min_height;
	f32 source_max_height;
	f32 source_min_width;
	f32 source_max_width;
	f32 min_vel;
	f32 max_vel;
	f32 min_size;
	f32 max_size;

	f32 start_angle;
	f32 end_angle;

	f32 min_spawn_radius;
	f32 max_spawn_radius;
	f32 min_spawn_rate;
	f32 max_spawn_rate;
	f32 particle_max_lifetime;
	f32 particle_min_lifetime;

	Vec4 color_0;
	Vec4 color_1;

	Vec2 global_accel;
	f32  global_max_vel;

	f32 fade_in_time;
	f32 fade_out_time;

	u32 max_particles;
} ParticlesDesc;


typedef struct {
	const ParticlesDesc *particles_desc;
	f32  time;
	Vec2 pos;
	u64  seed;
} ParticlesSource;


typedef enum {
	SHOW_PARTICLES_REGENERATE_NO,
	SHOW_PARTICLES_REGENERATE_YES,
} ShowParticlesRegenerate;

bool
Show_particles(ParticlesSource *particles, f32 dt, ShowParticlesRegenerate regenerate);


ParticlesSource
Make_particles_source(const ParticlesDesc *desc, Vec2 pos, u64 seed);

#endif // PARTICLES_H


