#ifndef MENU_H
#define MENU_H

typedef struct {
	const char *text;
	f32 height, x0, x1, y0, y1;
} MenuOption;

typedef enum {
	OPTION_STATE_UNSELECTED,
	OPTION_STATE_SELECTED,
	OPTION_STATE_HOLDED,
} OptionState;

MenuOption
Make_menu_option(const char *text, f32 y_percent);

int
Show_menu(MenuOption *options, u32 options_count, i32 selected_option, i32 *selected_option_out);



#endif // MENU_H

