
#include "scene_game.h"
#include "hud.h"
#include "game.h"

u32
Update_player_animation(f32 dt) {
	u32 sprite_idx = 0;

	if (GLOBALS.hammer.state != HAMMER_STATE_UNUSED && !Is_player_dead()) {
		f32 smash_lerp = GLOBALS.hammer.dir/90.0f;
		if (GLOBALS.hammer.state != HAMMER_STATE_GO_DOWN) {
			smash_lerp = 1.0f - smash_lerp;
		}
		if (GLOBALS.player.facing == PLAYER_FACING_LEFT) {
			sprite_idx = Animation_lerp(&PLAYER_ANIMATION_SMASH_LEFT, smash_lerp);
		}
		else {
			sprite_idx = Animation_lerp(&PLAYER_ANIMATION_SMASH_RIGHT, smash_lerp);
		}
		return sprite_idx;
	}

	const Animation *anim = NULL;

	if (Is_player_dead()) {
		anim = &PLAYER_ANIMATION_DAMAGED;
	}
	else if (GLOBALS.player.damaged_anim_time > 0.0f) {
		anim = &PLAYER_ANIMATION_DAMAGED;
	}
	else if (GLOBALS.player.physics.vel.x < 0.0f && GLOBALS.player.facing == PLAYER_FACING_RIGHT) {
		anim = &PLAYER_ANIMATION_TURN_RIGHT;
	}
	else if (GLOBALS.player.physics.vel.x > 0.0f && GLOBALS.player.facing == PLAYER_FACING_LEFT) {
		anim = &PLAYER_ANIMATION_TURN_LEFT;
	}
	else if (GLOBALS.player.physics.grounded) {
		if (GLOBALS.player.physics.vel.x < 0.0f) {
			anim = &PLAYER_ANIMATION_WALK_LEFT;
		}
		else if (GLOBALS.player.physics.vel.x > 0.0f) {
			anim = &PLAYER_ANIMATION_WALK_RIGHT;
		}
		else {
			if (GLOBALS.player.facing == PLAYER_FACING_RIGHT) {
				anim = &PLAYER_ANIMATION_IDLE_RIGHT;
			}
			else {
				anim = &PLAYER_ANIMATION_IDLE_LEFT;
			}
		}
	}
	else {
		if (GLOBALS.player.facing == PLAYER_FACING_RIGHT) {
			anim = &PLAYER_ANIMATION_JUMP_RIGHT;
		}
		else {
			anim = &PLAYER_ANIMATION_JUMP_LEFT;
		}
	}

	if (GLOBALS.player.anim_state.animation != anim) {
		GLOBALS.player.anim_state = Make_animation_state(anim);
	}
	return Play_animation(&GLOBALS.player.anim_state, dt);
}


u32
Update_enemy_animation(Enemy *e, f32 dt) {
	const Animation *anim = NULL;

	if (e->dead) {
		anim = &ENEMY_PROPS[e->type].ENEMY_ANIMATION_DEAD;
	}
    else if (e->physics.grounded) {
        if (e->physics.vel.x < 0.0f) {
            anim = &ENEMY_PROPS[e->type].ENEMY_ANIMATION_WALK_LEFT;
        }
        else if (e->physics.vel.x > 0.0f) {
            anim = &ENEMY_PROPS[e->type].ENEMY_ANIMATION_WALK_RIGHT;
        }
        else {
            anim = &ENEMY_PROPS[e->type].ENEMY_ANIMATION_IDLE;
        }
    }
    else {
        if (e->physics.vel.x < 0.0f) {
            anim = &ENEMY_PROPS[e->type].ENEMY_ANIMATION_JUMP_LEFT;
        }
        else {
            anim = &ENEMY_PROPS[e->type].ENEMY_ANIMATION_JUMP_RIGHT;
        }
    }

    if (e->anim_state.animation != anim) {
        e->anim_state = Make_animation_state(anim);
    }
    return Play_animation(&e->anim_state, dt);
}



void
Update_hammer_physics() {
	bool hammer_impact_moment = false;
	f32 shake_factor = 0.0f;
	if (GLOBALS.hammer.state == HAMMER_STATE_UNUSED) {

		GLOBALS.hammer.angle = 0.0f;
	}
	else if (GLOBALS.hammer.state == HAMMER_STATE_IMPACTING) {

		GLOBALS.hammer.time_impacting += GAMEPLAY_DELTA_TIME;
		if (GLOBALS.player.physics.grounded) {
			hammer_impact_moment = true;
			GLOBALS.hammer.state = HAMMER_STATE_GO_UP;
			GLOBALS.player.physics.accel.y = 0.0f;
			GLOBALS.player.physics.max_vel_air.y = PLAYER_MAX_VEL_Y;
			shake_factor = Min(GLOBALS.hammer.time_impacting/(GAMEPLAY_DELTA_TIME*50.0f), 1.0f);
			GLOBALS.hammer.time_impacting = 0.0f;
		}
	}
	else {
		f32 accel = HAMMER_ANGULAR_ACCEL;
		f32 vel   = GLOBALS.hammer.angular_vel;
		f32 dir   = GLOBALS.hammer.dir;

		vel += accel * GAMEPLAY_DELTA_TIME;
		vel = Clamp(vel, -HAMMER_MAX_ANGULAR_VEL, HAMMER_MAX_ANGULAR_VEL);

		dir += vel * GAMEPLAY_DELTA_TIME;

		if (dir >= 90.0f) {
			dir = 0.0f;
			vel = 0.0f;
			if (GLOBALS.hammer.state == HAMMER_STATE_GO_DOWN) {
				GLOBALS.hammer.state = HAMMER_STATE_IMPACTING;
				GLOBALS.player.physics.accel.y = PLAYER_SMASHING_ACCEL_Y;
				GLOBALS.player.physics.max_vel_air.y = PLAYER_SMASHING_MAX_VEL_Y;
			}
			else if (GLOBALS.hammer.state == HAMMER_STATE_GO_UP) {
				GLOBALS.hammer.state = HAMMER_STATE_UNUSED;
				++GLOBALS.hammer.total_chunks;
				GLOBALS.hammer.last_chunk = 0.0f;
			}
		}

		GLOBALS.hammer.angular_vel = vel;
		GLOBALS.hammer.dir         = dir;

		if (GLOBALS.hammer.state == HAMMER_STATE_GO_DOWN) {
			if (GLOBALS.player.facing == PLAYER_FACING_RIGHT) {
				GLOBALS.hammer.angle = -dir;
			}
			else {
				GLOBALS.hammer.angle = dir;
			}
		}
		if (GLOBALS.hammer.state == HAMMER_STATE_GO_UP) {
			if (GLOBALS.player.facing == PLAYER_FACING_RIGHT) {
				GLOBALS.hammer.angle = dir-90.0f;
			}
			else {
				GLOBALS.hammer.angle = 90.0f-dir;
			}
			GLOBALS.hammer.last_chunk = Lerp(0.0f, HAMMER_CHUNK_SIZE, dir/90.0f);
		}
	}

	// Compute the hammer head vertices
	{
		Vec2 pos = GLOBALS.player.physics.pos;
		if (GLOBALS.player.facing == PLAYER_FACING_RIGHT) {
			pos.x += HAMMER_POS_ON_PLAYER_X;
			pos.y += HAMMER_POS_ON_PLAYER_Y;
		}
		else {
			pos.x += PLAYER_WIDTH-HAMMER_POS_ON_PLAYER_X;
			pos.y += HAMMER_POS_ON_PLAYER_Y;
		}
		Mat2 rot = M2_rotation(Radians(GLOBALS.hammer.angle));
		f32 stick_height = GLOBALS.hammer.total_chunks * HAMMER_CHUNK_SIZE + GLOBALS.hammer.last_chunk;
		Vec2 head_o  = V2(HAMMER_HEAD_WIDTH/2, -stick_height-9.5f);
		Vec2 head_p_lb = V2_add(Mul_v2_m2(V2_sub(V2(0.0f, 0.0f), head_o), rot), pos);
		Vec2 head_p_rb = V2_add(Mul_v2_m2(V2_sub(V2(HAMMER_HEAD_WIDTH, 0.0f), head_o), rot), pos);
		Vec2 head_p_rt = V2_add(Mul_v2_m2(V2_sub(V2(HAMMER_HEAD_WIDTH, HAMMER_HEAD_HEIGH), head_o), rot), pos);
		Vec2 head_p_lt = V2_add(Mul_v2_m2(V2_sub(V2(0.0f, HAMMER_HEAD_HEIGH), head_o), rot), pos);

		if (hammer_impact_moment) {
			Vec2 bottom_vert_0, bottom_vert_1;
			if (GLOBALS.player.facing == PLAYER_FACING_RIGHT) {
				bottom_vert_0 = head_p_rb;
				bottom_vert_1 = head_p_rt;
			}
			else {
				bottom_vert_0 = head_p_lb;
				bottom_vert_1 = head_p_lt;
			}

			if (Point_on_collisionable_tile(bottom_vert_0) ||
				Point_on_collisionable_tile(V2_add(bottom_vert_0, V2(0.0f,-5.0f))) ||
				Point_on_collisionable_tile(bottom_vert_1) ||
				Point_on_collisionable_tile(V2_add(bottom_vert_1, V2(0.0f,-5.0f)))) {

				Vec2 medium_vert = V2_add(bottom_vert_0, V2_mulf(V2_sub(bottom_vert_1, bottom_vert_0), 0.5));
				Mixer_play_sound(GLOBALS.hammer_impact_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
				
				f32 part_source_x = (medium_vert.x + HAMMER_IMPACT_OFFSET_X);
				f32 part_source_y = (medium_vert.y + HAMMER_IMPACT_OFFSET_Y);
				Add_particle_source(&HAMMER_IMPACT_PARTICLES_DESC, part_source_x, part_source_y);

				void
				Set_shake_screen(Vec2 acceleration, f32 min_x, f32 min_y, f32 max_x, f32 max_y, u32 bounces);
				Set_shake_screen(V2(0, Lerp(-50, -150, shake_factor)), 0, Lerp(-2.0f, -6.0f, shake_factor), 0, Lerp(2.0f, 6.0f, shake_factor), 2);
			}
		}
		GLOBALS.hammer.head_vertices[0] = head_p_lb;
		GLOBALS.hammer.head_vertices[1] = head_p_rb;
		GLOBALS.hammer.head_vertices[2] = head_p_rt;
		GLOBALS.hammer.head_vertices[3] = head_p_lt;
	}
}


void
Player_falls_off_the_map() {
	if (GLOBALS.player.fall_respawn_time > 0.0f) return;
	f32 player_y = GLOBALS.player.physics.pos.y + PLAYER_HEIGHT;
	if (player_y < 0.0f) {
		Mixer_play_sound(GLOBALS.fall_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
		GLOBALS.player.fall_respawn_time = PLAYER_FALL_RESPAWN_TIME;
		if (GLOBALS.player.lives > 0) {
			GLOBALS.player.lives -= 1;
		}
		GLOBALS.hud.lost_live_time = HUD_LOST_LIVE_TIME;
	}
}


void
Draw_hammer(Vec2 pos) {
	f32 ORIGIN_Y = 9.0f;
	if (GLOBALS.player.facing == PLAYER_FACING_RIGHT) {
		pos.x += HAMMER_POS_ON_PLAYER_X;
		pos.y += HAMMER_POS_ON_PLAYER_Y;
	}
	else {
		pos.x += PLAYER_WIDTH-HAMMER_POS_ON_PLAYER_X;
		pos.y += HAMMER_POS_ON_PLAYER_Y;
	}
	Mat2 rot = M2_rotation(Radians(GLOBALS.hammer.angle));
	
	f32 stick_height = GLOBALS.hammer.total_chunks * HAMMER_CHUNK_SIZE + GLOBALS.hammer.last_chunk;

	f32 stickb_width  = HAMMER_STICK_WIDTH+HAMMER_BORDER*2;
	f32 stickb_height = stick_height+HAMMER_BORDER*2;
	Vec2 stickb_o  = V2(stickb_width * 0.5f, HAMMER_BORDER-ORIGIN_Y);

	Vec2 stickb_p0 = V2_add(Mul_v2_m2(V2_sub(V2(0.0f, 0.0f), stickb_o), rot), pos);
	Vec2 stickb_p1 = V2_add(Mul_v2_m2(V2_sub(V2(stickb_width, 0.0f), stickb_o), rot), pos);
	Vec2 stickb_p2 = V2_add(Mul_v2_m2(V2_sub(V2(stickb_width, stickb_height), stickb_o), rot), pos);
	Vec2 stickb_p3 = V2_add(Mul_v2_m2(V2_sub(V2(0.0f, stickb_height), stickb_o), rot), pos);

	Vec2 stick_o  = V2(HAMMER_STICK_WIDTH/2, 0.0f-ORIGIN_Y);
	Vec2 stick_p0 = V2_add(Mul_v2_m2(V2_sub(V2(0.0f, 0.0f), stick_o), rot), pos);
	Vec2 stick_p1 = V2_add(Mul_v2_m2(V2_sub(V2(HAMMER_STICK_WIDTH, 0.0f), stick_o), rot), pos);
	Vec2 stick_p2 = V2_add(Mul_v2_m2(V2_sub(V2(HAMMER_STICK_WIDTH, stick_height), stick_o), rot), pos);
	Vec2 stick_p3 = V2_add(Mul_v2_m2(V2_sub(V2(0.0f, stick_height), stick_o), rot), pos);
	
	f32 alpha = HAMMER_MAX_ALPHA;
	if (GLOBALS.hammer.state != HAMMER_STATE_UNUSED) {
		f32 smash_lerp = GLOBALS.hammer.dir/90.0f;
		if (GLOBALS.hammer.state != HAMMER_STATE_GO_DOWN) {
			smash_lerp = 1.0f - smash_lerp;
		}
		alpha = Lerp(HAMMER_MIN_ALPHA, HAMMER_MAX_ALPHA, smash_lerp);
	}
	else {
		alpha = HAMMER_MIN_ALPHA;
	}

	Draw_quad(
		HMM_Vec3(stickb_p0.x, stickb_p0.y, 0.0f),
		HMM_Vec3(stickb_p1.x, stickb_p1.y, 0.0f),
		HMM_Vec3(stickb_p2.x, stickb_p2.y, 0.0f),
		HMM_Vec3(stickb_p3.x, stickb_p3.y, 0.0f),
		HMM_Vec4(0.0f, 0.0f, 0.0f, alpha));

	Draw_quad(
		HMM_Vec3(stick_p0.x, stick_p0.y, 0.0f),
		HMM_Vec3(stick_p1.x, stick_p1.y, 0.0f),
		HMM_Vec3(stick_p2.x, stick_p2.y, 0.0f),
		HMM_Vec3(stick_p3.x, stick_p3.y, 0.0f),
		HMM_Vec4(1.0f, 1.0f, 1.0f, alpha));
	
	Flush_triangles();
    
	Draw_sprite_ex(
        pos.x, 
        pos.y, 
        18, 
        12, 
        GLOBALS.hammer.angle, 
		0.5f, (-stick_height-ORIGIN_Y)/12.0f, 
        &GLOBALS.hammer_head_sprite, 0, V4(1.0f, 1.0f, 1.0f, alpha));

}



void
Draw_player_bounding_mesh() {
	f32 px = GLOBALS.player.physics.pos.x+((f32)PLAYER_WIDTH * 0.5f);
	f32 py = GLOBALS.player.physics.pos.y+((f32)PLAYER_HEIGHT * 0.5f);
	Draw_bounding_mesh(
		&SQUARE_BOUNDING_MESH,
		PLAYER_BOUNDING_MESH_WIDTH,
		PLAYER_BOUNDING_MESH_HEIGHT,
		px,
		py,
		0.0f,
		V4(0.0f, 1.0f, 0.0f, 0.6f));
    
}

void
Draw_all_items(f32 dt) {
	u32 items_count = GLOBALS.items_size;
	for (u32 i = 0; i < items_count; ++i) {

        f32 alpha = 1.0f;
		f32 pad_y = 0.0f;

		f32 spawning_time = Get_item_spawning_time_left(&GLOBALS.items[i]);
		if (spawning_time <= 0.0f) {
			if (GLOBALS.items[i].time_left <= 5.0f) {
        	    f32 blank = fmodf(GLOBALS.items[i].time_left, ITEM_INVULNERABLE_BLANK_TIME*2.0f);
			    alpha = fabsf((blank-ITEM_INVULNERABLE_BLANK_TIME)/(ITEM_INVULNERABLE_BLANK_TIME));
        	}
			pad_y = sinf(GLOBALS.items[i].time_left * PI32) * 2.5f;
		}
		else {
			pad_y = Lerp(0.0f, 15.0f, spawning_time/ITEM_SPAWNING_TIME);
			alpha = 0.7f;
		}

        u32 item_spawn_id = GLOBALS.items[i].item_spawn;
        f32 item_x = GLOBALS.items_spawn[item_spawn_id].x;
        f32 item_y = GLOBALS.items_spawn[item_spawn_id].y;
        Draw_sprite_ex(
            item_x, item_y + pad_y, 
            ITEM_RESTORE_HAMMER_HEIGHT, 
            ITEM_RESTORE_HAMMER_HEIGHT, 
            0.0f, 
            0.0f, 0.0f, &GLOBALS.item_hammer_sprite, 0, V4(1.0f, 1.0f, 1.0f, alpha));
	}
}

void
Draw_hammer_bounding_mesh() {

	Vec2 v0 = GLOBALS.hammer.head_vertices[0];
	Vec2 v1 = GLOBALS.hammer.head_vertices[1];
	Vec2 v2 = GLOBALS.hammer.head_vertices[2];
	Vec2 v3 = GLOBALS.hammer.head_vertices[3];
	Draw_triangle(
		HMM_Vec3(v0.x, v0.y, 0.0f),
		HMM_Vec3(v1.x, v1.y, 0.0f),
		HMM_Vec3(v2.x, v2.y, 0.0f),
		HMM_Vec4(0.0f, 0.0f, 1.0f, 0.6f)
	);
	Draw_triangle(
		HMM_Vec3(v0.x, v0.y, 0.0f),
		HMM_Vec3(v2.x, v2.y, 0.0f),
		HMM_Vec3(v3.x, v3.y, 0.0f),
		HMM_Vec4(0.0f, 0.0f, 1.0f, 0.6f)
	);

}


void
Draw_items_bounding_mesh() {
	u32 items_count = GLOBALS.items_size;
    for(u32 i = 0; i < items_count; ++i) {
        Item *item = &GLOBALS.items[i];
		u32 spawn_id = item->item_spawn;
		f32 px = GLOBALS.items_spawn[spawn_id].x + ((f32)ITEM_RESTORE_HAMMER_WIDTH * 0.5f);
		f32 py = GLOBALS.items_spawn[spawn_id].y + ((f32)ITEM_RESTORE_HAMMER_HEIGHT * 0.5f);
		Draw_bounding_mesh(
			&SQUARE_BOUNDING_MESH,
			ITEM_RESTORE_HAMMER_BOUNDING_MESH_WIDTH,
			ITEM_RESTORE_HAMMER_BOUNDING_MESH_HEIGHT,
			px,
			py,
			0.0f,
			V4(1.0f, 1.0f, 0.0f, 0.6f));
    }
}



void
Draw_enemies_bounding_mesh() {
    for(u32 i = 0; i < GLOBALS.enemies_size; i++) {
        Enemy *e = &GLOBALS.enemies[i];
		f32 px = e->physics.pos.x+((f32)ENEMY_PROPS[e->type].ENEMY_WIDTH  * 0.5f);
		f32 py = e->physics.pos.y+((f32)ENEMY_PROPS[e->type].ENEMY_HEIGHT * 0.5f);
		Draw_bounding_mesh(
			&SQUARE_BOUNDING_MESH,
			ENEMY_PROPS[e->type].ENEMY_BOUNDING_MESH_WIDTH,
			ENEMY_PROPS[e->type].ENEMY_BOUNDING_MESH_HEIGHT,
			px,
			py,
			0.0f,
			V4(1.0f, 0.0f, 0.0f, 0.6f));
    }
}


void
Draw_doors() {
    for (u32 i = 0; i < GLOBALS.doors_size; i++)
    {
        Door *d = &GLOBALS.doors[i];
        if (d->spawn_counter <= 0) {
            d->anim_state = Make_animation_state(&DOOR_ANIMATION_CLOSED);
        }
        else {
            d->spawn_counter -= GLOBALS.delta_time_f32;
        }
        u32 spr_idx = Play_animation(&d->anim_state, GLOBALS.delta_time_f32);
        Draw_sprite_ex(
            d->x, d->y,
            DOOR_WIDTH, DOOR_HEIGHT,
            0,
            0, 0,
            d->spriteset,
            spr_idx,
            V4(1, 1, 1, 1.0)
        );
    }
}

void
Draw_enemies(f32 dt) {
    for(u32 i = 0; i < GLOBALS.enemies_size; i++) {
        Enemy *e = &GLOBALS.enemies[i];
		if (e->dead) continue;
        u32 spr_idx = Update_enemy_animation(e, dt);
        Vec4 color;
        if (e->ia_data.state == IA_SPAWN) {
            color = V4(1, 1, 1, 0.5);
        }
        else {
            color = RGB_DEFAULT;
        }
        Draw_sprite_ex(
            e->physics.pos.x + e->width*(ENEMY_CENTERS[e->type].x), 
            e->physics.pos.y + e->height*(ENEMY_CENTERS[e->type].y), 
            ENEMY_PROPS[e->type].ENEMY_WIDTH+e->added_size, 
            ENEMY_PROPS[e->type].ENEMY_HEIGHT+e->added_size, 
            e->physics.rotation, 
            ENEMY_CENTERS[e->type].x, ENEMY_CENTERS[e->type].y, 
            e->spriteset, spr_idx, color
        );
    }
}


void
Draw_dead_enemies(f32 dt) {
    for(u32 i = 0; i < GLOBALS.enemies_size; i++) {
        Enemy *e = &GLOBALS.enemies[i];
		if (!e->dead) continue;
        u32 spr_idx = Update_enemy_animation(e, dt);
        Draw_sprite_ex(
            e->physics.pos.x + e->width*(ENEMY_CENTERS[e->type].x), 
            e->physics.pos.y + e->height*(ENEMY_CENTERS[e->type].y), 
            ENEMY_PROPS[e->type].ENEMY_WIDTH+e->added_size, 
            ENEMY_PROPS[e->type].ENEMY_HEIGHT+e->added_size, 
            e->physics.rotation, 
            ENEMY_CENTERS[e->type].x, ENEMY_CENTERS[e->type].y, 
            e->spriteset, spr_idx, V4(1.0f, 1.0f, 1.0f, 1.0f)
        );
    }
}

void
Draw_player(f32 dt) {
	u32 sprite_idx = Update_player_animation(dt);

	f32 alpha = 1.0f;
	if (GLOBALS.player.invulnerable_time > 0.0f && !Is_player_dead()) {
		f32 blank = fmodf(GLOBALS.player.invulnerable_time, PLAYER_INVULNERABLE_BLANK_TIME*2.0f);
		alpha = fabsf((blank-PLAYER_INVULNERABLE_BLANK_TIME)/(PLAYER_INVULNERABLE_BLANK_TIME));
	}
    Draw_sprite_ex(
		GLOBALS.player.physics.pos.x,
		GLOBALS.player.physics.pos.y,
		PLAYER_WIDTH, PLAYER_HEIGHT, 0.0, 0.0f, 0.0f,
        &GLOBALS.player.spriteset, sprite_idx, V4(1.0f, 1.0f, 1.0f, alpha));

}

void
Draw_all_particles(f32 dt) {
	u32 total_particle_sources = GLOBALS.particle_sources_count;

	for (u32 i = 0; i < total_particle_sources; ++i) {
		bool still_runing = Show_particles(&GLOBALS.particle_sources[i], dt, SHOW_PARTICLES_REGENERATE_NO);

		// Delete the particle
		if (!still_runing) {
			GLOBALS.particle_sources[i] = GLOBALS.particle_sources[total_particle_sources-1];
			--i;
			--total_particle_sources;
		}
	}
	GLOBALS.particle_sources_count = total_particle_sources;
}

void
Add_particle_source(const ParticlesDesc *part_desc, f32 x, f32 y) {
	u32 total_particle_sources = GLOBALS.particle_sources_count;
	if (total_particle_sources >= MAX_PARTICLE_SOURCES) return;
	GLOBALS.particle_sources[total_particle_sources] = Make_particles_source(part_desc, V2(x, y), (u64)rand());
	++total_particle_sources;
	GLOBALS.particle_sources_count = total_particle_sources;
}


void
Check_item_player_collisions() {
	Assert(SQUARE_BOUNDING_MESH.total_verts == 6, "Square bounding mesh must have 6 verts");
	// if (GLOBALS.player.invulnerable_time > 0.0f) return;

	f32 player_x = GLOBALS.player.physics.pos.x + (f32)PLAYER_WIDTH * 0.5f;
	f32 player_y = GLOBALS.player.physics.pos.y + (f32)PLAYER_HEIGHT * 0.5f;
	f32 s_width  = PLAYER_BOUNDING_MESH_WIDTH;
	f32 s_height = PLAYER_BOUNDING_MESH_HEIGHT;
	Vec2 player_t0[3];
	Vec2 player_t1[3];
	player_t0[0] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[0], V2(s_width, s_height)), V2(player_x, player_y));
	player_t0[1] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[1], V2(s_width, s_height)), V2(player_x, player_y));
	player_t0[2] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[2], V2(s_width, s_height)), V2(player_x, player_y));
	player_t1[0] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[3], V2(s_width, s_height)), V2(player_x, player_y));
	player_t1[1] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[4], V2(s_width, s_height)), V2(player_x, player_y));
	player_t1[2] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[5], V2(s_width, s_height)), V2(player_x, player_y));

	u32 items_count = GLOBALS.items_size;
    for(u32 i = 0; i < items_count; ++i) {
        Item *item = &GLOBALS.items[i];
		if (Get_item_spawning_time_left(item) > 0.0f) continue;

		u32 spawn_id = item->item_spawn;

		f32 item_x = GLOBALS.items_spawn[spawn_id].x + ((f32)ITEM_RESTORE_HAMMER_WIDTH * 0.5f);
		f32 item_y = GLOBALS.items_spawn[spawn_id].y + ((f32)ITEM_RESTORE_HAMMER_HEIGHT * 0.5f);

		f32 s_width  = ITEM_RESTORE_HAMMER_BOUNDING_MESH_WIDTH;
		f32 s_height = ITEM_RESTORE_HAMMER_BOUNDING_MESH_HEIGHT;

		Vec2 item_t[3];
		item_t[0] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[0], V2(s_width, s_height)), V2(item_x, item_y));
		item_t[1] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[1], V2(s_width, s_height)), V2(item_x, item_y));
		item_t[2] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[2], V2(s_width, s_height)), V2(item_x, item_y));
		
		bool collided = false;
		if (Triangles_overlap(player_t0, item_t) || Triangles_overlap(player_t1, item_t)) {
			collided = true;
		}
		else {
			item_t[0] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[3], V2(s_width, s_height)), V2(item_x, item_y));
			item_t[1] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[4], V2(s_width, s_height)), V2(item_x, item_y));
			item_t[2] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[5], V2(s_width, s_height)), V2(item_x, item_y));
			if (Triangles_overlap(player_t0, item_t) || Triangles_overlap(player_t1, item_t)) {
				collided = true;
			}
		}
		if (collided) {
			GLOBALS.hammer.total_chunks = 1;
			GLOBALS.hammer.last_chunk   = 0.0f;
			
			GLOBALS.items[i] = GLOBALS.items[items_count-1];
			GLOBALS.items_spawn[spawn_id].filled = false;
			--items_count;
			--i;
            Mixer_play_sound(GLOBALS.item_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
            GLOBALS.item_spawned = false;
		}
    }
	GLOBALS.items_size = items_count;
}


void
Check_enemy_player_collisions() {
	Assert(SQUARE_BOUNDING_MESH.total_verts == 6, "Square bounding mesh must have 6 verts");
	if (GLOBALS.player.invulnerable_time > 0.0f) return;

	f32 player_x = GLOBALS.player.physics.pos.x + (f32)PLAYER_WIDTH * 0.5f;
	f32 player_y = GLOBALS.player.physics.pos.y + (f32)PLAYER_HEIGHT * 0.5f;
	f32 s_width  = PLAYER_BOUNDING_MESH_WIDTH;
	f32 s_height = PLAYER_BOUNDING_MESH_HEIGHT;
	Vec2 player_t0[3];
	Vec2 player_t1[3];
	player_t0[0] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[0], V2(s_width, s_height)), V2(player_x, player_y));
	player_t0[1] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[1], V2(s_width, s_height)), V2(player_x, player_y));
	player_t0[2] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[2], V2(s_width, s_height)), V2(player_x, player_y));
	player_t1[0] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[3], V2(s_width, s_height)), V2(player_x, player_y));
	player_t1[1] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[4], V2(s_width, s_height)), V2(player_x, player_y));
	player_t1[2] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[5], V2(s_width, s_height)), V2(player_x, player_y));

	u32 enemies_size = GLOBALS.enemies_size;
    for(u32 i = 0; i < enemies_size; ++i) {
        Enemy *e = &GLOBALS.enemies[i];
		if (e->dead || e->ia_data.state == IA_SPAWN) continue;
		f32 enemy_x = e->physics.pos.x+((f32)ENEMY_PROPS[e->type].ENEMY_WIDTH  * 0.5f);
		f32 enemy_y = e->physics.pos.y+((f32)ENEMY_PROPS[e->type].ENEMY_HEIGHT * 0.5f);
		f32 s_width  = ENEMY_PROPS[e->type].ENEMY_BOUNDING_MESH_WIDTH;
		f32 s_height = ENEMY_PROPS[e->type].ENEMY_BOUNDING_MESH_HEIGHT;
		Vec2 enemy_t[3];
		enemy_t[0] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[0], V2(s_width, s_height)), V2(enemy_x, enemy_y));
		enemy_t[1] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[1], V2(s_width, s_height)), V2(enemy_x, enemy_y));
		enemy_t[2] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[2], V2(s_width, s_height)), V2(enemy_x, enemy_y));
		
		bool collided = false;
		if (Triangles_overlap(player_t0, enemy_t) || Triangles_overlap(player_t1, enemy_t)) {
			collided = true;
		}
		else {
			enemy_t[0] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[3], V2(s_width, s_height)), V2(enemy_x, enemy_y));
			enemy_t[1] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[4], V2(s_width, s_height)), V2(enemy_x, enemy_y));
			enemy_t[2] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[5], V2(s_width, s_height)), V2(enemy_x, enemy_y));
			if (Triangles_overlap(player_t0, enemy_t) || Triangles_overlap(player_t1, enemy_t)) {
				collided = true;
			}
		}
		if (collided) {
            Mixer_play_sound(GLOBALS.player_damaged_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
			if (player_x < enemy_x) {
				GLOBALS.player.physics.vel.x = -PLAYER_DAMAGED_VEL_X;
			}else {
				GLOBALS.player.physics.vel.x = PLAYER_DAMAGED_VEL_X;
			}
			if (GLOBALS.player.lives > 0) {
				GLOBALS.player.lives -= 1;
				GLOBALS.hud.lost_live_time = HUD_LOST_LIVE_TIME;
			}
			GLOBALS.player.invulnerable_time = PLAYER_INVULNERABLE_TIME;
			GLOBALS.player.physics.vel.y = PLAYER_DAMAGED_VEL_Y;
			GLOBALS.player.damaged_anim_time = PLAYER_DAMAGED_ANIM_TIME;
			GLOBALS.player.physics.grounded = false; // The damage animation will be only player while is on the air
													 // we set this to false to ensure that the player will be on the air
			GLOBALS.player.jump_delay_time = 0.0f; // Also disable the jump delay
			return; // We dont check more to no take more damage
		}
    }
}


void Spawn_enemy(f32 x, f32 y, EnemyType type) {
    u32 i = GLOBALS.enemies_size++;
    GLOBALS.enemies[i].physics = Basic_enemy_physics;
    GLOBALS.enemies[i].physics.pos.x = x;
    GLOBALS.enemies[i].physics.pos.y = y;
    GLOBALS.enemies[i].spriteset = &ENEMY_PROPS[type].SPRITESET;
    GLOBALS.enemies[i].ia_update_func = ENEMY_PROPS[type].UPDATE_FUNCTION;
    GLOBALS.enemies[i].width = ENEMY_PROPS[type].ENEMY_WIDTH;
    GLOBALS.enemies[i].height = ENEMY_PROPS[type].ENEMY_HEIGHT;
    GLOBALS.enemies[i].type = type;
    GLOBALS.enemies[i].anim_state = Make_animation_state(&ENEMY_PROPS[type].ENEMY_ANIMATION_WALK_LEFT);
    GLOBALS.enemies[i].dead = false;
    GLOBALS.enemies[i].ia_data.state = IA_SPAWN;
    GLOBALS.enemies[i].ia_data.edge = false;
    GLOBALS.enemies[i].ia_data.edge_change = false;
    GLOBALS.enemies[i].ia_data.patrolCount = Get_number_of_patrols(type);
    GLOBALS.enemies[i].ia_data.lastDir = 0;
    GLOBALS.enemies[i].ia_data.r_action = 0;
    GLOBALS.enemies[i].ia_data.spawn_counter = DOOR_OPEN_TIME;
}


void
Check_enemy_hammer_collisions() {
	if (GLOBALS.hammer.state != HAMMER_STATE_IMPACTING) return;

	Vec2 hammer_v0 = GLOBALS.hammer.head_vertices[0];
	Vec2 hammer_v1 = GLOBALS.hammer.head_vertices[1];
	Vec2 hammer_v2 = GLOBALS.hammer.head_vertices[2];
	Vec2 hammer_v3 = GLOBALS.hammer.head_vertices[3];
	Vec2 hammer_t0[3];
	Vec2 hammer_t1[3];
	hammer_t0[0] = hammer_v0;
	hammer_t0[1] = hammer_v1;
	hammer_t0[2] = hammer_v2;
	hammer_t1[0] = hammer_v0;
	hammer_t1[1] = hammer_v2;
	hammer_t1[2] = hammer_v3;
	f32 hammer_center_x = (hammer_v0.x + hammer_v1.x + hammer_v2.x + hammer_v3.x) / 4.0f;

	Assert(SQUARE_BOUNDING_MESH.total_verts == 6, "Square bounding mesh must have 6 verts");
	u32 enemies_size = GLOBALS.enemies_size;
    for(u32 i = 0; i < enemies_size; ++i) {
        Enemy *e = &GLOBALS.enemies[i];
		if (e->dead || e->ia_data.state == IA_SPAWN) continue;
		f32 px = e->physics.pos.x+((f32)ENEMY_PROPS[e->type].ENEMY_WIDTH  * 0.5f);
		f32 py = e->physics.pos.y+((f32)ENEMY_PROPS[e->type].ENEMY_HEIGHT * 0.5f);
		f32 s_width  = ENEMY_PROPS[e->type].ENEMY_BOUNDING_MESH_WIDTH;
		f32 s_height = ENEMY_PROPS[e->type].ENEMY_BOUNDING_MESH_HEIGHT;
		Vec2 enemy_t[3];
		enemy_t[0] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[0], V2(s_width, s_height)), V2(px, py));
		enemy_t[1] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[1], V2(s_width, s_height)), V2(px, py));
		enemy_t[2] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[2], V2(s_width, s_height)), V2(px, py));
		
		bool collided = false;
		if (Triangles_overlap(hammer_t0, enemy_t) || Triangles_overlap(hammer_t1, enemy_t)) {
			collided = true;
		}
		else {
			enemy_t[0] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[3], V2(s_width, s_height)), V2(px, py));
			enemy_t[1] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[4], V2(s_width, s_height)), V2(px, py));
			enemy_t[2] = V2_add(V2_mul(SQUARE_BOUNDING_MESH.vertices[5], V2(s_width, s_height)), V2(px, py));
			if (Triangles_overlap(hammer_t0, enemy_t) || Triangles_overlap(hammer_t1, enemy_t)) {
				collided = true;
			}
		}
		if (collided) {
            e->dead = true;
            e->physics.accel.x = 0;
            e->physics.accel.y = 0;
            e->physics.grounded = false;
            e->physics.drag_on_air = DPF(0.01f);
            Mixer_play_sound(GLOBALS.basic_enemy_death_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
            Enemy *ne;
            if (e->type == ENEMY_TYPE_HELMET) {
                Spawn_enemy(e->physics.pos.x, e->physics.pos.y, ENEMY_TYPE_BASIC);
                ne = &GLOBALS.enemies[GLOBALS.enemies_size-1];
            }
			if (hammer_center_x < px) {
                e->physics.vel.x = HAMMER_IMPACT_FORCE_X;
                e->physics.vel.y = HAMMER_IMPACT_FORCE_Y;
                e->physics.angular = -HAMMER_IMPACT_FORCE_R;
                if (e->type == ENEMY_TYPE_HELMET) {
                    // ne->physics.vel.x = HAMMER_IMPACT_FORCE_X;
                    ne->physics.vel.y = HAMMER_IMPACT_FORCE_Y;
                    ne->physics.angular = -HAMMER_IMPACT_FORCE_R;
                }
			}else {
                e->physics.vel.x = -HAMMER_IMPACT_FORCE_X;
                e->physics.vel.y = HAMMER_IMPACT_FORCE_Y;
                e->physics.angular = HAMMER_IMPACT_FORCE_R;
                if (e->type == ENEMY_TYPE_HELMET) {
                    // ne->physics.vel.x = -HAMMER_IMPACT_FORCE_X;
                    ne->physics.vel.y = HAMMER_IMPACT_FORCE_Y;
                    ne->physics.angular = HAMMER_IMPACT_FORCE_R;
                }
			}
			GLOBALS.player.physics.vel.y = GLOBALS.player.physics.gravity*1.5f; // The player stops a bit
			f32 part_source_x = (hammer_center_x+px)*0.5f + ENEMY_HITTED_PARTICLES_OFFSET_X;
			f32 part_source_y = (e->physics.pos.y + ENEMY_HITTED_PARTICLES_OFFSET_Y);
			Add_particle_source(&ENEMY_HITTED_PARTICLES_DESC, part_source_x, part_source_y);
		}
    }
}


void
Load_level_objects(u32 obj_size, Object *objects) {
    GLOBALS.doors_size = 0;
    for (u32 i = 0; i < obj_size; i++)
    {
        if (strcmp(objects[i].name, "door") == 0) {
            Door *d = &GLOBALS.doors[GLOBALS.doors_size];
            GLOBALS.doors_size++;
            i32 aux_x = objects[i].x / (f32)TILE_SIZE;
            d->x = aux_x * TILE_SIZE;
            i32 aux_y = objects[i].y / (f32)TILE_SIZE;
            d->y = aux_y * TILE_SIZE;
            d->anim_state = Make_animation_state(&DOOR_ANIMATION_CLOSED);
            d->spriteset = &GLOBALS.door_spr;
        }
        else if (strcmp(objects[i].name, "player") == 0) {
            GLOBALS.player.physics = Player_physics;
            i32 aux_y = objects[i].y / (f32)TILE_SIZE;
            GLOBALS.player.physics.pos = V2(objects[i].x, aux_y * TILE_SIZE);
            GLOBALS.player.was_grounded = false;
            GLOBALS.player.is_jumping = false;
            GLOBALS.player.lives = 3;
            GLOBALS.player.invulnerable_time = 0.0f;
            GLOBALS.player.jump_delay_time = 0.0f;
            GLOBALS.player.fall_respawn_time = 0.0f;
            GLOBALS.player.damaged_anim_time = 0.0f;
            GLOBALS.player.last_ground_pos = V2(30.0f, 30.0f);
            GLOBALS.player.facing = PLAYER_FACING_RIGHT;
            GLOBALS.player.anim_state = Make_animation_state(&PLAYER_ANIMATION_IDLE_RIGHT);
        }
        else if (strcmp(objects[i].name, "spawn") == 0) {
            i32 aux_x = objects[i].x / (f32)TILE_SIZE;
            GLOBALS.items_spawn[GLOBALS.items_spawn_size].x = aux_x * TILE_SIZE;
            i32 aux_y = objects[i].y / (f32)TILE_SIZE;
            GLOBALS.items_spawn[GLOBALS.items_spawn_size].y = (aux_y * TILE_SIZE) + 3;
            GLOBALS.items_spawn[GLOBALS.items_spawn_size].filled = true;
            GLOBALS.items_spawn_size++;
        }
    }
}


void
Init_level(Level *level) {

	GLOBALS.level_introduction_time = LEVEL_INTRODUCTION_TIME;
	GLOBALS.gameplay_music_playing = false;

	

    GLOBALS.enemies_size = 0;

    GLOBALS.item_spawned = false;
    GLOBALS.items_spawned = 0;
    GLOBALS.items_spawn_size = 0;
    GLOBALS.items_size = 0;

    GLOBALS.enemies_killed = 0;
    GLOBALS.spawn_array = level->enemies_array;
    GLOBALS.enemies_left_to_spawn = level->enemies_to_spawn;
    GLOBALS.spawn_time_left = level->spawn_time;
    Load_level_objects(level->obj_size, level->objects);

	GLOBALS.hammer.state = HAMMER_STATE_UNUSED;
	GLOBALS.hammer.dir = 0.0f;
	GLOBALS.hammer.angle = 0.0f;
	GLOBALS.hammer.last_chunk = 0.0f;
	GLOBALS.hammer.total_chunks = 1;
	GLOBALS.hammer.last_chunk = 0.0f;

	/// TEST PURPOSESSSS
	// GLOBALS.items_spawn_size = 1;
	// GLOBALS.items_spawn[0].x = 50.0f;
	// GLOBALS.items_spawn[0].y = 50.0f;
	// GLOBALS.items_spawn[0].filled = true;
	
	// GLOBALS.items_size = 1;
	// GLOBALS.items[0].type       = 0;
	// GLOBALS.items[0].item_spawn = 0;
	// GLOBALS.items[0].time_left  = 10.0f;
}

bool
Is_player_dead() {
	return (GLOBALS.dead_time > 0.0f);
}

bool
Is_level_completed() {
	return (GLOBALS.level_completed_time > 0.0f);
}

void
Simulate_player(f32 dt) {

	if (!Is_player_dead() && GLOBALS.player.lives == 0) {
		GLOBALS.dead_time = PLAYER_DEAD_TIME;
		GLOBALS.player.physics.accel = V2(0.0f, 0.0f);
		Mixer_remove_track(&GLOBALS.gameplay_music_handle);
		Mixer_play_sound(GLOBALS.player_dead_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
	}

	if (Is_player_dead()) {
		GLOBALS.dead_time -= dt;
		if (GLOBALS.dead_time <= 0.0f) {
			Init_level(&GLOBALS.levels[GLOBALS.current_level]);
		}
	}
    if (!Is_level_completed() && GLOBALS.enemies_killed >= GLOBALS.levels[GLOBALS.current_level].enemies_to_spawn) {
		GLOBALS.level_completed_time = LEVEL_COMPLETED_TIME;
		GLOBALS.played_level_completed_sound = false;
		Mixer_set_track_volume(&GLOBALS.gameplay_music_handle, MAX_VOLUME * 0.25f);
    }

	if (Is_level_completed()) {
		GLOBALS.level_completed_time -= dt;
		if (GLOBALS.level_completed_time <= 0.0f) {
			++GLOBALS.current_level;
			if (GLOBALS.current_level >= MAX_LEVELS) {
				GLOBALS.current_scene = SCENE_CONGRATULATIONS;
				GLOBALS.current_level = START_LEVEL;
				Mixer_remove_track(&GLOBALS.intro_music_handle);
				Mixer_remove_track(&GLOBALS.gameplay_music_handle);
				Mixer_play_sound(GLOBALS.intro_music, PLAY_SOUND_FLAG_LOOP, MAX_VOLUME, &GLOBALS.intro_music_handle);
			}
			Init_level(&GLOBALS.levels[GLOBALS.current_level]);
		}
	}

	// This is if the player has fall outside of the map, we have a little delay before respawn
	if (GLOBALS.player.fall_respawn_time > 0.0f && !Is_player_dead() && !Is_level_completed()) {
		GLOBALS.player.fall_respawn_time -= dt;
		if (GLOBALS.player.fall_respawn_time <= 0.0f) {
			GLOBALS.player.physics = Player_physics;
			GLOBALS.player.physics.pos = GLOBALS.player.last_ground_pos;
			GLOBALS.player.physics.pos.y += 5;
			GLOBALS.hammer.state = HAMMER_STATE_UNUSED;
			GLOBALS.hammer.dir   = 0.0f;
			GLOBALS.hammer.angle = 0.0f;
			GLOBALS.player.invulnerable_time = PLAYER_INVULNERABLE_TIME;
		}
	}

    Update_physics(&GLOBALS.player.physics);

	if (Is_player_dead()) return;

    Apply_tilemap_collision(&GLOBALS.player.physics);


	if (GLOBALS.player.jump_delay_time > 0.0f) {
		GLOBALS.player.jump_delay_time -= dt;
	}
	if (GLOBALS.player.physics.grounded) {
		GLOBALS.player.last_ground_pos = GLOBALS.player.physics.pos;
		GLOBALS.player.jump_delay_time = JUMP_DELAY_TIME;
		GLOBALS.player.damaged_anim_time = 0.0f;
	}
	else {
		if (GLOBALS.player.damaged_anim_time > 0.0f) {
			GLOBALS.player.damaged_anim_time -= dt;
		}
	}

	// Manage landing
	if (!GLOBALS.player.was_grounded && GLOBALS.player.physics.grounded) {
		Mixer_play_sound(GLOBALS.jump_landing_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
		GLOBALS.player.is_jumping = false; // If we landed, the player isn't jumping :)
	}
	GLOBALS.player.was_grounded = GLOBALS.player.physics.grounded;


	if (GLOBALS.player.invulnerable_time > 0.0f) {
		GLOBALS.player.invulnerable_time -= dt;
	}

	if (Is_level_completed()) return;

	Player_falls_off_the_map();
}


void
Simulate_enemies(f32 dt) {
	u32 enemies_size = GLOBALS.enemies_size;
	for (u32 i = 0; i < enemies_size; ++i) {
        Enemy *e = &GLOBALS.enemies[i];

		// Enemy physics
        Update_physics(&e->physics);

		if (!e->dead) {
			// Tilemap collisions
			Apply_tilemap_collision(&e->physics);

			// AI
            e->ia_update_func(i);
		}
		else {
			e->added_size = e->added_size + (ENEMY_ADDED_SIZE * dt);
		}

		// Remove the enemy if is outside of the map
		// This should be in a separated loop???
        if ((e->physics.pos.x + e->width < 0 || e->physics.pos.y + e->height < 0)
            && (e->physics.pos.x > GAME_AREA_W || e->physics.pos.y < GAME_AREA_H)) {
            if (e->type != ENEMY_TYPE_HELMET || !e->dead) { //Por si algun enemigo salta donde no debe
                GLOBALS.enemies_killed++;
            }
            memcpy(&GLOBALS.enemies[i], &GLOBALS.enemies[i+1], (GLOBALS.enemies_size - i)*sizeof(Enemy));
            --i;
            --GLOBALS.enemies_size;
			--enemies_size;
        }
	}
}



void
Check_gameplay_input() {
	GLOBALS.player.physics.accel.x = 0.0f;
	if ((app_data.keyboard[PLATFORM_KEYCODE_LEFT] == KEY_STATUS_DOWN ||
		app_data.keyboard[PLATFORM_KEYCODE_LEFT] == KEY_STATUS_HOLD) &&
		GLOBALS.hammer.state == HAMMER_STATE_UNUSED) {

		GLOBALS.player.facing = PLAYER_FACING_LEFT;
		if (GLOBALS.player.physics.grounded) {
			GLOBALS.player.physics.accel.x = -PLAYER_ACCEL_FLOOR;
		}
		else {
			GLOBALS.player.physics.accel.x = -PLAYER_ACCEL_AIR;
		}
	}
	if ((app_data.keyboard[PLATFORM_KEYCODE_RIGHT] == KEY_STATUS_DOWN ||
		app_data.keyboard[PLATFORM_KEYCODE_RIGHT] == KEY_STATUS_HOLD) && 
		GLOBALS.hammer.state == HAMMER_STATE_UNUSED) {

		GLOBALS.player.facing = PLAYER_FACING_RIGHT;
		if (GLOBALS.player.physics.grounded) {
			GLOBALS.player.physics.accel.x = PLAYER_ACCEL_FLOOR;
		}
		else {
			GLOBALS.player.physics.accel.x = PLAYER_ACCEL_AIR;
		}
	}
	if ((app_data.keyboard[PLATFORM_KEYCODE_UP] == KEY_STATUS_DOWN ||
		app_data.keyboard[PLATFORM_KEYCODE_UP] == KEY_STATUS_HOLD) &&
		GLOBALS.hammer.state == HAMMER_STATE_UNUSED) {

		if (!GLOBALS.player.is_jumping && GLOBALS.player.jump_delay_time > 0.0f) {
			Mixer_play_sound(GLOBALS.jump_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
			GLOBALS.player.physics.vel.y = PLAYER_JUMP_VEL;
			GLOBALS.player.is_jumping = true;
		}
	}
	if (app_data.keyboard[PLATFORM_KEYCODE_SPACE] == KEY_STATUS_DOWN && GLOBALS.hammer.state == HAMMER_STATE_UNUSED) {
		GLOBALS.hammer.state = HAMMER_STATE_GO_DOWN;
		Mixer_play_sound(GLOBALS.use_hammer_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
	}
}


void
Spawn_enemies() {
    if (GLOBALS.spawn_time_left <= 0 && GLOBALS.enemies_left_to_spawn > 0) {
        u32 idx = rand() / (f32)RAND_MAX * GLOBALS.doors_size;
        EnemyType type = GLOBALS.spawn_array[GLOBALS.levels[GLOBALS.current_level].enemies_to_spawn - GLOBALS.enemies_left_to_spawn--];
        Spawn_enemy(GLOBALS.doors[idx].x, GLOBALS.doors[idx].y, type);
        GLOBALS.spawn_time_left = GLOBALS.levels[GLOBALS.current_level].spawn_time;
        GLOBALS.doors[idx].anim_state = Make_animation_state(&DOOR_ANIMATION_OPEN);
        GLOBALS.doors[idx].spawn_counter = DOOR_OPEN_TIME;
        Mixer_play_sound(GLOBALS.open_door_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
    }
    else {
        GLOBALS.spawn_time_left -= GLOBALS.delta_time_f32;
    }
}


void
Render_gameplay(f32 dt) {
	Shake_screen();
	Draw_tilemap(&GLOBALS.levels[GLOBALS.current_level].tilemap);
    Draw_doors();
    Draw_enemies(dt);
	Draw_hammer(GLOBALS.player.physics.pos);
	Draw_player(dt);
	Draw_all_items(dt);
	Draw_dead_enemies(dt);
	if (GLOBALS.show_bounding_meshes) {
		Draw_enemies_bounding_mesh();
		Draw_player_bounding_mesh();
		Draw_hammer_bounding_mesh();
		Draw_items_bounding_mesh();
		Flush_triangles();
	}

	Draw_all_particles(dt);
	Flush_triangles();

	Show_hud(dt);
	if (Is_level_completed()) {
		f32 total_time = LEVEL_COMPLETED_TIME - GLOBALS.level_completed_time;
		if (total_time >= LEVEL_COMPLETED_SHOW_TEXT_TIME) {
			const char text[] = "LEVEL COMPLETED!";
			f32 text_h = 16.0f;
			f32 text_w = Get_text_width_with_height(&GLOBALS.font, text, text_h);
			f32 text_x = (GAMEPLAY_WIDTH - text_w) * 0.5f;
			f32 text_y = (GAMEPLAY_HEIGHT - text_h) * 0.5f;

			f32 rect_w = GAMEPLAY_WIDTH;
			f32 rect_h = text_h + 15.0f;
			f32 rect_x = (GAMEPLAY_WIDTH - rect_w) * 0.5;
			f32 rect_y = (GAMEPLAY_HEIGHT - rect_h) * 0.5;

			Draw_rect(
				HMM_Vec3(rect_x, rect_y, 0.0f),
				rect_w,
				rect_h,
				HMM_Vec4(0.0f, 0.0f, 0.0f, 0.7));

			Flush_triangles();

			Draw_text_with_height(&GLOBALS.font, text, text_h, text_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);

			f32 fade_out_time = (total_time - LEVEL_COMPLETED_FADEOUT_START_TIME);
			f32 black_rect_alpha = Clamp(fade_out_time/LEVEL_COMPLETED_FADEOUT_DURATION, 0.0f, 1.0f);
			Draw_rect(
				HMM_Vec3(0.0f, 0.0f, 0.0f),
				GAMEPLAY_WIDTH,
				GAMEPLAY_HEIGHT,
				HMM_Vec4(0.0f, 0.0f, 0.0f, black_rect_alpha));
			Flush_triangles();
		}
	}
	else if (Is_player_dead()) {
		f32 fade_out_time = (PLAYER_DEAD_TIME - GLOBALS.dead_time - PLAYER_DEAD_FADEOUT_START_TIME);
		f32 black_rect_alpha = Clamp(fade_out_time/PLAYER_DEAD_FADEOUT_DURATION, 0.0f, 1.0f);
		Draw_rect(
			HMM_Vec3(0.0f, 0.0f, 0.0f),
			GAMEPLAY_WIDTH,
			GAMEPLAY_HEIGHT,
			HMM_Vec4(0.0f, 0.0f, 0.0f, black_rect_alpha));
		Flush_triangles();
	}
}


void
Render_level_introduction(f32 dt) {
	f32 push_message_time = LEVEL_INTRODUCTION_TIME * 0.2;
	f32 pop_message_time  = LEVEL_INTRODUCTION_TIME * 0.8;
	f32 total_time = LEVEL_INTRODUCTION_TIME - GLOBALS.level_introduction_time;

	f32 offset_x = 0.0f;
	if (total_time < push_message_time) {
		f32 lerp_factor = total_time/push_message_time;
		offset_x = Lerp(-GAMEPLAY_WIDTH, offset_x, lerp_factor);
	}
	else if (total_time > pop_message_time) {
		f32 lerp_factor = (total_time-pop_message_time)/(LEVEL_INTRODUCTION_TIME-pop_message_time);
		offset_x = Lerp(offset_x, GAMEPLAY_WIDTH, lerp_factor);
	}

	u32 current_level = GLOBALS.current_level+1;
	u32 total_levels = MAX_LEVELS;
	char text[20];
	snprintf(text, 20, "LEVEL %d/%d", current_level, total_levels);
	f32 text_height = 16.0f;
	f32 text_width = Get_text_width_with_height(&GLOBALS.font, text, text_height);
	f32 text_x = (GAMEPLAY_WIDTH  - text_width) * 0.5f;
	f32 text_y = (GAMEPLAY_HEIGHT - text_height) * 0.5f;

	f32 rect_w = GAMEPLAY_WIDTH;
	f32 rect_h = text_height+15.0f;
	f32 rect_x = (GAMEPLAY_WIDTH - rect_w) * 0.5f;
	f32 rect_y = (GAMEPLAY_HEIGHT - rect_h) * 0.5f;

	Draw_rect(
		HMM_Vec3(rect_x+offset_x, rect_y, 0.0f),
		rect_w, rect_h,
		HMM_Vec4(0.0f, 0.0f, 0.0f, 0.7f));
	Flush_triangles();

	Draw_text_with_height(&GLOBALS.font, text, text_height, text_x+offset_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);
}



f32
Get_item_spawning_time_left(Item *item) {
	f32 total_time = (f32)ITEM_DESPAWN_TIME - item->time_left;
	return ((f32)ITEM_SPAWNING_TIME - total_time);
}

// Reduces the time left and if is 0 removes the item, also sets his spawn point as not filled
void
Simulate_items(f32 dt) {
	u32 items_count = GLOBALS.items_size;
	for (u32 i = 0; i < items_count; ++i) {
		Item *item = &GLOBALS.items[i];
		item->time_left -= dt;

        
		if (item->time_left <= 0.0f) {
			u32 item_spawn_id = GLOBALS.items[i].item_spawn;
			GLOBALS.items_spawn[item_spawn_id].filled = false;
			GLOBALS.items[i] = GLOBALS.items[items_count-1];
            GLOBALS.item_spawned = false;
			--i;
			--items_count;
		}
	}
	GLOBALS.items_size = items_count;

    if (GLOBALS.levels[GLOBALS.current_level].items > 0) {
        i32 enemies_per_item = GLOBALS.levels[GLOBALS.current_level].enemies_to_spawn / GLOBALS.levels[GLOBALS.current_level].items;
        i32 e_limit = GLOBALS.items_spawned * enemies_per_item + enemies_per_item / 2;
        // printf("EI: %i, EL: %i, IS: %i\n", enemies_per_item, e_limit, GLOBALS.items_spawned);
        if (GLOBALS.enemies_killed >= e_limit && !GLOBALS.item_spawned && GLOBALS.enemies_killed < GLOBALS.levels[GLOBALS.current_level].enemies_to_spawn) {
            u32 idx = rand()/(f32)RAND_MAX * GLOBALS.items_spawn_size;
            GLOBALS.items[GLOBALS.items_size].item_spawn = idx;
            GLOBALS.items[GLOBALS.items_size].time_left = ITEM_DESPAWN_TIME;
            GLOBALS.items_size++;
            GLOBALS.items_spawn[idx].filled = true;
            GLOBALS.item_spawned = true;
            GLOBALS.items_spawned++;
            Mixer_play_sound(GLOBALS.new_item_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
        }
    }
}



void
Scene_game() {

	if (GLOBALS.level_introduction_time > 0.0f) {
		Render_gameplay(0.0f);
		Render_level_introduction(GLOBALS.delta_time_f32);
		if (GLOBALS.level_introduction_time < LEVEL_INTRODUCTION_TIME * 0.5f && !GLOBALS.gameplay_music_playing) {
			Mixer_remove_track(&GLOBALS.gameplay_music_handle);
			Mixer_play_sound(GLOBALS.gameplay_music, PLAY_SOUND_FLAG_LOOP, MAX_VOLUME, &GLOBALS.gameplay_music_handle);
			GLOBALS.gameplay_music_playing = true;
		}
		GLOBALS.level_introduction_time -= GLOBALS.delta_time_f32;
		return;
	}

	Render_gameplay(GLOBALS.delta_time_f32);

	if (!Is_player_dead()) {
		Check_gameplay_input();
		if (app_data.keyboard[PLATFORM_KEYCODE_ESCAPE] == KEY_STATUS_DOWN) {
			GLOBALS.current_scene = SCENE_IN_GAME_MENU;
			Mixer_remove_track(&GLOBALS.player_footstep_handle);
			GLOBALS.player_footstep_playing = false;
			Mixer_pause_track(&GLOBALS.gameplay_music_handle);
			return;
		}
	}

	if (Is_level_completed()) {
		f32 total_time = LEVEL_COMPLETED_TIME - GLOBALS.level_completed_time;
		if (total_time >= LEVEL_COMPLETED_SHOW_TEXT_TIME && !GLOBALS.played_level_completed_sound) {
			Mixer_remove_track(&GLOBALS.gameplay_music_handle);
			Mixer_play_sound(GLOBALS.level_completed_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
			GLOBALS.played_level_completed_sound = true;
		}
	}

	{ // Manage footsteps
		if (GLOBALS.player.physics.grounded && GLOBALS.player.physics.vel.x != 0) {
			if (!GLOBALS.player_footstep_playing) {
				Mixer_play_sound(
					GLOBALS.player_footstep_sound,
					PLAY_SOUND_FLAG_LOOP,
					MAX_VOLUME,
					&GLOBALS.player_footstep_handle);
				GLOBALS.player_footstep_playing = true;
			}
		}
		else {
			Mixer_remove_track(&GLOBALS.player_footstep_handle);
			GLOBALS.player_footstep_playing = false;
		}
	}
	
	// if (GLOBALS.player.landed) {
	// 	Mixer_play_sound(GLOBALS.jump_landing_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
	// }
	// GLOBALS.player.landed = false;

    Spawn_enemies();

	f32 simulation_itersf = GLOBALS.delta_time_f32 / (GAMEPLAY_DELTA_TIME/GLOBALS.playback_speed) + GLOBALS.partial_iters;
	u32 simulation_iters = (u32)simulation_itersf;
	GLOBALS.partial_iters = simulation_itersf - (f32)simulation_iters;

	for (u32 i = 0; i < simulation_iters; ++i) {
		Simulate_player(GAMEPLAY_DELTA_TIME);
        Update_hammer_physics();
		Simulate_enemies(GAMEPLAY_DELTA_TIME);
        Simulate_items(GAMEPLAY_DELTA_TIME);
		Check_enemy_hammer_collisions();
		Check_enemy_player_collisions();
		Check_item_player_collisions();
	}
}


void
Set_shake_screen(Vec2 vel, f32 min_x, f32 min_y, f32 max_x, f32 max_y, u32 bounces) {
	GLOBALS.shake_screen.bounces = bounces;
	GLOBALS.shake_screen.vel = vel;
	GLOBALS.shake_screen.pos = V2(0,0);
	GLOBALS.shake_screen.min_x = min_x;
	GLOBALS.shake_screen.min_y = min_y;
	GLOBALS.shake_screen.max_x = max_x;
	GLOBALS.shake_screen.max_y = max_y;
	GLOBALS.shake_screen.restoring = false;
}

void
Shake_screen() {
	u32 bounces = GLOBALS.shake_screen.bounces;
	if (bounces == 0 && !GLOBALS.shake_screen.restoring) {
		return;
	}

	f32 dt = GLOBALS.delta_time_f32;
	Vec2 vel   = GLOBALS.shake_screen.vel;
	Vec2 pos   = GLOBALS.shake_screen.pos;

	if (GLOBALS.shake_screen.restoring) {
		Vec2 diff_pos_origin = V2_sub(V2(0,0), pos);
		f32 sign_x = Sign(diff_pos_origin.x);
		f32 sign_y = Sign(diff_pos_origin.y);
		pos = V2_add(pos, V2_mulf(vel, dt));
		diff_pos_origin = V2_sub(V2(0,0), pos);
		if (sign_x != Sign(diff_pos_origin.x) || sign_y != Sign(diff_pos_origin.y)) {
			pos = V2(0,0);
			GLOBALS.shake_screen.restoring = false;
		}
	}
	else {
		f32 min_x = GLOBALS.shake_screen.min_x;
		f32 min_y = GLOBALS.shake_screen.min_y;
		f32 max_x = GLOBALS.shake_screen.max_x;
		f32 max_y = GLOBALS.shake_screen.max_y;

		//vel = V2_add(vel, V2_mulf(accel, dt));
		pos = V2_add(pos, V2_mulf(vel, dt));

		bool bounced = false;
		if (pos.x < min_x) {
			pos.x = min_x;
			vel.x   = -vel.x;
			bounced = true;
		}
		if (pos.x > max_x) {
			pos.x = max_x;
			vel.x   = -vel.x;
			bounced = true;
		}
		if (pos.y < min_y) {
			pos.y = min_y;
			vel.y   = -vel.y;
			bounced = true;
		}
		if (pos.y > max_y) {
			pos.y = max_y;
			vel.y   = -vel.y;
			bounced = true;
		}

		if (bounced) {
			--bounces;
		}
		if (bounces == 0) {
			f32 vel_len = V2_len(vel);
			Vec2 diff_pos_origin = V2_sub(V2(0,0), pos);
			vel = V2_mulf(V2_normalize(diff_pos_origin), vel_len);
			GLOBALS.shake_screen.restoring = true;
		}
	}

	GLOBALS.shake_screen.bounces = bounces;
	GLOBALS.shake_screen.vel = vel;
	GLOBALS.shake_screen.pos = pos;
	f32 scale_w = GLOBALS.viewport_w / (f32)GAMEPLAY_WIDTH;
	f32 scale_h = GLOBALS.viewport_h / (f32)GAMEPLAY_HEIGHT;
	glViewport(GLOBALS.viewport_x+pos.x*scale_w, GLOBALS.viewport_y+pos.y*scale_h, GLOBALS.viewport_w, GLOBALS.viewport_h);

}

