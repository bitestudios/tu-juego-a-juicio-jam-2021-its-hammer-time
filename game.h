#ifndef GAME_H
#define GAME_H

#include "engine/base.h"
#include "engine/platform.h"
#include "engine/basic_gl.h"
#include "engine/font.h"
#include "engine/mixer.h"
#include "engine/third_party/HandmadeMath.h"
#include "engine/madmath.h"
#include "engine/sprite.h"
#include "engine/animation.h"
#include "engine/mesh-loader.h"
#include "particles.h"
#include "menu.h"
#include "main_menu.h"
#include "scene_in_game_menu.h"

#define PF(x) x*60.0f
#define DPF(x) x*60.0f*60.0f

#define GAME_TITLE  "IT'S HAMMER TIME!"

#define SAMPLE_RATE 30000
#define SOUND_CHANNELS 1

#define GAMEPLAY_WIDTH  320
#define GAMEPLAY_HEIGHT 192
// Desired aspect 
#define ASPECT_RATIO_W 320
#define ASPECT_RATIO_H 192

#define TILE_SIZE 16
#define TILEMAP_WIDTH 320
#define COLLISIONABLE_LAYER 0

// Impulse effect parameters
#define IMPULSE_EFFECT_SHOW_TIME 0.3
#define IMPULSE_EFFECT_LENGTH 10
#define IMPULSE_EFFECT_WIDTH 20

#define HUD_HEIGHT (20.0f)
#define GAME_AREA_BORDER (0.0f)
#define GAME_AREA_X  (GAME_AREA_BORDER)
#define GAME_AREA_Y  (GAME_AREA_BORDER)
#define GAME_AREA_W  (GAMEPLAY_WIDTH - (GAME_AREA_BORDER * 2.0f))
#define GAME_AREA_H  (GAMEPLAY_HEIGHT - GAME_AREA_BORDER - HUD_HEIGHT)

#define MAX_VOLUME 0.5f

#define FONT_FILE "assets/pixel_font.ttf"

#define GAMEPLAY_DELTA_TIME (1.0f/300.0f)

#define PLAYBACK_SPEED_MIN 0.1f
#define PLAYBACK_SPEED_MAX 8.0f
#define PLAYBACK_SPEED_SLOW_STEP 0.1f
#define PLAYBACK_SPEED_FAST_STEP 0.5f

#define VSYNC_MSG_TIME 2.5f
#define PLAYBACK_SPEED_MSG_TIME 2.5f


#define PLAYER_GRAVITY  (60.0f*60.0f*0.1f)
#define PLAYER_MAX_VEL_FLOOR (60.0f*1.55f)
#define PLAYER_MAX_VEL_AIR   (60.0f*1.5f)
#define PLAYER_MAX_VEL_Y     (60.0f*4.0f)
#define PLAYER_ACCEL_FLOOR  (60.0f*60.0f*0.3f)
#define PLAYER_ACCEL_AIR    (60.0f*60.0f*0.2f)
#define PLAYER_SMASHING_ACCEL_Y (-(60.0f*60.0f*2.0f))
#define PLAYER_SMASHING_MAX_VEL_Y (60.0f*6.0f)
#define PLAYER_JUMP_VEL (60.0f*3.0f)
#define PLAYER_FRICTION_FLOOR (60.0f*60.0f*0.15f)
#define PLAYER_FRICTION_AIR   (60.0f*60.0f*0.05f)

#define JUMP_DELAY_TIME 0.05f

#define PLAYER_DAMAGED_ANIM_TIME 0.25f

#define PLAYER_WIDTH  18.0f
#define PLAYER_HEIGHT 18.0f
#define PLAYER_BOUNDING_MESH_WIDTH  9.0f
#define PLAYER_BOUNDING_MESH_HEIGHT 14.0f

#define PLAYER_FALL_RESPAWN_TIME 1.0f

#define PLAYER_DAMAGED_VEL_X 100.0f
#define PLAYER_DAMAGED_VEL_Y 70.0f

#define PLAYER_INVULNERABLE_TIME 3.0f
#define PLAYER_INVULNERABLE_BLANK_TIME 0.1f

#define PLAYER_DEAD_TIME 4.0f
#define PLAYER_DEAD_FADEOUT_START_TIME 2.0f
#define PLAYER_DEAD_FADEOUT_DURATION 1.0f

#define HAMMER_CHUNK_SIZE 4.0f
#define HAMMER_STICK_WIDTH 2.0f
#define HAMMER_HEAD_WIDTH 17.0f
#define HAMMER_HEAD_HEIGH 9.0f
#define HAMMER_BORDER 1.0f

#define HAMMER_IMPACT_OFFSET_Y (-10.0f)
#define HAMMER_IMPACT_OFFSET_X (0.0f)
#define HAMMER_IMPACT_FORCE_X DPF(0.05f)
#define HAMMER_IMPACT_FORCE_Y DPF(1)
#define HAMMER_IMPACT_FORCE_R PF(15)


#define HAMMER_POS_ON_PLAYER_X 8.5f
#define HAMMER_POS_ON_PLAYER_Y 8.0f

#define HAMMER_ANGULAR_ACCEL (60.0f*60.0f*7.0f)
#define HAMMER_MAX_ANGULAR_VEL (60.0f*10.0f)

#define HAMMER_MIN_ALPHA 0.7f
#define HAMMER_MAX_ALPHA 1.0f

#define MAX_LIVES 3

#define HUD_HEARTS_PAD 5.0f
#define HUD_HEART_WIDTH 12.0f
#define HUD_HEART_HEIGHT 12.0f
#define HUD_HEART_MAX_WIDTH 50.0f
#define HUD_HEART_MAX_HEIGHT 50.0f
#define HUD_LOST_LIVE_TIME 2.0f
#define HUD_ENEMIES_COUNTER_PAD 5.0f

#define ENEMY_HITTED_PARTICLES_OFFSET_Y (-10.0f)
#define ENEMY_HITTED_PARTICLES_OFFSET_X (0.0f)
#define ENEMY_ADDED_SIZE 3.0f
#define MAX_ENEMIES_ON_SCREEN 50

#define MAX_PARTICLE_SOURCES 20

#define LEVEL_INTRODUCTION_TIME 3.0f
#define LEVEL_COMPLETED_TIME 5.0f
#define LEVEL_COMPLETED_SHOW_TEXT_TIME 1.0f
#define LEVEL_COMPLETED_FADEOUT_START_TIME 3.0f
#define LEVEL_COMPLETED_FADEOUT_DURATION 1.0f

#define ITEM_RESTORE_HAMMER_WIDTH  16
#define ITEM_RESTORE_HAMMER_HEIGHT 16
#define ITEM_RESTORE_HAMMER_BOUNDING_MESH_WIDTH  12
#define ITEM_RESTORE_HAMMER_BOUNDING_MESH_HEIGHT 12
#define ITEM_SPAWNING_TIME 5
#define ITEM_DESPAWN_TIME 20
#define ITEM_INVULNERABLE_BLANK_TIME 0.1f

#define MAX_DOORS 20
#define DOOR_WIDTH 16
#define DOOR_HEIGHT 15
#define DOOR_OPEN_TIME 1

#define MAX_ITEMS 20

#define MAX_LEVELS 5

#define ENEMY_JUMP_DISTANCE 4 //In tiles. Does not affect actual jump distance, its just a measure

#define START_LEVEL 0

enum {
	SCENE_MAIN_MENU,
	SCENE_GAME,
	SCENE_IN_GAME_MENU,
	SCENE_CONGRATULATIONS,
	SCENE_CREDITS,
	SCENE_HELP
};

enum {
	HAMMER_STATE_UNUSED = 0,
	HAMMER_STATE_GO_DOWN,
	HAMMER_STATE_IMPACTING,
	HAMMER_STATE_GO_UP,
};

typedef struct {
    Vec2 pos;
    Vec2 vel;
    Vec2 accel;
    f32 rotation;
    f32 angular;
    Vec2 max_vel;
    Vec2 max_vel_air;
    Vec2 left_foot_pos;
    Vec2 right_foot_pos;
    f32 friction;
    f32 drag_on_air;
    f32 gravity;
    bool grounded;
    bool left_foot;
    bool right_foot;
} Physics;

typedef enum {
	PLAYER_FACING_LEFT,
	PLAYER_FACING_RIGHT,
} PlayerFacing;

typedef struct {
    f32 lastDir;
    i32 patrolCount;
    u32 state;
    f32 r_action;
    i32 edge_change;
    bool edge;
    f32 spawn_counter;
    i32 dir;
} IAData;

typedef enum {
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_HELMET,
    ENEMY_TYPE_SMART
} EnemyType;

typedef struct {
    void(*ia_update_func)(u32);
    Physics physics;
    AnimationState anim_state;
    Spriteset *spriteset;
    bool dead;
    IAData ia_data;
    u32 width, height;
	f32 added_size;
    EnemyType type;
} Enemy;


typedef struct {
    Animation ENEMY_ANIMATION_WALK_RIGHT;
    Animation ENEMY_ANIMATION_WALK_LEFT;
    Animation ENEMY_ANIMATION_IDLE;
    Animation ENEMY_ANIMATION_JUMP_RIGHT;
    Animation ENEMY_ANIMATION_JUMP_LEFT;
    Animation ENEMY_ANIMATION_DEAD;
    f32 ENEMY_WIDTH;
    f32 ENEMY_HEIGHT;
    f32 ENEMY_JUMP_VEL;// (60.0f*6.0f)
    f32 ENEMY_BOUNDING_MESH_WIDTH;//  10.0f
    f32 ENEMY_BOUNDING_MESH_HEIGHT;// 10.0f
    f32 ENEMY_ACELL;// (60.0f*60.0f*0.21f)
    Spriteset SPRITESET;
    void (*UPDATE_FUNCTION)(u32);
    u32 MAX_PATROLS;
} EnemyProps;


typedef struct {
    u32 x, y;
    AnimationState anim_state;
    Spriteset *spriteset;
    f32 spawn_counter;
} Door;

typedef struct {
    u32 item_spawn;
	f32 time_left;
} Item;

typedef struct {
    u32 x, y;
	bool filled;
} ItemSpawn;


typedef struct {
    u32 enemies_to_spawn;
    f32 spawn_time;
    EnemyType *enemies_array;
    Vec2 player_pos;
    Object *objects;
    u32 obj_size;
    Tilemap tilemap;
    u32 items;
} Level;

typedef struct {
	f32 viewport_x, viewport_y, viewport_w, viewport_h;
	f32 playback_speed;
	i64 current_time_i64;
	i64 delta_time_i64;
	f32 current_time_f32;
	f32 delta_time_f32;
	int mouse_x;
	int mouse_y;

	f32 partial_iters;

	int current_scene;
	bool quit;
	bool fullscreen;
	bool show_fps;
	bool show_bounding_meshes;
	bool vsync_off;

	f32 vsync_msg_time;

	f32 playback_speed_msg_time;

	bool mouse_moved;

	// Menu stuffffff
	i32 main_menu_selected_option;
	MenuOption main_menu_options[MAIN_MENU_OPTION_COUNT];
	
	i32 in_game_menu_selected_option;
	MenuOption in_game_menu_options[IN_GAME_MENU_OPTION_COUNT];

	TextFont font;
	Spriteset tileset;

	Spriteset hammer_head_sprite;

	Spriteset hearts_sprites;
	Spriteset enemy_icon;
	Spriteset select_arrow;

	u32 select_option_sound;
	u32 launch_option_sound;

	u32 jump_sound;
	u32 fall_sound;
	u32 jump_landing_sound;
	u32 hammer_impact_sound;
    u32 basic_enemy_death_sound;
    u32 player_damaged_sound;
    u32 player_dead_sound;
    u32 use_hammer_sound;
    u32 open_door_sound;
    u32 level_completed_sound;
    u32 item_sound;
    u32 new_item_sound;

    u32 intro_music;
	TrackHandle intro_music_handle;
    u32 gameplay_music;
	TrackHandle gameplay_music_handle;

	u32 player_footstep_sound;
	TrackHandle  player_footstep_handle;
	bool player_footstep_playing;

    u32 doors_size;
    Door doors[MAX_DOORS];
    Spriteset door_spr;
    Spriteset arrows_sprite;
    Spriteset space_sprite;
    Spriteset item_hammer_sprite;

	f32 dead_time;
	f32 level_completed_time;
	bool played_level_completed_sound;

    u32 enemies_killed;
    i32 enemies_left_to_spawn;
    EnemyType *spawn_array;
    f32 spawn_time_left;
	f32 level_introduction_time;
	bool gameplay_music_playing;

    Level levels[MAX_LEVELS];
    u32 current_level;

	u32 particle_sources_count;
	ParticlesSource particle_sources[MAX_PARTICLE_SOURCES];

    bool item_spawned;
    i32 items_spawned;
	u32 items_size;
    Item items[MAX_ITEMS];

    u32 items_spawn_size;
    ItemSpawn items_spawn[MAX_ITEMS];

	struct {
		f32 lost_live_time;
	} hud;

	struct {
		bool was_grounded; // We need to store if was grounded in the previous simulation to know if 
		                   // the player has landed
		bool is_jumping;
		u32 lives;
		f32 invulnerable_time;
		f32 jump_delay_time;
		f32 fall_respawn_time;
		f32 damaged_anim_time;
		Vec2 last_ground_pos;
		PlayerFacing facing;
		Spriteset spriteset;
		Physics physics;
		AnimationState anim_state;
	} player;

	struct {
		f32 time_impacting;
		u32 state;
		u32 total_chunks;
		f32 last_chunk;
		f32 dir;
		f32 angle;
		f32 angular_accel;
		f32 angular_vel;
		Vec2 head_vertices[4];
	} hammer;

	struct {
		u32 bounces;
		bool restoring;
		Vec2 vel;
		Vec2 pos;
		f32 min_x;
		f32 min_y;
		f32 max_x;
		f32 max_y;
	} shake_screen;

    u32 enemies_size;
    Enemy enemies[MAX_ENEMIES_ON_SCREEN];
} Globals;


extern Globals GLOBALS;

extern const ParticlesDesc HAMMER_IMPACT_PARTICLES_DESC;
extern const ParticlesDesc ENEMY_HITTED_PARTICLES_DESC;

extern const Animation PLAYER_ANIMATION_SMASH_LEFT;
extern const Animation PLAYER_ANIMATION_SMASH_RIGHT;
extern const Animation PLAYER_ANIMATION_WALK_LEFT;
extern const Animation PLAYER_ANIMATION_WALK_RIGHT;
extern const Animation PLAYER_ANIMATION_TURN_LEFT;
extern const Animation PLAYER_ANIMATION_TURN_RIGHT;
extern const Animation PLAYER_ANIMATION_DAMAGED;
extern const Animation PLAYER_ANIMATION_IDLE_LEFT;
extern const Animation PLAYER_ANIMATION_IDLE_RIGHT;
extern const Animation PLAYER_ANIMATION_JUMP_LEFT;
extern const Animation PLAYER_ANIMATION_JUMP_RIGHT;

extern const Animation DOOR_ANIMATION_CLOSED;
extern const Animation DOOR_ANIMATION_OPEN;

extern const Animation BASIC_ENEMY_WALK_LEFT;
extern const Animation BASIC_ENEMY_WALK_RIGHT;
extern const Animation BASIC_ENEMY_IDLE_LEFT;
extern const Animation BASIC_ENEMY_IDLE_RIGHT;
extern const Animation BASIC_ENEMY_JUMP_LEFT;
extern const Animation BASIC_ENEMY_JUMP_RIGHT;

extern const BoundingMesh SQUARE_BOUNDING_MESH;

extern EnemyProps ENEMY_PROPS[];
extern const Vec2 ENEMY_CENTERS[];

extern const Physics Player_physics;
extern const Physics Basic_enemy_physics;


#endif // GAME_H

