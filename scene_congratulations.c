#include "scene_congratulations.h"


void
Scene_congratulations() {
	{// Draws the congratulations message
		const char text[] = "CONGRATULATIONS!";
		f32 text_h = 14.0f;
		f32 text_w = Get_text_width_with_height(&GLOBALS.font, text, text_h);
		f32 text_x = (GAMEPLAY_WIDTH - text_w) * 0.5f;
		f32 text_y = (GAMEPLAY_HEIGHT) * 0.85f;

		Draw_text_with_height(&GLOBALS.font, text, text_h, text_x+0.5f, text_y-0.5f, 1.0f, 0.0f, 0.0f, 1.0f);
		Draw_text_with_height(&GLOBALS.font, text, text_h, text_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);
	}
	{// Draws the congratulations message
		const char text[] = "YOU COMPLETED THE GAME!";
		f32 text_h = 14.0f;
		f32 text_w = Get_text_width_with_height(&GLOBALS.font, text, text_h);
		f32 text_x = (GAMEPLAY_WIDTH - text_w) * 0.5f;
		f32 text_y = (GAMEPLAY_HEIGHT) * 0.75f;

		Draw_text_with_height(&GLOBALS.font, text, text_h, text_x+0.5f, text_y-0.5f, 1.0f, 0.0f, 0.0f, 1.0f);
		Draw_text_with_height(&GLOBALS.font, text, text_h, text_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);
	}

	{
		f32 player_x = (f32)(GAMEPLAY_WIDTH  - PLAYER_WIDTH) * 0.5f;
		f32 player_y = (f32)(GAMEPLAY_HEIGHT - PLAYER_HEIGHT) * 0.5f;
		Draw_hammer(V2(player_x, player_y));
		Draw_sprite_ex(
			player_x,
			player_y,
			PLAYER_WIDTH, PLAYER_HEIGHT, 0.0, 0.0f, 0.0f,
        	&GLOBALS.player.spriteset, 0, V4(1.0f, 1.0f, 1.0f, 1.0f));
	}

    MenuOption menu = Make_menu_option("BACK TO MAIN MENU", 0.15f);
	if (Show_menu(&menu, 1, 0, NULL)) {
		GLOBALS.current_scene = SCENE_MAIN_MENU;
	}
}



