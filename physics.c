
#include "physics.h"

void Update_physics(Physics *phy) {
    Vec2 accel   = V2_add(phy->accel, V2(0, -phy->gravity));
	Vec2 new_vel = phy->vel;

	f32 friction = 0.0f;
	Vec2 max_vel  = {.x=0.0f, .y=0.0f};
	if (phy->grounded) {
		friction = phy->friction;
		max_vel  = phy->max_vel;
	}
	else {
		friction = phy->drag_on_air;
		max_vel  = phy->max_vel_air;
	}

	if (new_vel.x < 0.0f) {
		new_vel.x = Min(new_vel.x + friction * GAMEPLAY_DELTA_TIME, 0.0f);
	}
	if (new_vel.x > 0.0f) {
		new_vel.x = Max(new_vel.x - friction * GAMEPLAY_DELTA_TIME, 0.0f);
	}

	new_vel   = V2_add(new_vel, V2_mulf(accel, GAMEPLAY_DELTA_TIME));
	new_vel.x = Clamp(new_vel.x, -max_vel.x, max_vel.x);
	new_vel.y = Clamp(new_vel.y, -max_vel.y, max_vel.y);

	Vec2 new_pos = V2_add(phy->pos, V2_mulf(new_vel, GAMEPLAY_DELTA_TIME));
	//new_pos.x = Clamp(new_pos.x, 16.0f, GAMEPLAY_WIDTH);
	//new_pos.y = Clamp(new_pos.y, 16.0f, GAMEPLAY_HEIGHT);

	phy->vel = new_vel;
	phy->pos = new_pos;

    phy->rotation = phy->rotation + phy->angular * GAMEPLAY_DELTA_TIME;
}