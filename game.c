#include "game.h"

#define SZ(x) sizeof(x)/sizeof(x[0])

EnemyType LEVEL_0_WAVE[] = {
    ENEMY_TYPE_BASIC,
};

EnemyType LEVEL_1_WAVE[] = {
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_HELMET,
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_HELMET,
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_SMART
};

EnemyType LEVEL_2_WAVE[] = {
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_HELMET,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_HELMET,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_SMART
};

EnemyType LEVEL_3_WAVE[] = {
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_HELMET,
    ENEMY_TYPE_HELMET,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_HELMET,
    ENEMY_TYPE_HELMET,
    ENEMY_TYPE_BASIC,
    ENEMY_TYPE_HELMET,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_HELMET,
    ENEMY_TYPE_HELMET,
    ENEMY_TYPE_HELMET,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_SMART
};

EnemyType LEVEL_4_WAVE[] = {
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_SMART,
    ENEMY_TYPE_SMART
    // ENEMY_TYPE_SMART,
    // ENEMY_TYPE_SMART,
    // ENEMY_TYPE_SMART
};

Globals GLOBALS = {
    .levels = {
        {
            .enemies_array = LEVEL_0_WAVE,
            .enemies_to_spawn = SZ(LEVEL_0_WAVE),
            .spawn_time = 2,
            .items = 0
        },
        {
            .enemies_array = LEVEL_1_WAVE,
            .enemies_to_spawn = SZ(LEVEL_1_WAVE),
            .spawn_time = 4,
            .items = 2
        },
        {
            .enemies_array = LEVEL_2_WAVE,
            .enemies_to_spawn = SZ(LEVEL_2_WAVE),
            .spawn_time = 3.3,
            .items = 2
        },
        {
            .enemies_array = LEVEL_3_WAVE,
            .enemies_to_spawn = SZ(LEVEL_3_WAVE),
            .spawn_time = 3,
            .items = 3
        },
        {
            .enemies_array = LEVEL_4_WAVE,
            .enemies_to_spawn = SZ(LEVEL_4_WAVE),
            .spawn_time = 3.5,
            .items = 2
        }
    }
};

const Physics Player_physics = {
    // .pos = {0, 0},
    // .vel = {0, 0},
    // .accel = {0, 0},
    .max_vel = {.x=PLAYER_MAX_VEL_FLOOR, .y=PLAYER_MAX_VEL_Y},
    .max_vel_air = {.x=PLAYER_MAX_VEL_AIR, .y=PLAYER_MAX_VEL_Y},
    .friction = PLAYER_FRICTION_FLOOR,
    .drag_on_air = PLAYER_FRICTION_AIR,
    .gravity = PLAYER_GRAVITY,
    .grounded = false,
    .left_foot_pos = {.x=4.0f, .y=0.0f},
    .right_foot_pos = {.x=15.0f, .y=0.0f}
};

const Physics Basic_enemy_physics = {
    // .pos = {0, 0},
    // .vel = {0, 0},
    // .accel = {0, 0},
    .max_vel = {.x=PF(1.5f), .y=PF(2.0f)},
    .max_vel_air = {.x=PF(2.0f), .y=PF(2.0f)},
    .friction = DPF(0.2f),
    .drag_on_air = DPF(0.005f),
    .gravity = DPF(0.05),
    .grounded = false,
    .left_foot_pos = {.x=4.0f, .y=0.0f},
    .right_foot_pos = {.x=12.0f, .y=0.0f}
};

const ParticlesDesc HAMMER_IMPACT_PARTICLES_DESC = {
	.min_vel  = 20.0f,
	.max_vel  = 20.0f,
	.min_size = 0.5f,
	.max_size = 1.0f,

	.start_angle = 60.0f,
	.end_angle   = 120.0f,

	.min_spawn_radius = 10.0f,
	.max_spawn_radius = 15.0f,
	.min_spawn_rate = 0.01,
	.max_spawn_rate = 0.02,
	.particle_max_lifetime = 0.41f,
	.particle_min_lifetime = 0.4f,

	.color_0 = V4(1.0f, 0.0f, 0.0f, 0.6f),
	.color_1 = V4(1.0f, 0.0f, 0.0f, 1.0f),

	.global_accel = V2(0.0f, -50.0f),
	.global_max_vel = 50.0f,

	.fade_in_time  = 0.001f,
	.fade_out_time = 0.01f,
	.max_particles   = 10,
};


const ParticlesDesc ENEMY_HITTED_PARTICLES_DESC = {
	.min_vel  = 20.0f,
	.max_vel  = 20.0f,
	.min_size = 0.5f,
	.max_size = 1.0f,

	.start_angle = 60.0f,
	.end_angle   = 120.0f,

	.min_spawn_radius = 10.0f,
	.max_spawn_radius = 15.0f,
	.min_spawn_rate = 0.01,
	.max_spawn_rate = 0.02,
	.particle_max_lifetime = 0.41f,
	.particle_min_lifetime = 0.4f,

	.color_0 = V4(1.0f, 0.0f, 0.0f, 1.0f),
	.color_1 = V4(1.0f, 0.0f, 0.0f, 1.0f),

	.global_accel = V2(0.0f, -50.0f),
	.global_max_vel = 50.0f,

	.fade_in_time  = 0.001f,
	.fade_out_time = 0.01f,
	.max_particles   = 5,
};


const Animation PLAYER_ANIMATION_SMASH_RIGHT = {
	.n_keyframes = 2,
	.total_time  = 2.0f,
	.sprites     = {0, 3},
	.durations   = {1.0f, 1.0f},
};

const Animation PLAYER_ANIMATION_SMASH_LEFT = {
	.n_keyframes = 2,
	.total_time  = 2.0f,
	.sprites     = {7, 10},
	.durations   = {1.0f, 1.0f},
};

const Animation PLAYER_ANIMATION_WALK_RIGHT = {
	.n_keyframes = 4,
	.total_time  = 0.4f,
	.sprites     = {1, 0, 2, 0},
	.durations   = {0.09f, 0.09f, 0.09f, 0.09f},
};

const Animation PLAYER_ANIMATION_WALK_LEFT = {
	.n_keyframes = 4,
	.total_time  = 0.4f,
	.sprites     = {8, 7, 9, 7},
	.durations   = {0.09f, 0.09f, 0.09f, 0.09f},
};

const Animation PLAYER_ANIMATION_TURN_RIGHT = {
	.n_keyframes = 1,
	.total_time  = 1.0f,
	.sprites     = {6},
	.durations   = {1.0f},
};

const Animation PLAYER_ANIMATION_TURN_LEFT = {
	.n_keyframes = 1,
	.total_time  = 1.0f,
	.sprites     = {13},
	.durations   = {1.0f},
};

const Animation PLAYER_ANIMATION_IDLE_RIGHT = {
	.n_keyframes = 1,
	.total_time  = 1.0f,
	.sprites     = {0},
	.durations   = {1.0f},
};

const Animation PLAYER_ANIMATION_IDLE_LEFT = {
	.n_keyframes = 1,
	.total_time  = 1.0f,
	.sprites     = {7},
	.durations   = {1.0f},
};


const Animation PLAYER_ANIMATION_JUMP_RIGHT = {
	.n_keyframes = 1,
	.total_time  = 1.0f,
	.sprites     = {4},
	.durations   = {1.0f},
};

const Animation PLAYER_ANIMATION_JUMP_LEFT = {
	.n_keyframes = 1,
	.total_time  = 1.0f,
	.sprites     = {11},
	.durations   = {1.0f},
};

const Animation PLAYER_ANIMATION_DAMAGED = {
	.n_keyframes = 1,
	.total_time  = 1.0f,
	.sprites     = {5},
	.durations   = {1.0f},
};


const Animation DOOR_ANIMATION_CLOSED = {
	.n_keyframes = 1,
	.total_time  = 1.0f,
	.sprites     = {0},
	.durations   = {1.0f},
};

const Animation DOOR_ANIMATION_OPEN = {
	.n_keyframes = 1,
	.total_time  = 1.0f,
	.sprites     = {1},
	.durations   = {1.0f},
};



    // Animation BASIC_ENEMY_ANIMATION_WALK_RIGHT;
    // Animation BASIC_ENEMY_ANIMATION_WALK_LEFT;
    // Animation BASIC_ENEMY_ANIMATION_IDLE;
    // Animation BASIC_ENEMY_ANIMATION_JUMP_RIGHT;
    // Animation BASIC_ENEMY_ANIMATION_JUMP_LEFT;

EnemyProps ENEMY_PROPS[] = {
    [ENEMY_TYPE_BASIC] = {
        .ENEMY_ANIMATION_WALK_RIGHT = {
            .n_keyframes = 4,
            .total_time  = 0.4f,
            .sprites     = {0, 1, 3, 2},
            .durations   = {0.1f, 0.1f, 0.1f, 0.1f},
        },
        .ENEMY_ANIMATION_WALK_LEFT = {
            .n_keyframes = 4,
            .total_time  = 0.4f,
            .sprites     = {0, 2, 3, 1},
            .durations   = {0.1f, 0.1f, 0.1f, 0.1f},
        },
        .ENEMY_ANIMATION_IDLE = {
            .n_keyframes = 2,
            .total_time  = 1.0f,
            .sprites     = {0, 3},
            .durations   = {1.0f},
        },
        .ENEMY_ANIMATION_JUMP_RIGHT = {
            .n_keyframes = 1,
            .total_time  = 1.0f,
            .sprites     = {3},
            .durations   = {1.0f},
        },
        .ENEMY_ANIMATION_JUMP_LEFT = {
            .n_keyframes = 1,
            .total_time  = 1.0f,
            .sprites     = {3},
            .durations   = {1.0f},
        },
        .ENEMY_ANIMATION_DEAD = {
            .n_keyframes = 1,
            .total_time  = 1.0f,
            .sprites     = {4},
            .durations   = {1.0f},
        },
        .ENEMY_WIDTH = 16.0f,
        .ENEMY_HEIGHT = 12.0f,
        .ENEMY_JUMP_VEL = PF(4.0f),
        .ENEMY_BOUNDING_MESH_HEIGHT = 10.0f,
        .ENEMY_BOUNDING_MESH_WIDTH = 10.0f,
        .ENEMY_ACELL = DPF(0.203f),
        .UPDATE_FUNCTION = Simple_enemy_update,
        .MAX_PATROLS = 3
    },

    [ENEMY_TYPE_HELMET] = {
        .ENEMY_ANIMATION_WALK_RIGHT = {
            .n_keyframes = 4,
            .total_time  = 0.4f,
            .sprites     = {0, 1, 3, 2},
            .durations   = {0.1f, 0.1f, 0.1f, 0.1f},
        },
        .ENEMY_ANIMATION_WALK_LEFT = {
            .n_keyframes = 4,
            .total_time  = 0.4f,
            .sprites     = {0, 2, 3, 1},
            .durations   = {0.1f, 0.1f, 0.1f, 0.1f},
        },
        .ENEMY_ANIMATION_IDLE = {
            .n_keyframes = 2,
            .total_time  = 1.0f,
            .sprites     = {0, 3},
            .durations   = {1.0f},
        },
        .ENEMY_ANIMATION_JUMP_RIGHT = {
            .n_keyframes = 1,
            .total_time  = 1.0f,
            .sprites     = {3},
            .durations   = {1.0f},
        },
        .ENEMY_ANIMATION_JUMP_LEFT = {
            .n_keyframes = 1,
            .total_time  = 1.0f,
            .sprites     = {3},
            .durations   = {1.0f},
        },
        .ENEMY_ANIMATION_DEAD = {
            .n_keyframes = 1,
            .total_time  = 1.0f,
            .sprites     = {4},
            .durations   = {1.0f},
        },
        .ENEMY_WIDTH = 16.0f,
        .ENEMY_HEIGHT = 12.0f,
        .ENEMY_JUMP_VEL = PF(4.0f),
        .ENEMY_BOUNDING_MESH_HEIGHT = 10.0f,
        .ENEMY_BOUNDING_MESH_WIDTH = 10.0f,
        .ENEMY_ACELL = DPF(0.203f),
        .UPDATE_FUNCTION = Simple_enemy_update,
        .MAX_PATROLS = 4
    },

    [ENEMY_TYPE_SMART] = {
        .ENEMY_ANIMATION_WALK_RIGHT = {
            .n_keyframes = 4,
            .total_time  = 0.4f,
            .sprites     = {0, 1, 3, 2},
            .durations   = {0.1f, 0.1f, 0.1f, 0.1f},
        },
        .ENEMY_ANIMATION_WALK_LEFT = {
            .n_keyframes = 4,
            .total_time  = 0.4f,
            .sprites     = {0, 2, 3, 1},
            .durations   = {0.1f, 0.1f, 0.1f, 0.1f},
        },
        .ENEMY_ANIMATION_IDLE = {
            .n_keyframes = 2,
            .total_time  = 1.0f,
            .sprites     = {0, 3},
            .durations   = {1.0f},
        },
        .ENEMY_ANIMATION_JUMP_RIGHT = {
            .n_keyframes = 1,
            .total_time  = 1.0f,
            .sprites     = {3},
            .durations   = {1.0f},
        },
        .ENEMY_ANIMATION_JUMP_LEFT = {
            .n_keyframes = 1,
            .total_time  = 1.0f,
            .sprites     = {3},
            .durations   = {1.0f},
        },
        .ENEMY_ANIMATION_DEAD = {
            .n_keyframes = 1,
            .total_time  = 1.0f,
            .sprites     = {4},
            .durations   = {1.0f},
        },
        .ENEMY_WIDTH = 16.0f,
        .ENEMY_HEIGHT = 12.0f,
        .ENEMY_JUMP_VEL = PF(4.0f),
        .ENEMY_BOUNDING_MESH_HEIGHT = 10.0f,
        .ENEMY_BOUNDING_MESH_WIDTH = 10.0f,
        .ENEMY_ACELL = DPF(0.203f),
        .UPDATE_FUNCTION = Smart_enemy_update,
        .MAX_PATROLS = 2
    }
};


const Vec2 ENEMY_CENTERS[] = {
    [ENEMY_TYPE_BASIC] = {.x = 0.5f, .y = 0.416},
    [ENEMY_TYPE_HELMET] = {.x = 0.5f, .y = 0.416},
    [ENEMY_TYPE_SMART] = {.x = 0.5f, .y = 0.416},
};

const BoundingMesh SQUARE_BOUNDING_MESH = {
	.total_verts = 6,
	.vertices = (Vec2[6]){
		{.x=-0.5f, .y=-0.5f},
		{.x= 0.5f, .y=-0.5f},
		{.x= 0.5f, .y= 0.5f},
		{.x=-0.5f, .y=-0.5f},
		{.x= 0.5f, .y= 0.5f},
		{.x=-0.5f, .y= 0.5f}
	}
};
