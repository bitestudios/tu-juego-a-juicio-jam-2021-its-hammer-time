
#include "game.h"
#include "utils.h"
#include "collisions.h"
#include "physics.h"
#include "ia.h"
#include "particles.h"
#include "scene_game.h"
#include "menu.h"
#include "main_menu.h"
#include "scene_in_game_menu.h"
#include "scene_congratulations.h"
#include "scene_credits.h"
#include "scene_help.h"

void
Game_cleanup();

void
Show_msg(const char *msg) {
	static const f32 MSG_HEIGHT = 15.0f;
	static const f32 MSG_PAD    = 2.0f;
	f32 vsync_msg_w = Get_text_width_with_height(&GLOBALS.font, msg, MSG_HEIGHT);
	f32 msg_x = (GAMEPLAY_WIDTH - vsync_msg_w)   * 0.5f;
	f32 msg_y = (GAMEPLAY_HEIGHT - MSG_HEIGHT) * 0.5f;
	f32 rect_width  = vsync_msg_w  + MSG_PAD * 2.0f;
	f32 rect_height = MSG_HEIGHT + MSG_PAD * 2.0f;
	f32 rect_x      = (GAMEPLAY_WIDTH - rect_width)   * 0.5f;
	f32 rect_y      = (GAMEPLAY_HEIGHT - rect_height) * 0.5f;
	Draw_rect(
		HMM_Vec3(rect_x, rect_y, 0.0f),
		rect_width, rect_height,
		HMM_Vec4(0.0f, 0.0f, 0.0f, 0.7f));

	Flush_triangles();

	Draw_text_with_height(
		&GLOBALS.font,
		msg,
		MSG_HEIGHT,
		msg_x, msg_y,
		1.0f, 1.0f, 1.0f, 1.0f);
}


void
Draw_fps(const char *fps_message) {
	const f32 FPS_HEIGHT = 5.0f;
	const f32 FPS_PAD    = 5.0f;
	f32 fps_message_w = Get_text_width_with_height(&GLOBALS.font, fps_message, FPS_HEIGHT);
	Draw_rect(
		HMM_Vec3(GAMEPLAY_WIDTH-fps_message_w-FPS_PAD*2.0f, GAMEPLAY_HEIGHT-FPS_HEIGHT-FPS_PAD*2.0f, 0.0f),
		fps_message_w+FPS_PAD*2, FPS_HEIGHT+FPS_PAD*2.0f,
		HMM_Vec4(0.0f, 0.0f, 0.0f, 0.7f));
	Flush_triangles();

	Draw_text_with_height(
		&GLOBALS.font,
		fps_message,
		FPS_HEIGHT,
		((f32)GAMEPLAY_WIDTH-fps_message_w-FPS_PAD), (f32)GAMEPLAY_HEIGHT-FPS_HEIGHT-FPS_PAD,
		1.0f, 1.0f, 1.0f, 1.0f);
}



int
Game_frame() {
	if (app_data.quit_requested || GLOBALS.quit) {
		Game_cleanup();
		return 1;
	}
	if (app_data.window_resized) {
		u32 w_width  = app_data.w_width;
		u32 w_height = app_data.w_height;

		Calculate_viewport(
			w_width, w_height,
			&GLOBALS.viewport_x, &GLOBALS.viewport_y,
			&GLOBALS.viewport_w, &GLOBALS.viewport_h);

		glViewport(GLOBALS.viewport_x, GLOBALS.viewport_y, GLOBALS.viewport_w, GLOBALS.viewport_h);
		glScissor(GLOBALS.viewport_x, GLOBALS.viewport_y, GLOBALS.viewport_w, GLOBALS.viewport_h);
	}

	i64 current_time = App_time();
	GLOBALS.delta_time_i64     = current_time - GLOBALS.current_time_i64;
	GLOBALS.current_time_i64   = current_time;
	GLOBALS.delta_time_f32   = To_sec_f32(GLOBALS.delta_time_i64);
	GLOBALS.current_time_f32 = To_sec_f32(current_time);
	
	GLOBALS.mouse_moved = false;
	if (GLOBALS.mouse_x != app_data.mouse.x) {
		GLOBALS.mouse_x = app_data.mouse.x;
		GLOBALS.mouse_moved = true;
	}
	if (GLOBALS.mouse_y != app_data.mouse.y) {
		GLOBALS.mouse_y = app_data.mouse.y;
		GLOBALS.mouse_moved = true;
	}

	glDisable(GL_SCISSOR_TEST);
	glClearColor(0.0, 0.0, 0.0, 1.0);// Reset all to black
	glClear(GL_COLOR_BUFFER_BIT);
	glEnable(GL_SCISSOR_TEST);

	{
		Renderer_set_max_world_coordinates(GAMEPLAY_WIDTH, GAMEPLAY_HEIGHT);

		hmm_mat4 f_ortho = HMM_Orthographic(0.0f, (f32)GAMEPLAY_WIDTH, 0.0f, (f32)GAMEPLAY_HEIGHT, 0.0f, 1.0f);
		Font_renderer_set_matrix(f_ortho);
	}

	if (GLOBALS.current_scene == SCENE_MAIN_MENU) {
		Scene_main_menu();
	}
	else if (GLOBALS.current_scene == SCENE_GAME) {
		Scene_game();
	}
	else if (GLOBALS.current_scene == SCENE_IN_GAME_MENU) {
		Scene_in_game_menu();
	}
	else if (GLOBALS.current_scene == SCENE_CONGRATULATIONS) {
		Scene_congratulations();
	}
	else if (GLOBALS.current_scene == SCENE_CREDITS) {
		Scene_credits();
	}
	else if (GLOBALS.current_scene == SCENE_HELP) {
		Scene_help();
	}


	if (app_data.keyboard[PLATFORM_KEYCODE_F4] == KEY_STATUS_DOWN) {
		GLOBALS.show_bounding_meshes = !GLOBALS.show_bounding_meshes;
	}

	if (app_data.keyboard[PLATFORM_KEYCODE_F1] == KEY_STATUS_DOWN) {
		GLOBALS.vsync_msg_time = VSYNC_MSG_TIME;
		GLOBALS.vsync_off = !GLOBALS.vsync_off;
		if (GLOBALS.vsync_off) {
			Set_swap_interval(0);
		}
		else {
			Set_swap_interval(1);
		}
	}

	if (app_data.keyboard[PLATFORM_KEYCODE_F5] == KEY_STATUS_DOWN) {
		f32 p_speed = GLOBALS.playback_speed;
		if (p_speed <= 1.0f) {
			p_speed -= PLAYBACK_SPEED_SLOW_STEP;
		}
		else {
			p_speed -= PLAYBACK_SPEED_FAST_STEP;
		}
		GLOBALS.playback_speed = Clamp(p_speed, PLAYBACK_SPEED_MIN, PLAYBACK_SPEED_MAX);
		GLOBALS.playback_speed_msg_time = PLAYBACK_SPEED_MSG_TIME;
	}
	if (app_data.keyboard[PLATFORM_KEYCODE_F6] == KEY_STATUS_DOWN) {
		f32 p_speed = GLOBALS.playback_speed;
		if (p_speed <= 1.0f) {
			p_speed += PLAYBACK_SPEED_SLOW_STEP;
		}
		else {
			p_speed += PLAYBACK_SPEED_FAST_STEP;
		}
		GLOBALS.playback_speed = Clamp(p_speed, PLAYBACK_SPEED_MIN, PLAYBACK_SPEED_MAX);
		GLOBALS.playback_speed_msg_time = PLAYBACK_SPEED_MSG_TIME;
	}

	if (app_data.keyboard[PLATFORM_KEYCODE_F11] == KEY_STATUS_DOWN ||
		app_data.keyboard[PLATFORM_KEYCODE_F] == KEY_STATUS_DOWN) {
		GLOBALS.fullscreen = !GLOBALS.fullscreen;
		Set_fullscreen(GLOBALS.fullscreen);
	}

	if (app_data.keyboard[PLATFORM_KEYCODE_F3] == KEY_STATUS_DOWN) {
		GLOBALS.show_fps = !GLOBALS.show_fps;
	}

	// Calculate fps
	static char fps_message[32] = "FPS: 00.00";
	static u32 total_fps = 0;
	static f32 fps = 0.0f;
	static f32 fps_time_acum = 0.0f;
	fps_time_acum += GLOBALS.delta_time_f32;
	++total_fps;

	if (fps_time_acum >= 1.0f) {
		fps = (f32)(total_fps) / fps_time_acum;
		fps_time_acum = 0;
		total_fps = 0;
		snprintf(fps_message, sizeof(fps_message), "FPS: %.2f", fps);
	}

	if (GLOBALS.show_fps) {
		Draw_fps(fps_message);
	}

	if (GLOBALS.vsync_msg_time > 0.0f) {
		const char *msg = GLOBALS.vsync_off ? "vsync: off" : "vsync: on";
		Show_msg(msg);
		GLOBALS.vsync_msg_time -= GLOBALS.delta_time_f32;
	}

	if (GLOBALS.playback_speed_msg_time > 0.0f) {
		static char msg[200];
		snprintf(msg, 200, "PLAYBACK SPEED X%.2f", GLOBALS.playback_speed);
		Show_msg(msg);
		GLOBALS.playback_speed_msg_time -= GLOBALS.delta_time_f32;
	}

	return 0;
}




// the sample callback, running in audio thread
static int
stream_cb(void *user_data, float* buffer, int n_frames, int n_channels) {
	Mixer_play_frames(buffer, n_frames * n_channels);
	return 0;
}


void
Free_levels() {
    //TODO
}


void
Game_cleanup() {
	Renderer_cleanup();

	Free_font(&GLOBALS.font);
	Font_renderer_cleanup();

	Terminate_sound_player();
	Mixer_cleanup();

	Cleanup_GL();
	Destroy_window();

    Free_levels();
}


void
Load_level(Level *level, const char *filename) {
	level->objects = Load_tilemap_from_tiled_json(filename, ((GAMEPLAY_WIDTH-TILEMAP_WIDTH)/2), 0, TILE_SIZE, TILE_SIZE, &GLOBALS.tileset, &level->tilemap, GAMEPLAY_HEIGHT, &level->obj_size);

}


void
Load_levels() {
    Load_level(&GLOBALS.levels[0], "assets/maps/map0.json");
    Load_level(&GLOBALS.levels[1], "assets/maps/map1.json");
    Load_level(&GLOBALS.levels[2], "assets/maps/map2.json");
    Load_level(&GLOBALS.levels[3], "assets/maps/map3.json");
    Load_level(&GLOBALS.levels[4], "assets/maps/map4.json");
}


int
Game_setup() {
	WindowDesc window_desc = {
		.title  = GAME_TITLE,
		.width  = GAMEPLAY_WIDTH,
		.height = GAMEPLAY_HEIGHT,
	};

	if (Create_window(&window_desc) != 0)
		Panic("Cannot create window.");

	if (Setup_GL() != 0)
		Panic("Cannot setup GL.");

	Set_swap_interval(1); // enable vsync

	Init_app_time();

	if (Renderer_setup((f32)GAMEPLAY_WIDTH, (f32)GAMEPLAY_HEIGHT) != 0)
		Panic("Failed to setup renderer\n");

	if (Font_renderer_setup() != 0)
		Panic("Failed to setup font renderer\n");

	if (Make_font(&GLOBALS.font, FONT_FILE, 64.0f, FONT_USE_LIENAR_INTERPOLATION_NO) != 0) 
		Panic("Cannot make font '%s'.\n", FONT_FILE);

    //Load and compile shaders
	if (Init_sprites() != 0)
		Panic("Failet do set up sprite system\n");
    Renderer_set_max_world_coordinates(GAMEPLAY_WIDTH, GAMEPLAY_HEIGHT);

	SoundPlayerDesc splayer_desc = {
		.sample_rate = SAMPLE_RATE,
		.num_channels = SOUND_CHANNELS,
		.buffer_frames = 1024,
    	.user_data = NULL,
		.stream_cb = stream_cb
    };

    if (Launch_sound_player(&splayer_desc) != 0) {
		fprintf(stderr, "Error launching the sound player.\n");
	}

	Mixer_setup(sound_player_info.sample_rate, sound_player_info.num_channels);

	GLOBALS.jump_sound = Mixer_load_sound("assets/jump.ogg");
	GLOBALS.jump_landing_sound = Mixer_load_sound("assets/jump-landing.ogg");
	GLOBALS.fall_sound = Mixer_load_sound("assets/player-fall.ogg");
	GLOBALS.hammer_impact_sound = Mixer_load_sound("assets/hammer-impact.ogg");
	GLOBALS.player_footstep_sound = Mixer_load_sound("assets/player_footstep_0.ogg");
	GLOBALS.player_damaged_sound = Mixer_load_sound("assets/player_damaged.ogg");
    GLOBALS.player_dead_sound = Mixer_load_sound("assets/player_dead.ogg");
	GLOBALS.basic_enemy_death_sound = Mixer_load_sound("assets/basic_enemy_death.ogg");
	GLOBALS.use_hammer_sound = Mixer_load_sound("assets/use_hammer.ogg");
	GLOBALS.open_door_sound = Mixer_load_sound("assets/open_door.ogg");
	GLOBALS.select_option_sound = Mixer_load_sound("assets/select_option.ogg");
	GLOBALS.launch_option_sound = GLOBALS.select_option_sound;
	GLOBALS.select_option_sound = Mixer_load_sound("assets/select_option.ogg");
	GLOBALS.level_completed_sound = Mixer_load_sound("assets/level_completed.ogg");
	GLOBALS.intro_music = Mixer_load_sound("assets/intro_music.ogg");
	GLOBALS.gameplay_music = Mixer_load_sound("assets/gameplay_music.ogg");
    GLOBALS.item_sound = Mixer_load_sound("assets/item.ogg");
    GLOBALS.new_item_sound = Mixer_load_sound("assets/alarm.ogg");

	Mixer_play_sound(GLOBALS.intro_music, PLAY_SOUND_FLAG_PAUSED|PLAY_SOUND_FLAG_LOOP, MAX_VOLUME, &GLOBALS.intro_music_handle);
	Mixer_play_sound(GLOBALS.gameplay_music, PLAY_SOUND_FLAG_PAUSED|PLAY_SOUND_FLAG_LOOP, MAX_VOLUME, &GLOBALS.gameplay_music_handle);

	Mixer_resume_track(&GLOBALS.intro_music_handle);

	if (Make_spriteset("assets/jumpman.png", 18, 18, false, &GLOBALS.player.spriteset) != 0)
		Panic("Cannot load player's texture");

	if (Make_spriteset("assets/maps/monochrome_tilemap.png", 16, 16, false, &GLOBALS.tileset) != 0)
		Panic("Cannot load tileset");

	if (Make_spriteset("assets/hammer_head.png", 18, 12, false, &GLOBALS.hammer_head_sprite) != 0)
		Panic("Cannot load hammer head texture");

	if (Make_spriteset("assets/hammer.png", ITEM_RESTORE_HAMMER_WIDTH, ITEM_RESTORE_HAMMER_HEIGHT, false, &GLOBALS.item_hammer_sprite) != 0)
		Panic("Cannot load hammer item texture");

	if (Make_spriteset("assets/hearts.png", 12, 12, false, &GLOBALS.hearts_sprites) != 0)
		Panic("Cannot load hearts texture");

	if (Make_spriteset("assets/enemy_icon.png", 16, 16, false, &GLOBALS.enemy_icon) != 0)
		Panic("Cannot load hearts texture");

	if (Make_spriteset("assets/select_arrow.png", 14, 12, false, &GLOBALS.select_arrow) != 0)
		Panic("Cannot load select arrow texture");

	if (Make_spriteset("assets/arrows.png", 54, 36, false, &GLOBALS.arrows_sprite) != 0)
		Panic("Cannot load select arrows texture");

	if (Make_spriteset("assets/space_button.png", 55, 17, false, &GLOBALS.space_sprite) != 0)
		Panic("Cannot load select space texture");

    if (Make_spriteset("assets/door.png", 16, 15, false, &GLOBALS.door_spr) != 0)
		Panic("Cannot load tileset");

	GLOBALS.main_menu_selected_option = MAIN_MENU_OPTION_PLAY;
	GLOBALS.main_menu_options[MAIN_MENU_OPTION_PLAY]    = Make_menu_option("PLAY", 0.66f);
    GLOBALS.main_menu_options[MAIN_MENU_OPTION_HELP]    = Make_menu_option("HELP", 0.54f);
	GLOBALS.main_menu_options[MAIN_MENU_OPTION_CREDITS] = Make_menu_option("CREDITS", 0.42f);
	GLOBALS.main_menu_options[MAIN_MENU_OPTION_EXIT]    = Make_menu_option("EXIT", 0.30f);

	GLOBALS.in_game_menu_selected_option = IN_GAME_MENU_OPTION_CONTINUE;
	GLOBALS.in_game_menu_options[IN_GAME_MENU_OPTION_CONTINUE]     = Make_menu_option("CONTINUE", 0.54f);
    GLOBALS.in_game_menu_options[IN_GAME_MENU_OPTION_BACK_TO_MENU] = Make_menu_option("BACK TO MAIN MENU", 0.42f);

    Load_levels();

	GLOBALS.player.physics = Player_physics;
    GLOBALS.player.physics.pos          = V2(30.0f, 30.0f);
	GLOBALS.player.lives = 3;
	GLOBALS.hammer.total_chunks = 1;

	GLOBALS.player.anim_state = Make_animation_state(&PLAYER_ANIMATION_WALK_LEFT);

    if (Make_spriteset("assets/basic.png", 16, 12, false, &ENEMY_PROPS[ENEMY_TYPE_BASIC].SPRITESET) != 0) {
        Panic("Cannot load enemy sprites!\n");
    }

    if (Make_spriteset("assets/helmet.png", 16, 13, false, &ENEMY_PROPS[ENEMY_TYPE_HELMET].SPRITESET) != 0) {
        Panic("Cannot load enemy sprites!\n");
    }

    if (Make_spriteset("assets/smart.png", 16, 13, false, &ENEMY_PROPS[ENEMY_TYPE_SMART].SPRITESET) != 0) {
        Panic("Cannot load enemy sprites!\n");
    }

	GLOBALS.playback_speed = 1.0f;

	GLOBALS.level_introduction_time = LEVEL_INTRODUCTION_TIME;

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable( GL_BLEND );
	glEnable( GL_SCISSOR_TEST );
	glViewport(0, 0, GAMEPLAY_WIDTH, GAMEPLAY_HEIGHT);

	GLOBALS.current_level = START_LEVEL;
    Init_level(&GLOBALS.levels[GLOBALS.current_level]);

	GLOBALS.current_scene = SCENE_MAIN_MENU;

    srand((unsigned)App_time());

	return 0;
}


int
main() {
	if (Game_setup() != 0) {
		return -1;
	}
    return Run_application_loop(Game_frame);
}


#include "utils.c"
#include "game.c"
#include "collisions.c"
#include "physics.c"
#include "ia.c"
#include "scene_game.c"
#include "hud.c"
#include "particles.c"
#include "main_menu.c"
#include "scene_in_game_menu.c"
#include "menu.c"
#include "scene_congratulations.c"
#include "scene_credits.c"
#include "scene_help.c"


