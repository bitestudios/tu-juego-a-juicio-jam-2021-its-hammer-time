#include "game.h"
#include "menu.h"
#include "engine/madmath.h"


MenuOption
Make_menu_option(const char *text, f32 y_percent) {
	MenuOption result;
	result.height = 0.065f*(f32)GAMEPLAY_HEIGHT;
	f32 msg_width = Get_text_width_with_height(&GLOBALS.font, text, result.height);
	result.text = text;
	result.x0   = ((f32)GAMEPLAY_WIDTH - msg_width) * 0.5;
	result.x1   = result.x0 + msg_width;
	result.y0   = (f32)GAMEPLAY_HEIGHT * y_percent;
	result.y1   = result.y0 + result.height;
	return result;
}

bool
Mouse_on_option(MenuOption *option) {
	f32 mouse_x, mouse_y;
	Get_mouse_on_game_screen_coords(&mouse_x, &mouse_y);
	if (mouse_x >= option->x0 && mouse_x <= option->x1 && mouse_y >= option->y0 && mouse_y <= option->y1) {
		return true;
	}
	return false;
}

void
Show_option(MenuOption *option, OptionState state) {
	const f32 focus_scale = 1.1f;
	Vec4 color = V4(1.0f, 1.0f, 1.0f, 1.0f);
	
	if (state != OPTION_STATE_UNSELECTED) {

		f32 msg_height = option->height*focus_scale;
		f32 msg_width = Get_text_width_with_height(&GLOBALS.font, option->text, msg_height);
		f32 msg_x = ((f32)GAMEPLAY_WIDTH - msg_width) * 0.5;
		
		Draw_text_with_height(&GLOBALS.font, option->text, msg_height, msg_x+0.5f, option->y0-0.5f, color.r, 0.0f, 0.0f, color.a);
		Draw_text_with_height(&GLOBALS.font, option->text, msg_height, msg_x, option->y0, color.r, color.g, color.b, color.a);

		f32 arrow_h = 12.0f;
		f32 arrow_w = 14.0f;
		f32 arrow_x = msg_x - arrow_w - 2.0f;
		f32 arrow_y = option->y0 + msg_height * 0.5f - 3.0f;
		f32 arrow_pad = 0.0f;
		if (state != OPTION_STATE_HOLDED) {
			arrow_pad = ((sinf(GLOBALS.current_time_f32*PI32)+1) * 0.5) * 5.0f;
		}
		//Draw_sprite_ex(arrow_x-arrow_pad+1.0f, arrow_y-1.0f, arrow_w, arrow_h, 0.0f, 0.0f, 0.5f, &GLOBALS.select_arrow, 0, V4(1.0f, 0.0f, 0.0f, 1.0f));
		Draw_sprite_ex(arrow_x-arrow_pad, arrow_y, arrow_w, arrow_h, 0.0f, 0.0f, 0.5f, &GLOBALS.select_arrow, 0, V4(1.0f, 1.0f, 1.0f, 1.0f));
	}
	else {
		Draw_text_with_height(&GLOBALS.font, option->text, option->height, option->x0, option->y0, color.r, color.g, color.b, color.a);
	}
}

int
Show_menu(MenuOption *options, u32 options_count, i32 selected_option, i32 *selected_option_out) {
	bool triggered = false;

	if (app_data.keyboard[PLATFORM_KEYCODE_UP] == KEY_STATUS_DOWN) {
		--selected_option;
		if (selected_option < 0) {
			selected_option = 0;
		}
		Mixer_play_sound(GLOBALS.select_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
	}

	if (app_data.keyboard[PLATFORM_KEYCODE_DOWN] == KEY_STATUS_DOWN) {
		++selected_option;
		if (selected_option >= options_count) {
			selected_option = options_count-1;
		}
		Mixer_play_sound(GLOBALS.select_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
	}

	bool mouse_interact = false;
	if (GLOBALS.mouse_moved ||
		app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] != KEY_STATUS_NONE) {
		mouse_interact = true;
	}
	bool mouse_selecting = false;

	for (u32 i = 0; i < options_count; ++i) {
		OptionState option_state = OPTION_STATE_UNSELECTED;
		if (i == selected_option) {
			option_state = OPTION_STATE_SELECTED;
		}
		if (mouse_interact && Mouse_on_option(&options[i])) {
			mouse_selecting = true;
			if (selected_option != i) {
				Mixer_play_sound(GLOBALS.select_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
				selected_option = i;
			}

			// We set the option state
			if (app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] == KEY_STATUS_DOWN ||
				app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] == KEY_STATUS_HOLD) {
				option_state = OPTION_STATE_HOLDED;
			}
			else {
				option_state = OPTION_STATE_SELECTED;
			}

			// If the mouse button is up and is on a option we launch that option
			if (app_data.mouse.buttons[PLATFORM_MOUSE_BUTTON_LEFT] == KEY_STATUS_UP) {
				Mixer_play_sound(GLOBALS.launch_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
				triggered = true;
			}
		}
		Show_option(&options[i], option_state);
	}

	if (app_data.keyboard[PLATFORM_KEYCODE_ENTER] == KEY_STATUS_DOWN) {
		Mixer_play_sound(GLOBALS.launch_option_sound, PLAY_SOUND_FLAG_NONE, MAX_VOLUME, NULL);
		triggered = true;
	}

	if (selected_option_out) *selected_option_out = selected_option;
	return triggered;
}


