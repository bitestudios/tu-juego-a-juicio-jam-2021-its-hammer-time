#include "ia.h"
#include "collisions.h"


void 
Patrol(Enemy *e) {
    if (e->physics.left_foot && !e->physics.right_foot) {
        e->physics.accel.x = -ENEMY_PROPS[e->type].ENEMY_ACELL;
        e->ia_data.edge_change++;
        e->ia_data.edge = true;
    }
    else if (!e->physics.left_foot && e->physics.right_foot) {
        e->physics.accel.x = ENEMY_PROPS[e->type].ENEMY_ACELL;
        e->ia_data.edge_change++;
        e->ia_data.edge = true;
    }
    else if (e->physics.left_foot && e->physics.right_foot) {
        if (e->physics.vel.x > 0) {
            e->physics.accel.x = ENEMY_PROPS[e->type].ENEMY_ACELL;
        }
        else {
            e->physics.accel.x = -ENEMY_PROPS[e->type].ENEMY_ACELL;
        }
        e->ia_data.edge_change = -1;
        e->ia_data.edge = false;
    }
}

u32
Get_number_of_patrols(EnemyType t) {
    u32 r = rand() / (f32)RAND_MAX * (f32)ENEMY_PROPS[t].MAX_PATROLS;
    // printf("N: %i\n", r);
    return r;
}

void
Change_state_to_patrol(Enemy *e) {
    e->ia_data.r_action = -1;
    e->ia_data.state = IA_PATROL;
    e->ia_data.patrolCount = Get_number_of_patrols(e->type);
    e->ia_data.lastDir = e->physics.vel.x;
}


void 
Get_platform_edge(u32 tx, u32 ty, i32 *rltx, i32 *rlty, i32 *rrtx, i32 *rrty) {
    *rrtx = tx;
    *rrty = ty;
    while(*rrtx < GLOBALS.levels[GLOBALS.current_level].tilemap.tiles_x && Is_collisionable(*rrtx, *rrty)) {
        *rrtx = *rrtx+1;
    }
    *rrtx = *rrtx-1;

    *rltx = tx;
    *rlty = ty;
    while(*rltx >= 0 && Is_collisionable(*rltx, *rlty)) {
        *rltx = *rltx-1;
    }
    *rltx = *rltx+1;
}


bool
There_is_a_platform_down(i32 tx, i32 ty) {
    bool result = false;
    while(ty >= 0) {
        result = result || Is_collisionable(tx, ty);
        ty--;
    }
    return result;
}


bool
Is_platform_reachable(u32 tx, u32 ty, i32 dir, u32 distance) {
    dir = dir > 0 ? 1 : -1;
    i32 fx = tx+dir;
    while(fx < GLOBALS.levels[GLOBALS.current_level].tilemap.tiles_x && fx >= 0 && !Is_collisionable(fx, ty)) {
        fx += dir;
    }

    return abs(fx - (i32)tx) <= distance && fx > 0 && fx < GLOBALS.levels[GLOBALS.current_level].tilemap.tiles_x-1;
}


bool
Set_fall(u32 e_tile_x, u32 e_tile_y, Enemy *e) {
    i32 rx, ry, lx, ly;
    Get_platform_edge(e_tile_x, e_tile_y-1, &lx, &ly, &rx, &ry);
    bool left = false;
    bool right = false;
    e->ia_data.r_action = rand()/(f32)RAND_MAX;
    if (lx > 0 && ly > 0) {
        left = There_is_a_platform_down(lx-1, ly);
    }
    if (rx < GLOBALS.levels[GLOBALS.current_level].tilemap.tiles_x-1
        && ry < GLOBALS.levels[GLOBALS.current_level].tilemap.tiles_y-1) {
        right = There_is_a_platform_down(rx+1, ry);
    }
    if (left && right) {
        if (e->ia_data.r_action < 0.5) {
            e->ia_data.dir = 1;
        }
        else {
            e->ia_data.dir = -1;
        }
    }
    else if (left) {
        e->ia_data.dir = -1;
    }
    else if (right) {
        e->ia_data.dir = 1;
    }
    else {
        e->ia_data.r_action = 1;
        return false;
    }
    return true;
}


bool
Set_lateral(Enemy *e) {
    i32 e_tile_x = (e->physics.pos.x + ENEMY_PROPS[e->type].ENEMY_WIDTH/2) / GLOBALS.levels[GLOBALS.current_level].tilemap.width;
    i32 e_tile_y = e->physics.pos.y / GLOBALS.levels[GLOBALS.current_level].tilemap.height;
    i32 p_tile_x = (GLOBALS.player.physics.pos.x + PLAYER_WIDTH/2) / GLOBALS.levels[GLOBALS.current_level].tilemap.width;
    i32 diff_x = p_tile_x - (i32)e_tile_x;

    e->ia_data.r_action = 2;
    i32 rx, ry, lx, ly;
    Get_platform_edge(e_tile_x, e_tile_y-1, &lx, &ly, &rx, &ry);
    i32 ox, oy, dir;
    if (diff_x > 0) {
        ox = rx;
        oy = ry;
        dir = 1;
    }
    else {
        ox = lx;
        oy = ly;
        dir = -1;
    }
    i32 d1 = abs(ox - (i32)e_tile_x);
    i32 d2 = abs((i32)p_tile_x - (i32)e_tile_x);
    // printf("d1: %i, d2: %i, ox: %i, px: %i, ex: %i, dir: %i\n", d1, d2, ox, p_tile_x, e_tile_x, dir);

    if ((d1 < d2) && Is_platform_reachable(ox, oy, dir, ENEMY_JUMP_DISTANCE)) { //IF distance to next platform is less than distance to player
        e->ia_data.dir = dir;
    }
    else {
        return false;
    }
    return true;
}


void
Set_up(Enemy *e) {
    e->ia_data.r_action = 1;
    e->ia_data.patrolCount = 2;
}


//
//IA HANDLERS
//
void
Simple_enemy_update(u32 e_idx) {
    Enemy *e = &GLOBALS.enemies[e_idx];
    switch (e->ia_data.state) {
        case IA_SPAWN:
        {
            if (e->ia_data.spawn_counter <= 0) {
                e->ia_data.state = IA_PATROL;
            }
            else {
                e->ia_data.spawn_counter -= GAMEPLAY_DELTA_TIME;
            }
            break;
        }
        case IA_PATROL:
        {
            if (e->physics.grounded) {
                e->physics.angular = 0;
                e->physics.rotation = 0;
                if (e->ia_data.patrolCount > 0) {
                    Patrol(e);
                    if (e->ia_data.edge_change == 0) {
                        e->ia_data.patrolCount--;
                        e->ia_data.lastDir = e->physics.vel.x;
                        // printf("Menos: %i\n", e->ia_data.patrolCount);
                    }
                }
                else {
                    e->ia_data.state = IA_RELOCATE;
                }
            }
            break;
        }
        case IA_RELOCATE:
        {
            // f32 diff_x = e->physics.pos.x - GLOBALS.player.physics.pos.x;
            // f32 diff_y = e->physics.pos.y - GLOBALS.player.physics.pos.y;
            u32 e_tile_x = (e->physics.pos.x + ENEMY_PROPS[e->type].ENEMY_WIDTH/2) / GLOBALS.levels[GLOBALS.current_level].tilemap.width;
            u32 e_tile_y = e->physics.pos.y / GLOBALS.levels[GLOBALS.current_level].tilemap.height;
            // u32 p_tile_x = e->physics.pos.x / GLOBALS.tilemap.width;
            // u32 p_tile_y = e->physics.pos.y / GLOBALS.tilemap.height;

            if (e_tile_x < 0 || e_tile_y < 0) {
                // printf("WARNING: negative enemy tile position\n");
                return;
            }

            if (e->ia_data.r_action <= 0) {
                e->ia_data.r_action = rand()/(f32)RAND_MAX*2;
                e->ia_data.patrolCount = 2;
                if (e->ia_data.r_action < 1) {
                    Set_fall(e_tile_x, e_tile_y, e);
                }
            }

            if (e->ia_data.r_action >= 2) {
                e->physics.accel.x = ENEMY_PROPS[e->type].ENEMY_ACELL * e->ia_data.dir;
                if (!Is_collisionable(e_tile_x, e_tile_y-1)) { //If reached border jump
                    e->physics.accel.x = 0;
                    e->physics.accel.y = 0;
                    e->physics.vel.y = ENEMY_PROPS[e->type].ENEMY_JUMP_VEL/4;
                    e->physics.vel.x *=200;
                    Change_state_to_patrol(e);
                }
            }
            else if (e->ia_data.r_action >= 1) {
                Patrol(e);
                if (Is_collisionable(e_tile_x, e_tile_y+1)) {
                    e->physics.accel.x = 0;
                    e->physics.vel.x = 0;
                    e->physics.vel.y = ENEMY_PROPS[e->type].ENEMY_JUMP_VEL;
                    Change_state_to_patrol(e);
                }
                else if (e->ia_data.edge_change == 0) {
                    e->ia_data.patrolCount--;
                    e->ia_data.lastDir = e->physics.vel.x;
                    if (e->ia_data.patrolCount <= 0) {
                        if (!Set_fall(e_tile_x, e_tile_y, e)) {
                            if (!Set_lateral(e)) {
                                Change_state_to_patrol(e);
                            }
                        }
                    }
                }
            }
            else if (e->ia_data.r_action >= 0) {
                e->physics.accel.x = ENEMY_PROPS[e->type].ENEMY_ACELL * e->ia_data.dir;
                if (!e->physics.grounded) {
                    e->physics.accel.x = 0;
                    e->physics.vel.x = 0;
                    Change_state_to_patrol(e);
                }
            }
            break;
        }
    }
}


void
Smart_enemy_update(u32 e_idx) {
    Enemy *e = &GLOBALS.enemies[e_idx];
    switch (e->ia_data.state) {
        case IA_SPAWN:
        {
            if (e->ia_data.spawn_counter <= 0) {
                Change_state_to_patrol(e);
            }
            else {
                e->ia_data.spawn_counter -= GAMEPLAY_DELTA_TIME;
            }
            break;
        }
        case IA_PATROL:
        {
            if (e->physics.grounded) {
                if (e->ia_data.patrolCount > 0) {
                    Patrol(e);
                    if (e->ia_data.edge_change == 0) {
                        e->ia_data.patrolCount--;
                        e->ia_data.lastDir = e->physics.vel.x;
                        // printf("Menos: %i\n", e->ia_data.patrolCount);
                    }
                }
                else {
                    e->ia_data.state = IA_RELOCATE;
                }
            }
            break;
        }
        case IA_RELOCATE:
        {
            i32 e_tile_x = (e->physics.pos.x + ENEMY_PROPS[e->type].ENEMY_WIDTH/2) / GLOBALS.levels[GLOBALS.current_level].tilemap.width;
            i32 e_tile_y = e->physics.pos.y / GLOBALS.levels[GLOBALS.current_level].tilemap.height;
            i32 p_tile_x = (GLOBALS.player.physics.pos.x + PLAYER_WIDTH/2) / GLOBALS.levels[GLOBALS.current_level].tilemap.width;
            i32 p_tile_y = GLOBALS.player.physics.pos.y / GLOBALS.levels[GLOBALS.current_level].tilemap.height;
            i32 diff_y = p_tile_y - (i32)e_tile_y;
            
            if (e_tile_x < 0 || e_tile_y < 0 || p_tile_x < 0 || p_tile_y < 0) {
                // printf("WARNING: negative tile position\n");
                return;
            }

            if (e->ia_data.r_action <= 0) {
                if (diff_y > 0) {
                    Set_up(e);
                }
                else if (diff_y < 0) {
                    if (!Set_fall(e_tile_x, e_tile_y, e)) {
                        if (!Set_lateral(e)) {
                            Set_up(e);
                        }
                    }
                }
                else {
                    if (!Set_lateral(e)) {
                        if (!Set_fall(e_tile_x, e_tile_y, e)) {
                            Set_up(e);
                        }
                    }
                }
            }

            if (e->ia_data.r_action >= 2) {
                e->physics.accel.x = ENEMY_PROPS[e->type].ENEMY_ACELL*e->ia_data.dir;
                if (!Is_collisionable(e_tile_x, e_tile_y-1)) { //If reached border jump
                    e->physics.accel.x = 0;
                    e->physics.accel.y = 0;
                    e->physics.vel.y = ENEMY_PROPS[e->type].ENEMY_JUMP_VEL/4;
                    e->physics.vel.x *=200;
                    Change_state_to_patrol(e);
                }
            }
            else if (e->ia_data.r_action >= 1) {
                Patrol(e);
                if (Is_collisionable(e_tile_x, e_tile_y+1)) {
                    e->physics.accel.x = 0;
                    e->physics.vel.x = 0;
                    e->physics.vel.y = ENEMY_PROPS[e->type].ENEMY_JUMP_VEL;
                    Change_state_to_patrol(e);
                }
                else if (e->ia_data.edge_change == 0) {
                    e->ia_data.patrolCount--;
                    e->ia_data.lastDir = e->physics.vel.x;
                    if (e->ia_data.patrolCount <= 0) {
                        if (!Set_lateral(e)) {
                            if (!Set_fall(e_tile_x, e_tile_y, e)) {
                                Change_state_to_patrol(e);
                            }
                        }
                    }
                }
            }
            else if (e->ia_data.r_action >= 0) {
                e->physics.accel.x = ENEMY_PROPS[e->type].ENEMY_ACELL * e->ia_data.dir;
                if (!e->physics.grounded) {
                    e->physics.accel.x = 0;
                    e->physics.vel.x = 0;
                    Change_state_to_patrol(e);
                }
            }
            break;
        }
    }
}
