#include "collisions.h"

bool
Is_collisionable(u32 tile_x, u32 tile_y) {
    u32 tn = Tilemap_get_tile(&GLOBALS.levels[GLOBALS.current_level].tilemap, COLLISIONABLE_LAYER, tile_x, tile_y);
    // printf("N: %i\n", tn);
    return tn > 0;
}


bool
Point_on_collisionable_tile(Vec2 point) {
	if (point.x < 0.0f || point.y < 0.0f) {
		return false;
	}
    u32 tile_x = point.x / GLOBALS.levels[GLOBALS.current_level].tilemap.width;
    u32 tile_y = point.y / GLOBALS.levels[GLOBALS.current_level].tilemap.height;
	if (tile_x >= GLOBALS.levels[GLOBALS.current_level].tilemap.tiles_x || tile_y >= GLOBALS.levels[GLOBALS.current_level].tilemap.tiles_y) {
		return false;
	}
    if (Is_collisionable(tile_x, tile_y)) {
		return true;
	}
	return false;
}


bool
Check_tilemap_collision(Vec2 *position, Vec2 speed, f32 *new_y) {
	if (position->x < 0.0f || position->y < 0.0f) {
		return false;
	}
    u32 tile_x = position->x / GLOBALS.levels[GLOBALS.current_level].tilemap.width;
    u32 tile_y = position->y / GLOBALS.levels[GLOBALS.current_level].tilemap.height;
    u32 l_tile_y = (position->y - speed.y * GAMEPLAY_DELTA_TIME) / GLOBALS.levels[GLOBALS.current_level].tilemap.height;
	if (tile_x >= GLOBALS.levels[GLOBALS.current_level].tilemap.tiles_x 
        || tile_y >= GLOBALS.levels[GLOBALS.current_level].tilemap.tiles_y 
        || l_tile_y >= GLOBALS.levels[GLOBALS.current_level].tilemap.tiles_y) {
		return false;
	}

    if (Is_collisionable(tile_x, tile_y) && !Is_collisionable(tile_x, l_tile_y)) {
        if (speed.y <= 0) { //Check if going down
            *new_y = (tile_y+1) * GLOBALS.levels[GLOBALS.current_level].tilemap.height;
            return true;
        }
    }
    return false;
}

void
Apply_tilemap_collision(Physics *phy) {
    f32 ry, ly;
    Vec2 lf = V2_add(phy->pos, phy->left_foot_pos);
    phy->left_foot = Check_tilemap_collision(&lf, phy->vel, &ly);
    Vec2 rf = V2_add(phy->pos, phy->right_foot_pos);
    phy->right_foot = Check_tilemap_collision(&rf, phy->vel, &ry);

    if (phy->right_foot) {
        phy->pos.y = ry - phy->right_foot_pos.y;
        phy->vel.y = 0.0f;
        phy->grounded = true;
    }
    else if (phy->left_foot) {
        phy->pos.y = ly - phy->left_foot_pos.y;
        phy->vel.y = 0.0f;
        phy->grounded = true;
    }
	else {
        phy->grounded = false;
	}
}
