#version 100

varying mediump vec2 f_tex_coord;
uniform sampler2D texture;
uniform mediump vec4 color;

void main()
{
	mediump float alpha = texture2D(texture, f_tex_coord).r;
    gl_FragColor = vec4(color.rgb, color.a*alpha);
}


