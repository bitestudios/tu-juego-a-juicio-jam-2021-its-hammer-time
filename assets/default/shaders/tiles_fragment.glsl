#version 100

varying mediump vec2 f_text_coord;
uniform sampler2D texture;


void main()
{
    gl_FragColor = texture2D(texture, f_text_coord);
}

