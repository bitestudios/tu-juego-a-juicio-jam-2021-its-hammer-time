#version 100

varying mediump vec2 f_text_coord;
uniform sampler2D texture;
uniform mediump vec4 color;

void main()
{
    gl_FragColor = color * texture2D(texture, f_text_coord);
}

