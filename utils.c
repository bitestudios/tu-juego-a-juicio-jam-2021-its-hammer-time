
#include "game.h"
#include "engine/mesh-loader.h"


// Calculates a valid centered viewport based on the defined aspect ratio
void
Calculate_viewport(u32 w_width, u32 w_height, f32 *vpx, f32 *vpy, f32 *vpw, f32 *vph) {

	u32 vpwi = w_width;
	u32 vphi = w_height;

	u32 vpxi = 0;
	u32 vpyi = 0;

	// Calculate the possible width based on the height
	u32 possible_width = w_height * ASPECT_RATIO_W / ASPECT_RATIO_H;

	// The window is wider 
	if ((u32)possible_width <= w_width) {
		vpwi = possible_width;
		vpxi = (w_width - vpwi) / 2;
	}
	else { // The window is higher
		vphi = w_width * ASPECT_RATIO_H / ASPECT_RATIO_W;
		vpyi = (w_height - vphi) / 2;
	}

	*vpx = (f32)vpxi;
	*vpy = (f32)vpyi;
	*vpw = (f32)vpwi;
	*vph = (f32)vphi;
}

void
Get_mouse_on_game_screen_coords(f32 *x_out, f32 *y_out) {
	*x_out = ((f32)app_data.mouse.x - GLOBALS.viewport_x) * ((f32)GAMEPLAY_WIDTH/GLOBALS.viewport_w);
	*y_out = ((f32)(app_data.w_height - app_data.mouse.y) - GLOBALS.viewport_y) * ((f32)GAMEPLAY_HEIGHT/GLOBALS.viewport_h);
}

static inline f32
det_2D(Vec2 p0, Vec2 p1, Vec2 p2) {
	return p0.x * (p1.y - p2.y) + p1.x * (p2.y - p0.y) + p2.x * (p0.y - p1.y);
}


bool
Triangles_overlap(Vec2 * t0, Vec2 * t1) {
	for (u8 i = 0; i < 3; ++i) {
		u8 j = (i+1)%3;

		if (det_2D(t0[i], t0[j], t1[0]) <= 0 &&
			det_2D(t0[i], t0[j], t1[1]) <= 0 &&
			det_2D(t0[i], t0[j], t1[2]) <= 0) {
			return false;
		}

		if (det_2D(t1[i], t1[j], t0[0]) <= 0 &&
			det_2D(t1[i], t1[j], t0[1]) <= 0 &&
			det_2D(t1[i], t1[j], t0[2]) <= 0) {
			return false;
		}
	}
	return true;
}


void
Transform_triangle(Vec2 v0, Vec2 v1, Vec2 v2, f32 scale_x, f32 scale_y, f32 x, f32 y, f32 rotation, Vec2 *v0_out, Vec2 *v1_out, Vec2 *v2_out) {
	Mat2 rot_mat = M2_rotation(Radians(rotation));
	*v0_out = V2_add(Mul_v2_m2(V2_mul(v0, V2(scale_x, scale_y)), rot_mat), V2(x, y));
	*v1_out = V2_add(Mul_v2_m2(V2_mul(v1, V2(scale_x, scale_y)), rot_mat), V2(x, y));
	*v2_out = V2_add(Mul_v2_m2(V2_mul(v2, V2(scale_x, scale_y)), rot_mat), V2(x, y));
}

void
Draw_bounding_mesh(const BoundingMesh *bm, f32 scale_x, f32 scale_y, f32 x,f32 y, f32 rotation, Vec4 color) {
	Assert((bm->total_verts%3) == 0, "");
	u32 n_triangles = bm->total_verts/3;
	for (u32 triangle_i = 0; triangle_i < n_triangles; ++triangle_i) {
		u32 v0_i = triangle_i * 3 + 0;
		u32 v1_i = triangle_i * 3 + 1;
		u32 v2_i = triangle_i * 3 + 2;
		Vec2 v0, v1, v2;
		Transform_triangle(
			bm->vertices[v0_i], bm->vertices[v1_i], bm->vertices[v2_i],
			scale_x, scale_y, x, y, rotation,
			&v0, &v1, &v2);
		Draw_triangle(
			HMM_Vec3(v0.x, v0.y, 0.0f),
			HMM_Vec3(v1.x, v1.y, 0.0f),
			HMM_Vec3(v2.x, v2.y, 0.0f),
			HMM_Vec4(color.r, color.g, color.b, color.a)
		);
	}
}


