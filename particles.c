	
#include "particles.h"

ParticlesSource
Make_particles_source(const ParticlesDesc *desc, Vec2 pos, u64 seed) {
	ParticlesSource result;
	result.particles_desc = desc;
	result.time = 0.0f;
	result.pos  = pos;
	result.seed = seed;
	return result;
}

bool
Show_particles(ParticlesSource *particles, f32 dt, ShowParticlesRegenerate regenerate) {
	const ParticlesDesc *desc = particles->particles_desc;
	if (desc == NULL) return false;

	f32 global_t = particles->time;
	u64 seed = particles->seed;
	f32 source_x = particles->pos.x;
	f32 source_y = particles->pos.y;

	u32 max_particles = desc->max_particles;
	f32 min_vel = desc->min_vel;
	f32 max_vel = desc->max_vel;
	f32 min_size = desc->min_size;
	f32 max_size = desc->max_size;
	f32 min_spawn_radius = desc->min_spawn_radius;
	f32 max_spawn_radius = desc->max_spawn_radius;
	f32 min_spawn_rate = desc->min_spawn_rate;
	f32 max_spawn_rate = desc->max_spawn_rate;
	f32 particle_min_lifetime = desc->particle_min_lifetime;
	f32 particle_max_lifetime = desc->particle_max_lifetime;

	f32 inv_fade_in  = 1.0f / desc->fade_in_time;
	f32 inv_fade_out = 1.0f / desc->fade_out_time;

	Vec4 color_0 = desc->color_0;
	Vec4 color_1 = desc->color_1;

	Vec2 global_accel    = desc->global_accel;
	f32  global_max_vel = desc->global_max_vel;
	
	f32 start_angle = Radians(desc->start_angle);
	f32 end_angle   = Radians(desc->end_angle);
	f32 total_angle = end_angle - start_angle;
	f32 angle_step = total_angle/(f32)(max_particles);
	
	// This is only for the (no regenerate) case to check at the end if all the particles finalized
	u32 particles_alive = 0;

	for (int i = 0; i < max_particles; ++i) {
		
		// Calculate all the lerp factors
		f32 lerp_spawn_rate        = wy2u01(wyrand(&seed));
		f32 lerp_particle_lifetime = wy2u01(wyrand(&seed));
		f32 lerp_vel               = wy2u01(wyrand(&seed));
		f32 lerp_color             = wy2u01(wyrand(&seed));
		f32 lerp_size              = wy2u01(wyrand(&seed));
		f32 lerp_spawn_radius      = wy2u01(wyrand(&seed));


		f32 particle_start_time = Lerp(min_spawn_rate, max_spawn_rate, lerp_spawn_rate);
		f32 particle_lifetime   = Lerp(particle_min_lifetime, particle_max_lifetime, lerp_particle_lifetime);

		f32 t;
		if (regenerate == SHOW_PARTICLES_REGENERATE_YES) {
			// When the partice dies we restart everything
			t = fmodf(global_t, (particle_start_time + particle_lifetime)) - particle_start_time;
		}
		else { // No regenerate particles mode
			t = global_t - particle_start_time;
			if (t <= particle_lifetime) {
				++particles_alive;
			}
		}
		if (t < 0 || t > particle_lifetime) continue;

		f32 dir = (f32)i*angle_step+start_angle;
		f32 cos_dir = cosf(dir);
		f32 sin_dir = sinf(dir);
		Vec2 vel_v2;
		{ // Calculate the velocity for the particle
			f32 vel = Lerp(min_vel,  max_vel, lerp_vel);
			vel_v2 = V2(cos_dir * vel, sin_dir * vel);
			vel_v2 = V2_add(vel_v2, V2_mulf(global_accel, t)); // Apply the global acceleration
			vel = V2_len(vel_v2);
			f32 clamped_vel = Clamp(vel, -global_max_vel, global_max_vel);
			vel_v2 = V2_mulf(vel_v2, clamped_vel/vel);
		}

		f32 x, y;
		{ // Calculate the y and x
			f32 spawn_radius = Lerp(min_spawn_radius, max_spawn_radius, lerp_spawn_radius);
			x = (cos_dir * spawn_radius + source_x + vel_v2.x * t);
			y = (sin_dir * spawn_radius + source_y + vel_v2.y * t);
		}

		// Color
		Vec4 color = V4_lerp(color_0, color_1, lerp_color);

		// Size
		f32 s = Lerp(min_size, max_size, lerp_size);


		f32 fade_alpha;
		{ // Calculate fade alpha
			f32 in_a = Min(t*inv_fade_in, 1.0f);
			f32 out_a = Min(((particle_lifetime-t)*inv_fade_out), 1.0f);
			fade_alpha = in_a * out_a;
		}


		Draw_quad(
			HMM_Vec3(x-1.0f*s, y-1.0f*s, 0.0f),
			HMM_Vec3(x+1.0f*s, y-1.0f*s, 0.0f),
			HMM_Vec3(x+1.0f*s, y+1.0f*s, 0.0f),
			HMM_Vec3(x-1.0f*s, y+1.0f*s, 0.0f),
			HMM_Vec4(color.r, color.g, color.b, color.a * fade_alpha));
	}
	particles->time += dt;

	if (regenerate == SHOW_PARTICLES_REGENERATE_NO && particles_alive == 0) {
		return false;
	}

	return true;
}


