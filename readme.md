# Tu Juego A Juicio jam 2021 - It's Hammer Time!

## Compile for windows

We only crosscompiled from linux, so we don't know if this will compile correctly form windows.

To crosscompile easily from linux you can use zig.

Debug

```
zig cc main.c engine/third_party/third_party.c -target x86_64-windows-gnu -g -lkernel32 -luser32 -lgdi32 -lole32 -o its-hammer-time.exe
```

Release

```
zig cc main.c engine/third_party/third_party.c .gen/bundled_tree.c -target x86_64-windows-gnu -DBUNDLED_TREE -Oz -lkernel32 -luser32 -lgdi32 -lole32 -o its-hammer-time.exe
```

## Compile for linux

This was tested with clang, gcc and 'zig cc'. You can replace cc by your compiler program.

Debug build 

```
cc main.c engine/third_party/third_party.c -g -Wall -lpthread -lasound -lEGL -lGLESv2 -lm -lX11 -lXi -lXcursor -o its-hammer-time
```

Release build 

```
cc main.c engine/third_party/third_party.c .gen/bundled_tree.c -DBUNDLED_TREE -Os -Wall -lpthread -lasound -lEGL -lGLESv2 -lm -lX11 -lXi -lXcursor -o its-hammer-time
```

## Compile web version

You can compile using 'zig cc' or clang if you setup the wasi libc manually.

Debug build 

```
zig cc main.c engine/third_party/third_party.c .gen/bundled_tree.c -target wasm32-wasi -DWASM -g -Wall -o its-hammer-time.wasm
```

Release build 

```
zig cc main.c engine/third_party/third_party.c .gen/bundled_tree.c -target wasm32-wasi -DBUNDLED_TREE -DWASM -Os -Wall -o its-hammer-time.wasm
```

You have to put index.html, space-dizziness.wasm and engine/loader.js in the same folder and serve it with a http server.

