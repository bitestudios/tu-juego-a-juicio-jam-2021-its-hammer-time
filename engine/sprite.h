#ifndef _SPRITE_H_
#define _SPRITE_H_

#include "base.h"
#include "platform.h"
#include "third_party/stb_image.h"
#include "basic_gl.h"
#include "third_party/HandmadeMath.h"

#define FLATTEN_IDX(x, y, width) x + y * width
#define NORMALIZE(pos, max) (pos / (f32)max) * 2 + 1
#define RGB_DEFAULT V4(1.0f, 1.0f, 1.0f, 1.0f)

typedef struct {
    GLuint id;
    i32 width;
    i32 height;
    i32 channels;
    char* filename;
} Texture;

typedef struct {
    f32 vx; //Position coordinates
    f32 vy;
    f32 tx; //Texture coordinates
    f32 ty;
} Vertex_object;

typedef struct {
    struct {
        Vertex_object left_down;
        Vertex_object left_up;
        Vertex_object right_up;
    } triangle1;
    
    struct {
        Vertex_object left_down;
        Vertex_object right_down;
        Vertex_object right_up;
    } triangle2;
} Square_vertex_data;


// This defines a group of sprites with the same width/height in the same texture
// this is also used as tileset
typedef struct {
	u32 sprite_width;
	u32 sprite_height;
	Texture texture;
} Spriteset;

typedef struct {
    u32 init_x;
    u32 init_y;
    u32 width;
    u32 height;
    u32 n_layers;
    u32 tiles_x;
    u32 tiles_y;
    f32 scroll_x;
    f32 scroll_y;
    u32 total_tilemap_width;
    u32 total_tilemap_height;
    
	bool render_data_generated;
    GLuint vertex_buffer;
    Square_vertex_data *vertex_data; //Array of squares forming the tiles of the tilemap
    u32 *tilemap_data;
    i32 tilemap_size;
    i32 real_size;
	Spriteset *spriteset;
} Tilemap;



typedef struct {
    const char *name;
    const char *value;
} Attribute;

typedef struct {
    f32 x, y, width, height, rotation, vx, vy, vr;
    char name[50];
    Attribute *attributes;
    u32 attributes_size;
} Object;

typedef struct {
    char *name;
    Texture texture;
} Texture_set;


/*
* Init_sprites
* ===================================================================================
*
* DESCRIPTIION
* -------------------------------------------------------------------------------------
* Initializes the render. Necesary to call once at the start of the application.
*
* RETURNS
* -------------------------------------------------------------------------------------
*   i32: Returns 0 if everithing went OK. Returns > 0 otherwise.
*/
i32
Init_sprites();


/*
* Free_objects
* ===================================================================================
*
* DESCRIPTIION
* -------------------------------------------------------------------------------------
* Frees the memory of the object array
*
* PARAMETERS
* -------------------------------------------------------------------------------------
*   obj: Object array
*/
void
Free_objects(Object *obj,  u32 objects_size);


/*
* Load_objects_from_tiled_json
* ===================================================================================
*
* DESCRIPTIION
* -------------------------------------------------------------------------------------
* Loads a object map from tiled
*
* PARAMETERS
* -------------------------------------------------------------------------------------
*   filename: file name of the map.
*   set: Texture set
*   set_size: size of the texture set
*   gameplay_height: width of the gamplay y axis
*   objects_size: Pointer to get the size of the result array
*
* RETURNS
* -------------------------------------------------------------------------------------
*   Object*: Array of objects loaded
*/
Object*
Load_objects_from_tiled_json(const char *filename, u32 gameplay_height, u32 *objects_size);


/*
* Get_tile_number
* ===================================================================================
*
* DESCRIPTIION
* -------------------------------------------------------------------------------------
* Gets a tile using its x and y position.
*
* PARAMETERS
* -------------------------------------------------------------------------------------
*   tile_x, tile_y: position
*   tilemap: Tilemap
*
* RETURNS
* -------------------------------------------------------------------------------------
*   U32: Number of the tile
*/
u32
Get_tile_number(u32 tile_x, u32 tile_y, Tilemap *tilemap);


/*
* Get_texture_by_name
* ===================================================================================
*
* DESCRIPTIION
* -------------------------------------------------------------------------------------
* Gets the texture pointer by name from the texture set
*
* PARAMETERS
* -------------------------------------------------------------------------------------
*   nmb: Name of the object.
*   set: Texture set
*   set_size: size of the texture set
*
* RETURNS
* -------------------------------------------------------------------------------------
*   Texture*: Pointer to the correspondant texture
*/
Texture*
Get_texture_by_name(const char *nmb, Texture_set *set, u32 set_size);


/*
* Make_spriteset
* ===================================================================================
*
* DESCRIPTIION
* -------------------------------------------------------------------------------------
* Creates a spriteset using the image filename passed as parameter.
*
* To release all the spriteset resources we must call Free_spriteset.
*
* PARAMETERS
* -------------------------------------------------------------------------------------
*   image_filename: The filename of the image that will be set as texture.
*   tile_width:  Width of each tile in pixels
*   tile_height: Height of each tile in pixels
*   spriteset_result: Pointer to the spriteset to make
*
* RETURNS
* -------------------------------------------------------------------------------------
*   int: If is != 0 The image couldn't be loaded or the tile widths donn't match.
*/
int
Make_spriteset(const char *image_filename, u32 tile_width, u32 tile_height, bool linear_interpolation, Spriteset *spriteset_result);





/*
* Free_spriteset
* ===================================================================================
*
* DESCRIPTIION
* -------------------------------------------------------------------------------------
* Releases all the resources grabbed by the spriteset (Just the texture)
*
* PARAMETERS
* -------------------------------------------------------------------------------------
*   spriteset: Spriteset to free
*
*/
void
Free_spriteset(Spriteset *spriteset);


/*
* DESCRIPTIION
* -------------------------------------------------------------------------------------
* Combines tilemaps two tilemaps into one. t1 and t2 memory are freed, so the become null.
* WARNING: tile and screen sizes might not be correct, as well as initial position
* if they have diferent sizes and initial positions.
*
* PARAMETERS
* -------------------------------------------------------------------------------------
*   t1: first tilemap
*   t2: second tilemap
*
* RETURNS
* -------------------------------------------------------------------------------------
*   Tilemap: sctruct containing tilemap data combined. Widths and Heights on the result
*            tilemap are the bigger of the two. Initial position is left with the lower
*            values.
*/
Tilemap*
Combine_tilemaps(Tilemap *t1, Tilemap *t2);


/*
* DESCRIPTIION
* -------------------------------------------------------------------------------------
* Makes a tilemap
*
* Note that the tilemap isn't ready to draw, you must call Tilemap_gen_render_data
* to generate the resources needed to be rendered.
*
* The tilemap holds resources that should be released calling Free_tilemap
*
* PARAMETERS
* -------------------------------------------------------------------------------------
*   init_x: Most left position of the screen to start drawing.
*   init_y: Most right position of the screen to start drawing.
*   width: Width of a tile in world coordinates.
*   height: Height of a tile in world coordinates.
*   n_layers: The number of layers that will have the tilemap
*   tiles_x: Number of tiles on the x axis. (Width of the tilemap in tiles)
*   tiles_y: Number of tiles on the y axis. (Heigth of the tilemap in tiles)
*
*
* RETURNS
* -------------------------------------------------------------------------------------
*   Tilemap: the tilemap :)
*/
Tilemap
Make_tilemap(i32 init_x, i32 init_y, u32 width, u32 height, u32 n_layers, u32 tiles_x, u32 tiles_y);



/*
* Tilemap_set_tile
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Sets the value of 1 tile on the tilemap
*
* VARIABLES
* -------------------------------------------------------------------
* tilemap: Tilemap to use
* layer: Layer where is the tile
* x: Coordinate
* y: Coordinate
* tile: Tile value
*
*/
void
Tilemap_set_tile(Tilemap *tilemap, u32 layer, u32 x, u32 y, u32 tile);




/*
* Tilemap_get_tile
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Gets the value of 1 tile on the tilemap
*
* VARIABLES
* -------------------------------------------------------------------
* tilemap: Tilemap to use
* layer: Layer where is the tile
* x: Coordinate
* y: Coordinate
*
* RETURNS
* -------------------------------------------------------------------------------------
*   u32: The value of the tile
*/
u32
Tilemap_get_tile(Tilemap *tilemap, u32 layer, u32 x, u32 y);


/*
* Tilemap_gen_render_data
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Generates the render data for a tilemap, the tilemaps should be previously
* initialized with Make_tilemap and the spriteset with Make_spriteset.
*
* This function must be called only once with the same tilemap.
*
* VARIABLES
* -------------------------------------------------------------------
* tilemap: pointer to the tilemap
* spriteset: pointer to the spriteset
*
*/
void
Tilemap_gen_render_data(Tilemap *tilemap, Spriteset *spriteset);


/*
* Load_tilemap_from_tiled_json
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Loads a tilemap from a json generated with tiled
*
* The tilemap holds resources that should be released calling Free_tilemap
*
* VARIABLES
* -------------------------------------------------------------------
* filename: The json file to load
* init_x: initial position of the tilemap
* init_y: initial position of the tilemap
* screen_tile_width: tile width when displayed. 0 for default (use tilemap size)
* screen_tile_height: tile height when displayed. 0 for default (use tilemap size)
* spriteset: spriteset to use
* tilemap_result: Pointer to the resulting tilemap
*
* RETURNS
* -------------------------------------------------------------------
* int: != 0 on failure
*
*/
Object*
Load_tilemap_from_tiled_json(const char *filename, i32 init_x, i32 init_y, 
    u32 screen_tile_width, u32 screen_tile_height, Spriteset *spriteset, 
    Tilemap *tilemap_result, u32 gameplay_height, u32 *objects_size);



/*
* DESCRIPTIION
*   Sets up the buffer for the vertex attributes neded for the sprite shaders
*/
void
Setup_Buffers();



/*
* DESCRIPTIION
* --------------------------------------------------------------------------------------
* Prepares a texture using its filename.
*
* Free_texture must be call in order to release the resource
*
* VARIABLES
* --------------------------------------------------------------------------------------
* filename: Name of the image of the texture
* linear_interpolation: If you want to use GL_LINEAR or GL_NEAREST
* texture:  The texture to setup
*
* RETURNS
* -------------------------------------------------------------------------------------
* int: != 0 in case of error
*
*/
int
Setup_texture(const char *filename, bool linear_interpolation, Texture *texture);



/*
* DESCRIPTIION
* --------------------------------------------------------------------------------------
* Releases the opengl texture
*
* VARIABLES
* --------------------------------------------------------------------------------------
* texture: The texture to free
*
*/
void
Free_texture(Texture *texture);





/*
* DESCRIPTIION
*   Reads compiles and loads the shaders needed for the sprites.
*
* RETURNS
*   int: 0 if went succesfully. -1 if process failed.
*/
static int
Setup_shaders();

/*
* DESCRIPTIION
* --------------------------------------------------------------------------------------
* Draws the sprite indicated by the sprite_number of the texture. Draws it in a box defined by the
* parameters. Takes into account aspect ratio deformations.
*
* PARAMETERS
* -------------------------------------------------------------------------------------
*   x: Position in the X axis in world coordinates
*   y: Position in the Y axis in world coordinates
*   width: Width in world coordinates.
*   heigth: Heigth in world coordinates
*   rotation: Degrees the sprite is rotated. The rotation point its the center of the sprite.
*   texture_id: Id of the texture of the sprite.
*   sprite_width: Width of a single sprite in the original image.
*   sprite_height: Height of a single sprite in the original image.
*   sprite_number: Index of the sprite to draw. Starting by 0. If multiple dimensions, counts left to right, and top to bottom.
*/
void
Draw_sprite(f32 x, f32 y, f32 width, f32 height, f32 rotation, Texture *texture, u32 sprite_number, hmm_vec4 rgb_color);

/*
* DESCRIPTIION
* --------------------------------------------------------------------------------------
* Draws a tilemap
*
* PARAMETERS
* -------------------------------------------------------------------------------------
*   tilemap: Tilemap struct data to draw.
*/
void 
Draw_tilemap(Tilemap *tilemap);

/*
* DESCRIPTIION
* --------------------------------------------------------------------------------------
* Frees the memory used by a Tilemap. Not calling it causes a leak.
*
* PARAMETERS
* -------------------------------------------------------------------------------------
*   tilemap: Tilemap struct data.
*/
void
Free_tilemap(Tilemap *tilemap);

/*
* DESCRIPTIION
* --------------------------------------------------------------------------------------
* Cleans and frees OpenGL memory used by the shaders
*/
void
Sprite_cleanup();

/*
* DESCRIPTIION
* --------------------------------------------------------------------------------------
* Scrolls the tilemap by the indicated amount
*
* PARAMETERS
* -------------------------------------------------------------------------------------
*   x: Scroll on the x axis.
*   y: Scroll on the y axis.
*   tilemap: Tilemap to be scrolled.
*/
void
Scroll_tilemap(f32 x, f32 y, Tilemap* tilemap);


static struct {
    f32 vertex_array[24];

    struct {
        GLuint program_id;
        GLuint vertex_buffer;
        GLint vertex, texture_loc, projection_loc, model_loc, v_text_coord, color_loc; //uniforms
    } shaders;
} sprite_data = {
    .vertex_array = { 
        // pos      // tex
        0.0f, 0.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 0.0f
    }
};


static struct {
    GLuint program_id;
    GLint vertex_loc, texture_loc, v_text_coord, scroll_x_loc, scroll_y_loc;
} tiles_data;


//Documented above
i32
Init_sprites() {
    Setup_Buffers();
    return Setup_shaders();
}


//Documented above
void
Setup_Buffers() {
    glGenBuffers(1, &sprite_data.shaders.vertex_buffer);

    //Vertex atribute definitions
    glBindBuffer(GL_ARRAY_BUFFER, sprite_data.shaders.vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sprite_data.vertex_array), sprite_data.vertex_array, GL_STATIC_DRAW);
}


//Documented above
int
Setup_texture(const char *filename, bool linear_interpolation, Texture *texture) {
	u32 image_data_size;
	u8 *image_data = Get_file_contentsz(filename, &image_data_size);
	if (image_data == NULL) {
		fprintf(stderr, "Cannot load file %s\n", filename);
		return -1;
	}
    unsigned char *sprite = stbi_load_from_memory(image_data, image_data_size, &texture->width, &texture->height, &texture->channels, STBI_rgb_alpha);
    if (sprite == NULL) {
        fprintf(stderr, "Couldnt load texture\n");
		Free_file_contents(image_data);
		return -1;
    }
	Free_file_contents(image_data);

    glGenTextures(1, &texture->id);
    glBindTexture(GL_TEXTURE_2D, texture->id);
	if (linear_interpolation) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // On webgl we need a pow
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // of 2 texture or set this
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture->width, texture->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, sprite);
    //glGenerateMipmap(GL_TEXTURE_2D);

    stbi_image_free(sprite);

	return 0;
}


//Documented above
void
Free_texture(Texture *texture) {
	glDeleteTextures(1, &texture->id);
	memset(texture, 0, sizeof(Texture));
}

//Documented above
static int
Setup_shaders() {
	if (0 > Make_program(&sprite_data.shaders.program_id, "assets/default/shaders/sprite_vertex.glsl", "assets/default/shaders/sprite_fragment.glsl")) {
        fprintf(stderr, "Unable to load and build sprite shader programs.");
        return -1;
    }
	
	// now get the locations (kind of handle) of the shaders variables
    // Vertex location
	sprite_data.shaders.vertex = glGetAttribLocation(sprite_data.shaders.program_id, "vertex");
	if (sprite_data.shaders.vertex < 0) {
		fprintf(stderr, "Unable to get vertex attrib location\n");
		// Renderer_cleanup();  //TODO
		return -1;
	} 
    // Texture coordinates location
	sprite_data.shaders.v_text_coord = glGetAttribLocation(sprite_data.shaders.program_id, "v_text_coord");
	if (sprite_data.shaders.v_text_coord < 0) {
		fprintf(stderr, "Unable to get v_text_coord attrib location\n");
		// Renderer_cleanup();  //TODO
		return -1;
	} 
	// sprite_data.shaders.position_loc  = glGetAttribLocation(sprite_data.shaders.program_id, "vertex");
	// if (sprite_data.shaders.position_loc < 0) {
	// 	fprintf(stderr, "Unable to get vertex uniform location\n");
	// 	return -1;
	// }
    sprite_data.shaders.texture_loc    = glGetUniformLocation(sprite_data.shaders.program_id, "texture");
	if (sprite_data.shaders.texture_loc < 0) {
		fprintf(stderr, "Unable to get texture uniform location\n");
		return -1;
	}
    sprite_data.shaders.projection_loc    = glGetUniformLocation(sprite_data.shaders.program_id, "projection");
	if (sprite_data.shaders.projection_loc < 0) {
		fprintf(stderr, "Unable to get projection uniform location\n");
		return -1;
	}
    sprite_data.shaders.model_loc    = glGetUniformLocation(sprite_data.shaders.program_id, "model");
	if (sprite_data.shaders.model_loc < 0) {
		fprintf(stderr, "Unable to get model uniform location\n");
		return -1;
	}
    sprite_data.shaders.color_loc    = glGetUniformLocation(sprite_data.shaders.program_id, "color");
	if (sprite_data.shaders.color_loc < 0) {
		fprintf(stderr, "Unable to get color uniform location\n");
		return -1;
	}

    // Setup tileset shaders
    if (0 > Make_program(&tiles_data.program_id, "assets/default/shaders/tiles_vertex.glsl", "assets/default/shaders/tiles_fragment.glsl")) {
        fprintf(stderr, "Unable to load and build tiles shader programs.");
        return -1;
    }

    tiles_data.vertex_loc = glGetAttribLocation(tiles_data.program_id, "vertex");
	if (tiles_data.vertex_loc < 0) {
		fprintf(stderr, "Unable to get vertex attrib location\n");
		// Renderer_cleanup();  //TODO
		return -1;
	} 
    // Texture coordinates location
	tiles_data.v_text_coord = glGetAttribLocation(tiles_data.program_id, "v_text_coord");
	if (tiles_data.v_text_coord < 0) {
		fprintf(stderr, "Unable to get v_text_coord attrib location\n");
		// Renderer_cleanup();  //TODO
		return -1;
	} 
    tiles_data.texture_loc    = glGetUniformLocation(tiles_data.program_id, "texture");
	if (tiles_data.texture_loc < 0) {
		fprintf(stderr, "Unable to get texture uniform location\n");
		return -1;
	}
    tiles_data.scroll_x_loc    = glGetUniformLocation(tiles_data.program_id, "scroll_x");
	if (tiles_data.scroll_x_loc < 0) {
		fprintf(stderr, "Unable to get scroll_x uniform location\n");
		return -1;
	}
    tiles_data.scroll_y_loc    = glGetUniformLocation(tiles_data.program_id, "scroll_y");
	if (tiles_data.scroll_y_loc < 0) {
		fprintf(stderr, "Unable to get scroll_y uniform location\n");
		return -1;
	}

	return 0;
}


void
Calculate_texture_coordinates(f32 *vertex_array, Texture *texture, u32 sprite_width, u32 sprite_height, u32 sprite_number) {
    Assert(texture->width % sprite_width == 0, "ERROR: Texture width does not match sprite width.");
    Assert(texture->height % sprite_height == 0, "ERROR: Texture heigth does not match sprite height.");

    u32 tex_count_x = texture->width / sprite_width;
    // u32 tex_count_y = texture->height / sprite_height;

    u32 tx = sprite_number % tex_count_x;
    u32 ty = sprite_number / tex_count_x;
    f32 start_x = tx * sprite_width; //start of the sprite in the texture in pixels
    f32 start_y = ty * sprite_height;
    f32 x0 = (start_x + 0.5f) / (f32)(texture->width);
    f32 x1 = (start_x + (f32)(sprite_width) - 0.5f) / (f32)(texture->width);
    f32 y0 = (start_y + 0.5f) / (f32)(texture->height);
    f32 y1 = (start_y + (f32)(sprite_height) - 0.5f) / (f32)(texture->height);

    //     // pos      // tex
    // 0.0f, 0.0f, 0.0f, 1.0f,
    // 0.0f, 1.0f, 0.0f, 0.0f,
    // 1.0f, 0.0f, 1.0f, 1.0f,
    // 1.0f, 1.0f, 1.0f, 0.0f
    vertex_array[2] = x0;
    vertex_array[3] = y1;
    vertex_array[6] = x0;
    vertex_array[7] = y0;
    vertex_array[10] = x1;
    vertex_array[11] = y1;
    vertex_array[14] = x1;
    vertex_array[15] = y0;
}


//Documented above
void
Draw_sprite_ex(f32 x, f32 y, f32 width, f32 height, f32 rotation, f32 center_x, f32 center_y, Spriteset *spriteset, u32 sprite_idx, Vec4 color) {
    //--------  Calculate texture coordinates
    Calculate_texture_coordinates(sprite_data.vertex_array, &spriteset->texture, spriteset->sprite_width, spriteset->sprite_height, sprite_idx);

    //---------  Draw sprite
    glUseProgram(sprite_data.shaders.program_id);

    //Scale
    hmm_mat4 model = HMM_Scale(HMM_Vec3(width, height, 1.0f));

    //Rotate
    model = HMM_MultiplyMat4(HMM_Translate(HMM_Vec3(-width*center_x, -height*center_y, 0.0f)), model);
    model = HMM_MultiplyMat4(HMM_Rotate(rotation, HMM_Vec3(0.0f, 0.0f, 1.0f)), model);

    //Translate
    model = HMM_MultiplyMat4(HMM_Translate(HMM_Vec3(x, y, 0.0f)), model);

    hmm_mat4 *ortho = Renderer_get_ortho();
    glUniformMatrix4fv(sprite_data.shaders.projection_loc, 1, GL_FALSE, (GLfloat *)ortho);
    glUniformMatrix4fv(sprite_data.shaders.model_loc, 1, GL_FALSE, (GLfloat *)&model);

    glUniform4f(sprite_data.shaders.color_loc, color.r, color.g, color.b, color.a);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, spriteset->texture.id);
    glUniform1i(sprite_data.shaders.texture_loc, 0);
	
    glBindBuffer(GL_ARRAY_BUFFER, sprite_data.shaders.vertex_buffer);
    glEnableVertexAttribArray (sprite_data.shaders.vertex);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(sprite_data.vertex_array), sprite_data.vertex_array);
    glVertexAttribPointer(sprite_data.shaders.vertex, 2, GL_FLOAT, false, 4*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray (sprite_data.shaders.v_text_coord);
    glVertexAttribPointer(sprite_data.shaders.v_text_coord, 2, GL_FLOAT, false, 4*sizeof(float), (void*)(2*sizeof(float)));
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}


//Documented above
void
Draw_sprite(f32 x, f32 y, f32 width, f32 height, f32 rotation, Texture *texture, u32 sprite_number, hmm_vec4 rgba_color) {
    f32 sprite_width = texture->width;
    f32 sprite_height = texture->height;
    //--------  Calculate texture coordinates
    Calculate_texture_coordinates(sprite_data.vertex_array, texture, sprite_width, sprite_height, sprite_number);

    //---------  Draw sprite
    glUseProgram(sprite_data.shaders.program_id);

    //Scale
    hmm_mat4 model = HMM_Scale(HMM_Vec3(width, height, 1.0f));

    //Rotate
    f32 h_width = (f32)width/2.0f;
    f32 h_height = (f32)height/2.0f;
    model = HMM_MultiplyMat4(HMM_Translate(HMM_Vec3(-h_width, -h_height, 0.0f)), model);
    model = HMM_MultiplyMat4(HMM_Rotate(rotation, HMM_Vec3(0.0f, 0.0f, 1.0f)), model);
    model = HMM_MultiplyMat4(HMM_Translate(HMM_Vec3(h_width, h_height, 0.0f)), model);

    //Translate
    model = HMM_MultiplyMat4(HMM_Translate(HMM_Vec3(x, y, 0.0f)), model);

    hmm_mat4 *ortho = Renderer_get_ortho();
    glUniformMatrix4fv(sprite_data.shaders.projection_loc, 1, GL_FALSE, (GLfloat *)ortho);
    glUniformMatrix4fv(sprite_data.shaders.model_loc, 1, GL_FALSE, (GLfloat *)&model);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture->id);
    glUniform1i(sprite_data.shaders.texture_loc, 0);

    glUniform4f(sprite_data.shaders.color_loc, rgba_color.R, rgba_color.G, rgba_color.B, rgba_color.A);
	
    glBindBuffer(GL_ARRAY_BUFFER, sprite_data.shaders.vertex_buffer);
    glEnableVertexAttribArray (sprite_data.shaders.vertex);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(sprite_data.vertex_array), sprite_data.vertex_array);
    glVertexAttribPointer(sprite_data.shaders.vertex, 2, GL_FLOAT, false, 4*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray (sprite_data.shaders.v_text_coord);
    glVertexAttribPointer(sprite_data.shaders.v_text_coord, 2, GL_FLOAT, false, 4*sizeof(float), (void*)(2*sizeof(float)));
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

f32
Normalize(f32 pos, f32 max) {
    return (pos / (f32)max) * 2 - 1;
}

f32
Flatten_idx(u32 x, u32 y, u32 width) {
    return x + y * width;
}


//Documented above
void 
Draw_tilemap(Tilemap *tilemap) {
	Assert(tilemap->render_data_generated == true, "Tilemap render data must be generated before render it");
    glUseProgram(tiles_data.program_id);

    glUniform1f(tiles_data.scroll_x_loc, tilemap->scroll_x / renderer_data.max_x);
    glUniform1f(tiles_data.scroll_y_loc, tilemap->scroll_y / renderer_data.max_y);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tilemap->spriteset->texture.id);
    glUniform1i(tiles_data.texture_loc, 0);

    glBindBuffer(GL_ARRAY_BUFFER, tilemap->vertex_buffer);
    glEnableVertexAttribArray (tiles_data.vertex_loc);
    glVertexAttribPointer(tiles_data.vertex_loc, 2, GL_FLOAT, false, 4*sizeof(f32), (void*)(0*sizeof(f32)));
    glEnableVertexAttribArray (tiles_data.v_text_coord);
    glVertexAttribPointer(tiles_data.v_text_coord, 2, GL_FLOAT, false, 4*sizeof(f32), (void*)(2*sizeof(f32)));

	glDrawArrays(GL_TRIANGLES, 0, tilemap->real_size * 6);
}

// Documented above
int
Make_spriteset(const char *image_filename, u32 sprite_width, u32 sprite_height, bool linear_interpolation, Spriteset *spriteset_result) {
	memset(spriteset_result, 0, sizeof(Spriteset));
	if (Setup_texture(image_filename, linear_interpolation, &spriteset_result->texture) != 0) {
		Free_spriteset(spriteset_result);
		return -1;
	}
	if (spriteset_result->texture.width % sprite_width != 0) {
		Free_spriteset(spriteset_result);
		return -1;
	}
	if (spriteset_result->texture.height % sprite_height != 0) {
		Free_spriteset(spriteset_result);
		return -1;
	}
	spriteset_result->sprite_width  = sprite_width;
	spriteset_result->sprite_height = sprite_height;
	return 0;
}

// Documented above
void
Free_spriteset(Spriteset *spriteset) {
	Free_texture(&spriteset->texture);
	memset(spriteset, 0, sizeof(Spriteset));
}




typedef struct {
    Vertex_object left_down, left_up, right_down, right_up;
} Square;


//Documented above
Tilemap
Make_tilemap(i32 init_x, i32 init_y, u32 width, u32 height, u32 n_layers, u32 tiles_x, u32 tiles_y) {
	Tilemap result  = {0};
    result.init_x   = init_x;
    result.init_y   = init_y;
    result.width    = width;
    result.height   = height;
	result.n_layers = n_layers;
    result.tiles_x  = tiles_x;
    result.tiles_y  = tiles_y;
    result.scroll_x = 0;
    result.scroll_y = 0;
    result.total_tilemap_width  = width  * tiles_x;
    result.total_tilemap_height = height * tiles_y;
	result.tilemap_data = Alloc(u32, n_layers * tiles_x * tiles_y);
	return result;
}

//Documented above
void
Tilemap_set_tile(Tilemap *tilemap, u32 layer, u32 x, u32 y, u32 tile) {
	Assert(layer < tilemap->n_layers, "%d < %d", layer, tilemap->n_layers);
	Assert(x     < tilemap->tiles_x,  "%d < %d", x, tilemap->tiles_x);
	Assert(y     < tilemap->tiles_y,  "%d < %d", y, tilemap->tiles_y);
	u32 w = tilemap->tiles_x;
	u32 h = tilemap->tiles_y;
	tilemap->tilemap_data[(layer * w * h) + (y * w) + x] = tile;
}

//Documented above
u32
Tilemap_get_tile(Tilemap *tilemap, u32 layer, u32 x, u32 y) {
	Assert(layer < tilemap->n_layers, "%d < %d", layer, tilemap->n_layers);
	Assert(x     < tilemap->tiles_x,  "%d < %d", x, tilemap->tiles_x);
	Assert(y     < tilemap->tiles_y,  "%d < %d", y, tilemap->tiles_y);
	u32 w = tilemap->tiles_x;
	u32 h = tilemap->tiles_y;
	return tilemap->tilemap_data[(layer * w * h) + (y * w) + x];
}

//Documented above
void
Tilemap_gen_render_data(Tilemap *tilemap, Spriteset *spriteset) {
	Assert(tilemap->render_data_generated == false, "The render data was already generated.");
	i32 init_x   = tilemap->init_x;
	i32 init_y   = tilemap->init_y;
    u32 width    = tilemap->width;
    u32 height   = tilemap->height;
	u32 tiles_x  = tilemap->tiles_x;
	u32 tiles_y  = tilemap->tiles_y;
	u32 n_layers = tilemap->n_layers;
	tilemap->render_data_generated = true;
    tilemap->vertex_data = Alloc(Square_vertex_data, n_layers*tiles_y*tiles_x);
	tilemap->spriteset     = spriteset;

	u32 vertex_squares_count = 0;

	for (u32 layer_i = 0; layer_i < n_layers; ++layer_i) {
		for (u32 y = 0; y < tiles_y; ++y) {
    	    for (u32 x = 0; x < tiles_x; ++x) {
    	        i32 px0 = init_x + x * width;
    	        i32 px1 = px0 + width;
    	        i32 py0 = init_y + y * height;
    	        i32 py1 = py0 + height;

    	        f32 nx0 = Normalize(px0, renderer_data.max_x); //Normalice coordinates between -1 and 1
    	        f32 nx1 = Normalize(px1, renderer_data.max_x); //Normalice coordinates between -1 and 1
    	        f32 ny0 = Normalize(py0, renderer_data.max_y);
    	        f32 ny1 = Normalize(py1, renderer_data.max_y);

				u32 tile_number = Tilemap_get_tile(tilemap, layer_i, x, y);

    	        if (-1 == tile_number) continue;

    	        // Calculate coordinates
    	        //     // pos      // tex
    	        // 0.0f, 0.0f, 0.0f, 1.0f,
    	        // 0.0f, 1.0f, 0.0f, 0.0f,
    	        // 1.0f, 0.0f, 1.0f, 1.0f,
    	        // 1.0f, 1.0f, 1.0f, 0.0f
    	        Square sq;
    	        sq.left_down.vx = nx0;
    	        sq.left_down.vy = ny0;
    	        
    	        sq.left_up.vx = nx0;
    	        sq.left_up.vy = ny1;

    	        sq.right_down.vx = nx1;
    	        sq.right_down.vy = ny0;

    	        sq.right_up.vx = nx1;
    	        sq.right_up.vy = ny1;

    	        Calculate_texture_coordinates(
					&sq.left_down.vx,
					&spriteset->texture,
					spriteset->sprite_width,
					spriteset->sprite_height,
					tile_number);

    	        Square_vertex_data *square = &tilemap->vertex_data[vertex_squares_count];
    	        square->triangle1.left_down = sq.left_down;
    	        square->triangle1.left_up = sq.left_up;
    	        square->triangle1.right_up = sq.right_up;

    	        square->triangle2.left_down = sq.left_down;
    	        square->triangle2.right_down = sq.right_down;
    	        square->triangle2.right_up = sq.right_up;

				++vertex_squares_count;
    	    }
    	}
	}

	tilemap->real_size = vertex_squares_count;
    //Create and bind vertex buffer
    glGenBuffers(1, &tilemap->vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, tilemap->vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Square_vertex_data) * vertex_squares_count, tilemap->vertex_data, GL_STATIC_DRAW);
}


//Documented above
u32
Get_tile_number(u32 tile_x, u32 tile_y, Tilemap *tilemap) {
    u32 idx = tile_x + tile_y * tilemap->tiles_x;
    Assert(idx < tilemap->tiles_x * tilemap->tiles_y, "ERROR: Requesting tile outside the tilemap range. Tile requested: %i", idx);
    return tilemap->tilemap_data[idx];
}


// Documented above
#include "third_party/parson.h"
Object*
Load_tilemap_from_tiled_json(const char *filename, i32 init_x, i32 init_y, 
    u32 screen_tile_width, u32 screen_tile_height, Spriteset *spriteset, 
    Tilemap *tilemap_result, u32 gameplay_height, u32 *objects_size) {
	const char *file_data = (const char *)Get_file_contentsz(filename, NULL);

	JSON_Value *root_value = json_parse_string(file_data);

	if(json_value_get_type(root_value) != JSONObject){
		Panic("Error, no valid file\n");
        return NULL;
	}

	JSON_Array *layers = json_object_get_array(json_object(root_value), "layers");

    u32 n_layers = (u32)json_array_get_count(layers);

    u32 tile_width = json_object_get_number(json_object(root_value), "tilewidth");
    u32 tile_height = json_object_get_number(json_object(root_value), "tileheight");

    if (screen_tile_height == 0 || screen_tile_width == 0) {
        screen_tile_width = tile_width;
        screen_tile_height = tile_height;
    }

    u32 tiles_x = (u32)json_object_get_number(json_object(root_value),  "width");
    u32 tiles_y  = (u32)json_object_get_number(json_object(root_value), "height");
	Tilemap tm = Make_tilemap(init_x, init_y, screen_tile_width, screen_tile_height, n_layers-1, tiles_x, tiles_y);

    Object *obj;

    for (u32 layer_i = 0; layer_i < n_layers; ++layer_i) {
        JSON_Object *json_tilemap      = json_array_get_object(layers, layer_i);
        const char *l_name = json_object_get_string(json_tilemap, "name");

        if (strcmp(l_name, "objetos") != 0) {
            JSON_Array  *json_tilemap_data = json_object_get_array(json_tilemap, "data");

            if(tiles_x * tiles_y != json_array_get_count(json_tilemap_data)) {
                json_value_free(root_value);
                Panic("Size of the tilemap does not match\n");
            }

            // The tilemap has the origin starting on the bottom left, we have to store it inverted
            for(size_t i =0; i < tiles_y; ++i){
                for(size_t j = 0; j < tiles_x; ++j) {
                    u32 y = (tiles_y-i-1);
                    u32 x = j;
                    u32 tile_number = (u32)json_array_get_number(json_tilemap_data, i*tiles_x+j)-1;
                    Tilemap_set_tile(&tm, layer_i, x, y, tile_number);
                }
            }
        }
        else {
            JSON_Array  *objects = json_object_get_array(json_tilemap, "objects");
            u32 n_objects = (u32)json_array_get_count(objects);
            *objects_size = n_objects;
            obj = Alloc(Object, n_objects);

            for (u32 o = 0; o < n_objects; o++) {
                JSON_Object *obj_json = json_array_get_object(objects, o);
                obj[o].x = json_object_get_number(obj_json, "x");
                obj[o].height = json_object_get_number(obj_json, "height");
                obj[o].y = gameplay_height - (json_object_get_number(obj_json, "y") + obj[o].height);
                obj[o].width = json_object_get_number(obj_json, "width");
                obj[o].rotation = json_object_get_number(obj_json, "rotation");

                const char *name = json_object_get_string(obj_json, "name");
                strcpy(obj[o].name, name);
                
                JSON_Array *props = json_object_get_array(obj_json, "properties");
                u32 p_count = json_array_get_count(props);
                obj[o].attributes = Alloc(Attribute, p_count);
                obj[o].attributes_size = p_count;
                for (u32 j = 0; j < p_count; j++) {
                    JSON_Object *pr_obj = json_array_get_object(props, j);
                    obj[o].attributes[j].name = json_object_get_string(pr_obj, "name");
                    obj[o].attributes[j].value = json_object_get_string(pr_obj, "value");
                }
            }
        }
    }

    json_value_free(root_value);

	Tilemap_gen_render_data(&tm, spriteset);

	*tilemap_result = tm;


	return obj;
}

//Documented above
Texture*
Get_texture_by_name(const char *nmb, Texture_set *set, u32 set_size) {
    if (nmb == NULL && strcmp(nmb, "") == 0) {
        Panic("Texture name is null or empty\n");
    }
    for (u32 i = 0; i < set_size; i++) {
        if (strcmp(nmb, set[i].name) == 0) {
            return &set[i].texture;
        }
    }
    fprintf(stderr, "ERROR: Texture with name %s not found!\n", nmb);
    return NULL;
}


//Documented above
Object*
Load_objects_from_tiled_json(const char *filename, u32 gameplay_height, u32 *objects_size) {
    Object *obj;

    const char *file_data = (const char *)Get_file_contentsz(filename, NULL);
	JSON_Value *root_value = json_parse_string(file_data);
	if(json_value_get_type(root_value) != JSONObject){
		fprintf(stderr, "Error, no valid file\n");
		return NULL;
	}

	JSON_Array *layers = json_object_get_array(json_object(root_value), "layers");
    JSON_Object *layer      = json_array_get_object(layers, 0);
    JSON_Array  *objects = json_object_get_array(layer, "objects");
    u32 n_objects = (u32)json_array_get_count(objects);
    *objects_size = n_objects;
    obj = Alloc(Object, n_objects);

    for (u32 i = 0; i < n_objects; i++) {
        JSON_Object *obj_json = json_array_get_object(objects, i);
        obj[i].x = json_object_get_number(obj_json, "x");
        obj[i].height = json_object_get_number(obj_json, "height");
        obj[i].y = gameplay_height - (json_object_get_number(obj_json, "y") + obj[i].height);
        obj[i].width = json_object_get_number(obj_json, "width");
        obj[i].rotation = json_object_get_number(obj_json, "rotation");

        const char *name = json_object_get_string(obj_json, "name");
        strcpy(obj[i].name, name);
        // if (name != NULL && strcmp(name, "") != 0) {
        //     obj[i].texture = Get_texture_by_name(name, set, set_size);
        // }
        
        JSON_Array *props = json_object_get_array(obj_json, "properties");
        u32 p_count = json_array_get_count(props);
        obj[i].attributes = Alloc(Attribute, p_count);
        obj[i].attributes_size = p_count;
        for (u32 j = 0; j < p_count; j++) {
            JSON_Object *pr_obj = json_array_get_object(props, j);
            obj[i].attributes[j].name = json_object_get_string(pr_obj, "name");
            obj[i].attributes[j].value = json_object_get_string(pr_obj, "value");
        }
    }

    return obj;
}


//Documented above
void
Free_objects(Object *obj, u32 objects_size) {
    if (obj != NULL) {
        #if !defined(BUNDLED_TREE)
            for (u32 i = 0; i < objects_size; i++) {
                free(obj[i].attributes);
            }
            free(obj);
        #endif
        obj = NULL;
    }
}


//Documented above
void
Scroll_tilemap(f32 x, f32 y, Tilemap* tilemap) {
    tilemap->scroll_x += x;
    tilemap->scroll_y += y;
}


//Documented above
void
Free_tilemap(Tilemap *tilemap) {
    free(tilemap->tilemap_data);
	if (tilemap->render_data_generated) {
		glDeleteBuffers(1, &tilemap->vertex_buffer);
        #if !defined(BUNDLED_TREE)
		    free(tilemap->vertex_data);
        #endif
	}
	*tilemap = (Tilemap){0};
}


//Documented above
void Sprite_cleanup() {
	glDeleteProgram(sprite_data.shaders.program_id);
	glDeleteProgram(tiles_data.program_id);

    glDeleteBuffers(1, &sprite_data.shaders.vertex_buffer);
}


#endif //_SPRITE_H_
