
#ifndef _BASIC_GL_
#define _BASIC_GL_

#include "base.h"
#include "third_party/HandmadeMath.h"


/*
* Make_shader
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Makes a shader from the file path passed, the type of the shader is specified
* on the last parameter.
*
* If the shader has any error, it will be printed and the function will return
* != of 0
*
* VARIABLES
* -------------------------------------------------------------------
* shader_result: The shader id generated it cannot be NULL
* shader_file: Path of the file
* shader_type: If is GL_VERTEX_SHADER or GL_FRAGMENT_SHADER
*
* RETURNS
* -------------------------------------------------------------------
* int: 0 on success, < 0 on failure
*
*/
static int
Make_shader(GLuint *shader_result, const char *shader_file, GLenum shader_type);


/*
* Make_program
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Makes a shader program receiving a vertex shader filepath and a fragment shader filepath
* If any shader has any error, it will be printed and the function will return
* != of 0
*
* VARIABLES
* -------------------------------------------------------------------
* program: The shader program generated it cannot be NULL
* vertex_shader_file: Path of the vertex shader file
* fragment_shader_file: Path of the vertex shader file
*
* RETURNS
* -------------------------------------------------------------------
* int: 0 on success, < 0 on failure
*
*/
static int
Make_program(GLuint *program, const char *vertex_shader_file, const char *fragment_shader_file);


/*
* Renderer_setup
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Setups the basic renderer, it will allow you to draw various types of figures,
* moreover, is possible that other kinds of renderer are dependant of this.
*
* To clean the resources Renderer_cleanup has to be called.
*
* VARIABLES
* -------------------------------------------------------------------
* max_world_x: Maximum world coordinate value for x axis.
* max_world_y: Maximum world coordinate value for y axis.
*
* RETURNS
* -------------------------------------------------------------------
* int: 0 on success, < 0 on failure
*
*/
int
Renderer_setup(u32 max_world_x, u32 max_world_y);



/*
* Renderer_cleanup
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Cleans all the resources used by the renderer.
*
*/
void
Renderer_cleanup();



/*
* Draw_quad
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Draws a colored quad, it can be degenerated.
*
* Note that the quad is buffered in the triangles buffer, you must call
* to Flush_triangles to guarantee that is being drawed
*
* VARIABLES
* -------------------------------------------------------------------
* v0, v1, v2, v3: Each vertex
* color: The color
*
*/
void
Draw_quad(hmm_vec3 v0, hmm_vec3 v1, hmm_vec3 v2, hmm_vec3 v3, hmm_vec4 color);



/*
* Draw_rect
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Draws a rectangle internally using Draw_quad.
*
* Note that the quad is buffered in the triangles buffer, you must call
* to Flush_triangles to guarantee that is being drawed
*
* VARIABLES
* -------------------------------------------------------------------
* pos: position of the bottom left corner
* width, height: the dimensions
* color: The color
*
*/
void
Draw_rect(hmm_vec3 pos, f32 width, f32 height, hmm_vec4 color);




/*
* Draw_triangle
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Draws a colored triangle.

 * Note that the triangle is buffered in the triangles buffer, you must call
* to Flush_triangles to guarantee that is being drawed
*
* VARIABLES
* -------------------------------------------------------------------
* v0, v1, v2: Each vertex
* color: The color
*
*/
void
Draw_triangle(hmm_vec3 v0, hmm_vec3 v1, hmm_vec3 v2, hmm_vec4 color);


/*
* Draw_circle
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Draw all buffered triangles.
*
*/
void
Flush_triangles();



/*
* Draw_circle
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Draws a circle, its just a quad but without color on the corners
* using a special shader.
*
* This function doesn't draw the circle inmediately, it is pushed to a
* buffer and when the buffer is full it will draw all circles at once.
*
* You must call Flush_circles to draw all remaining circles.
*
* VARIABLES
* -------------------------------------------------------------------
* v0, v1, v2, v3: Each vertex
* color: The color
*
*/
void
Draw_circle(hmm_vec3 v0, hmm_vec3 v1, hmm_vec3 v2, hmm_vec3 v3, hmm_vec4 color);


/*
* Draw_circle_from_center
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Draws a circle, its just a quad but without color on the corners
* using a special shader. It uses a point and a radius to define 
* the circle
*
* This function doesn't draw the circle inmediately, it is pushed to a
* buffer and when the buffer is full it will draw all circles at once.
*
* You must call Flush_circles to draw all remaining circles.
*
* VARIABLES
* -------------------------------------------------------------------
* x, y : position
* radius: radius of the circle
* color: The color
*
*/
void
Draw_circle_from_center(f32 x, f32 y, f32 radius, hmm_vec4 color);

/*
* Draw_circle
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Draw all buffered circles.
*
*/
void
Flush_circles();



/*
* DESCRIPTIION
*   Sets the max world coordinates for x and y axes. Creates an orthographic matrix 
*   4x4 to apply a over all vertices.
*
* VARIABLES
*   x_max: Max value of x axis.
*   y_max: Max value of y axis.
*/
void
Renderer_set_max_world_coordinates(u32 x_max, u32 y_max);

/*
* Renderer_get_ortho
* =============================================================
*
* DESCRIPTIION
* --------------------------------------------------------------
* Gets the current orthographic matrix
*
* RETURNS
* -------------------------------------------------------------------
* hmm_mat4*: Pointer to the orthographic matrix
*
*/
hmm_mat4*
Renderer_get_ortho();





///////////////////////////////////////////////////////////////////////////////////////
//
//
//                              IMPLEMENTATION STARTS
//
//
///////////////////////////////////////////////////////////////////////////////////////

// Makes a shader from a filename (Documented above)
static int
Make_shader(GLuint *shader_result, const char *shader_file, GLenum shader_type) {
	u32 size;
	const char *shader_source = (char *)Get_file_contentsz(shader_file, &size);
	if (shader_source == NULL) {
		fprintf(stderr, "Failed to load shader: %s\n", shader_file);
		return -1;
	}

	GLuint shader = glCreateShader(shader_type);
	glShaderSource(shader, 1, &shader_source, NULL);
	Free_file_contents((u8 *)shader_source); // We can free the source code here
	glCompileShader(shader);
	GLint shader_compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_compiled);
	if (shader_compiled != GL_TRUE){
		GLsizei log_length = 0;
		static GLchar message[1024];
		glGetShaderInfoLog(shader, 1024, &log_length, message);
		fprintf(stderr, "Shader '%s' compilation error:\n%.*s\n", shader_file, log_length, message);
		return -1;
	}

	*shader_result = shader;
	return 0;
}


// Makes a shader program from 2 shader filenames (Documented above)
static int
Make_program(GLuint *program, const char *vertex_shader_file, const char *fragment_shader_file) {
	GLuint vertex_shader;
	if (Make_shader(&vertex_shader, vertex_shader_file, GL_VERTEX_SHADER) != 0) {
		return -1;
	}
	GLuint fragment_shader;
	if (Make_shader(&fragment_shader, fragment_shader_file, GL_FRAGMENT_SHADER) != 0) {
		return -1;
	}

	GLuint shader_program  = glCreateProgram();      // create program object
	glAttachShader(shader_program, vertex_shader);   // and attach both...
	glAttachShader(shader_program, fragment_shader); // ... shaders to it

	glLinkProgram(shader_program);   // link the program

	// We can delete de shaders here
	glDetachShader(shader_program, vertex_shader);
	glDeleteShader(vertex_shader);
	glDetachShader(shader_program, fragment_shader);
	glDeleteShader(fragment_shader);

	*program = shader_program;
	return 0;
}


#define MAX_BUFFER_TRIANGLES 256
//                           xy    RGBA    N Verices
#define COLOR_TRIANGLE_SIZE ((2   +  4)   *   3)

#define MAX_BUFFER_CIRCLES 256
//                           xy    RGBA   corners   N Verices
#define COLOR_CIRCLE_SIZE   ((2   +  4   +  2)   *   6)

#define UNDEFINED_MAX_WORLD_AXIS 0
// 
// All the internal data used by the renderer
//
static struct {
    hmm_mat4 ortho; // Current orthographic matrix. Non transposed

	GLuint color_triangles_VBO;
	f32 triangles_buffer[MAX_BUFFER_TRIANGLES * COLOR_TRIANGLE_SIZE];
	u32 triangles_buffer_count;

	GLuint color_circles_VBO;
	f32 circles_buffer[MAX_BUFFER_CIRCLES * COLOR_CIRCLE_SIZE];
	u32 circles_buffer_count;

	// Shader used to draw colored quads and triangles
	struct {
		GLuint id;
		GLint vertex;
		GLint vertex_color;
		GLint vmat;
	} color_shader;

	// Shader used to draw colored circles
	struct {
		GLuint id;
		GLint  vertex;
		GLint  vertex_color;
		GLint  corner;
		GLint  vmat;
	} circle_shader;

    u32 max_x;
    u32 max_y;
    

} renderer_data = {
    .max_x = UNDEFINED_MAX_WORLD_AXIS,
    .max_y = UNDEFINED_MAX_WORLD_AXIS
};



// Documented above
int
Renderer_setup(u32 max_world_x, u32 max_world_y) {

	// Setup the VBOs

	glGenBuffers(1, &renderer_data.color_triangles_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, renderer_data.color_triangles_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(renderer_data.triangles_buffer), NULL, GL_DYNAMIC_DRAW);

	glGenBuffers(1, &renderer_data.color_circles_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, renderer_data.color_circles_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(renderer_data.circles_buffer), NULL, GL_DYNAMIC_DRAW);


	//
	//
	// ALL THIS FOR COLORED QUADS AND TRIANGLES
	//
	//

	if (Make_program(
			&renderer_data.color_shader.id,
			"assets/default/shaders/color_vertex.glsl",
			"assets/default/shaders/color_fragment.glsl") != 0) {
		
		Renderer_cleanup();
		return -1;
	}

	glUseProgram(renderer_data.color_shader.id);

	// Vertex location
	renderer_data.color_shader.vertex = glGetAttribLocation(renderer_data.color_shader.id, "vertex");
	if (renderer_data.color_shader.vertex < 0) {
		fprintf(stderr, "Unable to get vertex attrib location\n");
		Renderer_cleanup();
		return -1;
	}

	// Color location
	renderer_data.color_shader.vertex_color = glGetAttribLocation(renderer_data.color_shader.id, "vertex_color");
	if (renderer_data.color_shader.vertex_color < 0) {
		fprintf(stderr, "Unable to get vertex_color attrib location\n");
		Renderer_cleanup();
		return -1;
	}

	// Vertex matrix location
	renderer_data.color_shader.vmat = glGetUniformLocation(renderer_data.color_shader.id, "vmat");
	if (renderer_data.color_shader.id < 0) {
		fprintf(stderr, "Unable to get vmat uniform location\n");
		Renderer_cleanup();
		return -1;
	}

	
	//
	//
	// ALL THIS FOR COLORED CIRCLES
	//
	//

	if (Make_program(
			&renderer_data.circle_shader.id,
			"assets/default/shaders/color_circle_vertex.glsl",
			"assets/default/shaders/color_circle_fragment.glsl") != 0) {
		
		Renderer_cleanup();
		return -1;
	}

	glUseProgram(renderer_data.circle_shader.id);

	// Vertex location
	renderer_data.circle_shader.vertex = glGetAttribLocation(renderer_data.circle_shader.id, "vertex");
	if (renderer_data.circle_shader.vertex < 0) {
		fprintf(stderr, "Unable to get vertex attrib location\n");
		Renderer_cleanup();
		return -1;
	}

	// Corner location
	renderer_data.circle_shader.corner = glGetAttribLocation(renderer_data.circle_shader.id, "corner");
	if (renderer_data.circle_shader.corner < 0) {
		fprintf(stderr, "Unable to get corner attrib location\n");
		Renderer_cleanup();
		return -1;
	}

	// Color location
	renderer_data.circle_shader.vertex_color = glGetAttribLocation(renderer_data.circle_shader.id, "vertex_color");
	if (renderer_data.circle_shader.vertex_color < 0) {
		fprintf(stderr, "Unable to get color_vertex attrib location\n");
		Renderer_cleanup();
		return -1;
	}

	// Vertex matrix location
	renderer_data.circle_shader.vmat = glGetUniformLocation(renderer_data.circle_shader.id, "vmat");
	if (renderer_data.circle_shader.vmat < 0) {
		fprintf(stderr, "Unable to get vmat uniform location\n");
		Renderer_cleanup();
		return -1;
	}

    Renderer_set_max_world_coordinates(max_world_x, max_world_y);

	return 0;

}

// Documented above
void
Renderer_cleanup() {
	// Clean the shader program
	glDeleteProgram(renderer_data.color_shader.id);
	glDeleteProgram(renderer_data.circle_shader.id);
	// Clean VBO
	glDeleteBuffers(1, &renderer_data.color_triangles_VBO);
	glDeleteBuffers(1, &renderer_data.color_circles_VBO);
	memset(&renderer_data, 0, sizeof(renderer_data));
}



// Documented above
void
Flush_triangles() {
	glUseProgram(renderer_data.color_shader.id);

	glBindBuffer(GL_ARRAY_BUFFER, renderer_data.color_triangles_VBO);
	glBufferSubData(
		GL_ARRAY_BUFFER,
		0,
		sizeof(f32) * COLOR_TRIANGLE_SIZE * renderer_data.triangles_buffer_count,
		renderer_data.triangles_buffer);

    glEnableVertexAttribArray(renderer_data.color_shader.vertex);
	glVertexAttribPointer(renderer_data.color_shader.vertex, 2, GL_FLOAT, false, 6*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(renderer_data.color_shader.vertex_color);
	glVertexAttribPointer(renderer_data.color_shader.vertex_color, 4, GL_FLOAT, false, 6*sizeof(float), (void*)(2*sizeof(float)));

	glDrawArrays(GL_TRIANGLES, 0, renderer_data.triangles_buffer_count*3);
	renderer_data.triangles_buffer_count = 0;
}


// Documented above
void
Draw_triangle(hmm_vec3 v0, hmm_vec3 v1, hmm_vec3 v2, hmm_vec4 color) {

	if (renderer_data.triangles_buffer_count == MAX_BUFFER_TRIANGLES) {
		Flush_triangles();
	}

	f32 *data = &renderer_data.triangles_buffer[COLOR_TRIANGLE_SIZE * renderer_data.triangles_buffer_count];

	data[0] = v0.X; data[1] = v0.Y;
	data[2] = color.R; data[3] = color.G; data[4] = color.B; data[5] = color.A;
	data[6] = v1.X; data[7] = v1.Y;
	data[8] = color.R; data[9] = color.G; data[10] = color.B; data[11] = color.A;
	data[12] = v2.X; data[13] = v2.Y;
	data[14] = color.R; data[15] = color.G; data[16] = color.B; data[17] = color.A;

	++renderer_data.triangles_buffer_count;
}

// Documented above
void
Draw_quad(hmm_vec3 v0, hmm_vec3 v1, hmm_vec3 v2, hmm_vec3 v3, hmm_vec4 color) {
	Draw_triangle(v0, v1, v2, color);
	Draw_triangle(v0, v2, v3, color);
}


// Documented above
void
Draw_rect(hmm_vec3 pos, f32 width, f32 height, hmm_vec4 color) {
	Draw_quad(
		pos,
		HMM_Vec3(pos.X + width, pos.Y, pos.Z),
		HMM_Vec3(pos.X + width, pos.Y + height, pos.Z),
		HMM_Vec3(pos.X, pos.Y + height, pos.Z),
		color);
}

// Documented above
void
Flush_circles() {
	glUseProgram(renderer_data.circle_shader.id);

	glBindBuffer(GL_ARRAY_BUFFER, renderer_data.color_circles_VBO);
	glBufferSubData(
		GL_ARRAY_BUFFER,
		0,
		sizeof(f32) * COLOR_CIRCLE_SIZE * renderer_data.circles_buffer_count,
		renderer_data.circles_buffer);
    glEnableVertexAttribArray(renderer_data.circle_shader.vertex);
	glVertexAttribPointer(renderer_data.circle_shader.vertex, 2, GL_FLOAT, false, 8*sizeof(float), (void*)(0));
    glEnableVertexAttribArray(renderer_data.circle_shader.corner);
	glVertexAttribPointer(renderer_data.circle_shader.corner, 2, GL_FLOAT, false, 8*sizeof(float), (void*)(2*sizeof(f32)));
    glEnableVertexAttribArray(renderer_data.circle_shader.vertex_color);
	glVertexAttribPointer(renderer_data.circle_shader.vertex_color, 4, GL_FLOAT, false, 8*sizeof(float), (void*)(4*sizeof(f32)));

	glDrawArrays(GL_TRIANGLES, 0, renderer_data.circles_buffer_count*6);

	renderer_data.circles_buffer_count = 0;
}


// Documented above
void
Draw_circle(hmm_vec3 v0, hmm_vec3 v1, hmm_vec3 v2, hmm_vec3 v3, hmm_vec4 color) {

	if (renderer_data.circles_buffer_count == MAX_BUFFER_CIRCLES) {
		Flush_circles();
	}

	f32 *data = &renderer_data.circles_buffer[COLOR_CIRCLE_SIZE * renderer_data.circles_buffer_count];

	data[0] = v0.X;    ++data; data[0]= v0.Y;     ++data; data[0] = -1.0f;   ++data; data[0] = -1.0f;   ++data; 
	data[0] = color.R; ++data; data[0]= color.G;  ++data; data[0] = color.B; ++data; data[0] = color.A; ++data; 
	data[0] = v1.X;    ++data; data[0]= v1.Y;     ++data; data[0] =  1.0f;   ++data; data[0] = -1.0f;   ++data; 
	data[0] = color.R; ++data; data[0] = color.G; ++data; data[0] = color.B; ++data; data[0] = color.A; ++data; 
	data[0] = v2.X;    ++data; data[0] = v2.Y;    ++data; data[0] =  1.0f;   ++data; data[0] =  1.0f;   ++data; 
	data[0] = color.R; ++data; data[0] = color.G; ++data; data[0] = color.B; ++data; data[0] = color.A; ++data; 
	data[0] = v0.X;    ++data; data[0]= v0.Y;     ++data; data[0] = -1.0f;   ++data; data[0] = -1.0f;   ++data; 
	data[0] = color.R; ++data; data[0]= color.G;  ++data; data[0] = color.B; ++data; data[0] = color.A; ++data; 
	data[0] = v2.X;    ++data; data[0]= v2.Y;     ++data; data[0] =  1.0f;   ++data; data[0] =  1.0f;   ++data; 
	data[0] = color.R; ++data; data[0] = color.G; ++data; data[0] = color.B; ++data; data[0] = color.A; ++data; 
	data[0] = v3.X;    ++data; data[0] = v3.Y;    ++data; data[0] = -1.0f;   ++data; data[0] =  1.0f;   ++data; 
	data[0] = color.R; ++data; data[0] = color.G; ++data; data[0] = color.B; ++data; data[0] = color.A; ++data; 


	++renderer_data.circles_buffer_count;
}


void
Draw_circle_from_center(f32 x, f32 y, f32 radius, hmm_vec4 color) {
    Draw_circle(
        HMM_Vec3(-1.0f * radius + x, -1.0f * radius + y, 0.0f),
        HMM_Vec3( 1.0f * radius + x, -1.0f * radius + y, 0.0f),
        HMM_Vec3( 1.0f * radius + x,  1.0f * radius + y, 0.0f),
        HMM_Vec3(-1.0f * radius + x,  1.0f * radius + y, 0.0f),

        color
    );
}


//Documented above
void
Renderer_set_max_world_coordinates(u32 x_max, u32 y_max) {
    renderer_data.max_x = x_max;
    renderer_data.max_y = y_max;

    renderer_data.ortho = HMM_Orthographic(0, x_max, 0, y_max, -1, 1);
    
    hmm_mat4 transposed_mat = HMM_Transpose(renderer_data.ortho); // Opengl wants the hmm matrix transposed

	glUseProgram(renderer_data.color_shader.id);
    glUniformMatrix4fv(renderer_data.color_shader.vmat, 1, GL_FALSE, (GLfloat *)&transposed_mat);

	glUseProgram(renderer_data.circle_shader.id);
    glUniformMatrix4fv(renderer_data.circle_shader.vmat, 1, GL_FALSE, (GLfloat *)&transposed_mat);
}


//Documented above
hmm_mat4*
Renderer_get_ortho() {
    return &renderer_data.ortho;
}


#endif // _BASIC_GL_


