#version 100

varying mediump vec2 pixel_pos;
varying mediump vec4 pixel_color;

void main()
{
    mediump float dist = sqrt(dot(pixel_pos, pixel_pos));
    if (dist > 1.0) {
        discard;
    }
    gl_FragColor = pixel_color;
}

