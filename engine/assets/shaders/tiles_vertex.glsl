#version 100

attribute vec2 vertex;
attribute vec2 v_text_coord;

varying mediump vec2 f_text_coord;

uniform float scroll_x;
uniform float scroll_y;

void main()
{
    gl_Position = vec4(vertex.x + scroll_x, vertex.y + scroll_y, 0.0, 1.0);
    f_text_coord = v_text_coord;
}
