#version 100
attribute vec3 vertex;
attribute vec2 v_tex_coord;
uniform mat4 vmat;
uniform mat4 tex_mat;
uniform mediump vec4 color;
varying vec2 f_tex_coord;


void main()
{
    gl_Position = vec4(vertex, 1.0) * vmat;
	vec4 tex_coord_v4 = vec4(v_tex_coord, 1.0, 1.0) * tex_mat;
    f_tex_coord = tex_coord_v4.xy;
}


