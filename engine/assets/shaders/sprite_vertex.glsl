#version 100

attribute vec2 vertex;
attribute vec2 v_text_coord;

uniform mat4 projection;
uniform mat4 model;

varying mediump vec2 f_text_coord;

void main()
{
    mediump vec4 pos = projection * model * vec4(vertex.x, vertex.y, 0.0, 1.0);
    gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);
    f_text_coord = v_text_coord;
}
