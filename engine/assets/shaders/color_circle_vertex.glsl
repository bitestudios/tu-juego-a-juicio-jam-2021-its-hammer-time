#version 100

attribute vec2 vertex;
attribute vec4 vertex_color;
attribute vec2 corner;
uniform mat4 vmat;
varying vec4 pixel_color;
varying vec2 pixel_pos;

void main() {
    pixel_pos = corner;
	pixel_color = vertex_color;
    gl_Position = vec4(vertex, 0.0, 1.0) * vmat;
}

