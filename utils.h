#ifndef UTILS_H
#define UTILS_H

#include "engine/base.h"
#include "game.h"

void
Calculate_viewport(u32 w_width, u32 w_height, f32 *vpx, f32 *vpy, f32 *vpw, f32 *vph);

void
Get_mouse_on_game_screen_coords(f32 *x_out, f32 *y_out);

bool
Triangles_overlap(Vec2 * t0, Vec2 * t1);


#endif // UTILS_H

