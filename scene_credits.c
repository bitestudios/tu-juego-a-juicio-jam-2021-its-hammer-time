

#include "scene_credits.h"

#define CREDITS_TOTAL_PEOPLE 4

const struct {
	const char *name;
	const char *rol;
	f32 x, y;
} credits[CREDITS_TOTAL_PEOPLE] = {
	{
		.name = "TANO",
		.rol = "PROGRAMMING",
		.x = GAMEPLAY_WIDTH * 0.3f,
		.y = GAMEPLAY_HEIGHT * 0.7f,
	},
	{
		.name = "TONET",
		.rol = "PROGRAMMING",
		.x = GAMEPLAY_WIDTH * 0.7f,
		.y = GAMEPLAY_HEIGHT * 0.7f,
	},
	{
		.name = "SALVA",
		.rol = "MUSIC AND ART",
		.x = GAMEPLAY_WIDTH * 0.3f,
		.y = GAMEPLAY_HEIGHT * 0.4f,
	},
	{
		.name = "CARLOS",
		.rol = "GAME DESIGN",
		.x = GAMEPLAY_WIDTH * 0.7f,
		.y = GAMEPLAY_HEIGHT * 0.4f,
	}
};

void
Scene_credits() {
	{// Draws the congratulations message
		const char text[] = "CREDITS";
		f32 text_h = 16.0f;
		f32 text_w = Get_text_width_with_height(&GLOBALS.font, text, text_h);
		f32 text_x = (GAMEPLAY_WIDTH - text_w) * 0.5f;
		f32 text_y = (GAMEPLAY_HEIGHT) * 0.85f;

		Draw_text_with_height(&GLOBALS.font, text, text_h, text_x+1.0f, text_y-1.0f, 1.0f, 0.0f, 0.0f, 1.0f);
		Draw_text_with_height(&GLOBALS.font, text, text_h, text_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);
	}

	for (u32 i = 0; i < CREDITS_TOTAL_PEOPLE; ++i) {
		{// Draws the name
			f32 text_h = 12.0f;
			f32 text_w = Get_text_width_with_height(&GLOBALS.font, credits[i].name, text_h);
			f32 text_x = (credits[i].x - (text_w * 0.5f));
			f32 text_y = credits[i].y;
			Draw_text_with_height(&GLOBALS.font, credits[i].name, text_h, text_x+0.5f, text_y-0.5f, 1.0f, 0.0f, 0.0f, 1.0f);
			Draw_text_with_height(&GLOBALS.font, credits[i].name, text_h, text_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);
		}
		{// Draws the rol
			f32 text_h = 10.0f;
			f32 text_w = Get_text_width_with_height(&GLOBALS.font, credits[i].rol, text_h);
			f32 text_x = (credits[i].x - (text_w * 0.5f));
			f32 text_y = credits[i].y-12.0f;
			Draw_text_with_height(&GLOBALS.font, credits[i].rol, text_h, text_x, text_y, 0.8f, 0.8f, 0.8f, 1.0f);
		}
	}
    MenuOption menu = Make_menu_option("BACK TO MAIN MENU", 0.15f);
	if (Show_menu(&menu, 1, 0, NULL)) {
		GLOBALS.current_scene = SCENE_MAIN_MENU;
	}
}




