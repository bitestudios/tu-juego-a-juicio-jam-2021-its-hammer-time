#include "scene_help.h"

void
Scene_help() {
	{
		const char text[] = "HELP";
		f32 text_h = 16.0f;
		f32 text_w = Get_text_width_with_height(&GLOBALS.font, text, text_h);
		f32 text_x = (GAMEPLAY_WIDTH - text_w) * 0.5f;
		f32 text_y = (GAMEPLAY_HEIGHT) * 0.85f;

		Draw_text_with_height(&GLOBALS.font, text, text_h, text_x+1.0f, text_y-1.0f, 1.0f, 0.0f, 0.0f, 1.0f);
		Draw_text_with_height(&GLOBALS.font, text, text_h, text_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);
	}
	{
		const char text[] = "SMASH ALL THE ENEMIES";
		f32 text_h = 10.0f;
		f32 text_w = Get_text_width_with_height(&GLOBALS.font, text, text_h);
		f32 text_x = (GAMEPLAY_WIDTH - text_w) * 0.5f;
		f32 text_y = (GAMEPLAY_HEIGHT) * 0.75f;
		Draw_text_with_height(&GLOBALS.font, text, text_h, text_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);
	}
	{
		const char text[] = "WARNING!";
		f32 text_h = 12.0f;
		f32 text_w = Get_text_width_with_height(&GLOBALS.font, text, text_h);
		f32 text_x = (GAMEPLAY_WIDTH - text_w) * 0.5f;
		f32 text_y = (GAMEPLAY_HEIGHT) * 0.65f;
		Draw_text_with_height(&GLOBALS.font, text, text_h, text_x, text_y, 1.0f, 0.0f, 0.0f, 1.0f);
	}
	{
		const char text[] = "YOUR HAMMER MAY BECOME TOO LONG";
		f32 text_h = 10.0f;
		f32 text_w = Get_text_width_with_height(&GLOBALS.font, text, text_h);
		f32 text_x = (GAMEPLAY_WIDTH - text_w) * 0.5f;
		f32 text_y = (GAMEPLAY_HEIGHT) * 0.55f;
		Draw_text_with_height(&GLOBALS.font, text, text_h, text_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);
	}

	{ // Move info
		f32 sprite_w = 54.0f;
		f32 sprite_h = 36.0f;
		f32 sprite_x = GAMEPLAY_WIDTH  * 0.6f;
		f32 sprite_y = GAMEPLAY_HEIGHT * 0.25f;
		Draw_sprite_ex(sprite_x, sprite_y, sprite_w, sprite_h, 0.0f, 0.0f, 0.0f, &GLOBALS.arrows_sprite, 0, V4(1.0f, 1.0f, 1.0f, 1.0f));

		{ // Left
			const char text[] = "LEFT";
			f32 text_h = 8.0f;
			f32 text_w = Get_text_width_with_height(&GLOBALS.font, text, text_h);
			f32 text_x = sprite_x - text_w;
			f32 text_y = sprite_y + 7.0f;
			Draw_text_with_height(&GLOBALS.font, text, text_h, text_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);
		}
		{ // Right
			const char text[] = "RIGHT";
			f32 text_h = 8.0f;
			//f32 text_w = Get_text_width_with_height(&GLOBALS.font, text, text_h);
			f32 text_x = sprite_x + sprite_w;
			f32 text_y = sprite_y + 7.0f;
			Draw_text_with_height(&GLOBALS.font, text, text_h, text_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);
		}
		{ // Right
			const char text[] = "JUMP";
			f32 text_h = 8.0f;
			f32 text_w = Get_text_width_with_height(&GLOBALS.font, text, text_h);
			f32 text_x = (sprite_x + sprite_w * 0.5f) - (text_w * 0.5f);
			f32 text_y = sprite_y + sprite_h;
			Draw_text_with_height(&GLOBALS.font, text, text_h, text_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

	{ // Smash info
		f32 sprite_w = 55.0f;
		f32 sprite_h = 17.0f;
		f32 sprite_x = GAMEPLAY_WIDTH  * 0.2f;
		f32 sprite_y = GAMEPLAY_HEIGHT * 0.25f;
		Draw_sprite_ex(sprite_x, sprite_y, sprite_w, sprite_h, 0.0f, 0.0f, 0.0f, &GLOBALS.space_sprite, 0, V4(1.0f, 1.0f, 1.0f, 1.0f));

		{ // Right
			const char text[] = "SMASH";
			f32 text_h = 8.0f;
			f32 text_w = Get_text_width_with_height(&GLOBALS.font, text, text_h);
			f32 text_x = (sprite_x + sprite_w * 0.5f) - (text_w * 0.5f);
			f32 text_y = sprite_y + sprite_h + 5;
			Draw_text_with_height(&GLOBALS.font, text, text_h, text_x, text_y, 1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

    MenuOption menu = Make_menu_option("BACK TO MAIN MENU", 0.10f);
	if (Show_menu(&menu, 1, 0, NULL)) {
		GLOBALS.current_scene = SCENE_MAIN_MENU;
	}
}

