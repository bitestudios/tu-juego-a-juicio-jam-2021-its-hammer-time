#include "hud.h"

void
Show_hud(f32 dt) {

	// All this to draw the lives
	{
		u32 max_lives = MAX_LIVES;
		f32 heart_width      = HUD_HEART_WIDTH;
		f32 heart_height     = HUD_HEART_HEIGHT;
		f32 heart_max_width  = HUD_HEART_MAX_WIDTH;
		f32 heart_max_height = HUD_HEART_MAX_HEIGHT;
		f32 x_cursor = HUD_HEARTS_PAD;
		f32 y_cursor = GAMEPLAY_HEIGHT-heart_height-HUD_HEARTS_PAD;
		for (u32 i = 0; i < max_lives; ++i) {
    	    Draw_sprite_ex(
    	        x_cursor, 
    	        y_cursor, 
    	        heart_width, 
    	        heart_height, 
    	        0.0f,
    	        0.0f, 0.0f, 
    	        &GLOBALS.hearts_sprites, 0, V4(1.0f,1.0f,1.0f,1.0f));

			if (i < GLOBALS.player.lives) {
				Draw_sprite_ex(
    	    	    x_cursor,
					y_cursor, 
    	    	    heart_width, 
    	    	    heart_height, 
    	    	    0.0f,
    	    	    0.0f, 0.0f, 
    	    	    &GLOBALS.hearts_sprites, 1, V4(1.0f,0.0f,0.0f,1.0f));
			}

			if (i == GLOBALS.player.lives && GLOBALS.hud.lost_live_time > 0.0f) {
				f32 lerp_factor = (GLOBALS.hud.lost_live_time / HUD_LOST_LIVE_TIME);
				f32 w = Lerp(heart_max_width, heart_width, lerp_factor);
				f32 h = Lerp(heart_max_height, heart_height, lerp_factor);
				f32 x = x_cursor + heart_width * 0.5f;
				f32 y = y_cursor + heart_height * 0.5f;
				Draw_sprite_ex(
    	    	    x,
					y, 
    	    	    w, 
    	    	    h, 
    	    	    0.0f,
    	    	    0.5f, 0.5f, 
    	    	    &GLOBALS.hearts_sprites, 1, V4(1.0f,0.0f,0.0f,lerp_factor));

				GLOBALS.hud.lost_live_time -= dt;
			}

			x_cursor += (heart_width + HUD_HEARTS_PAD);
		}
	}

	// All this to draw the enemy counter
	{
		f32 HUD_ENEMY_COUNTER_HEIGH = 16.0f;
		u32 total_level_enemies  = GLOBALS.levels[GLOBALS.current_level].enemies_to_spawn;
		u32 level_enemies_killed = GLOBALS.enemies_killed;
		char text[20];
		snprintf(text, 20, "%02d/%02d", level_enemies_killed, total_level_enemies);
		f32 text_width = Get_text_width_with_height(&GLOBALS.font, text, HUD_ENEMY_COUNTER_HEIGH);
		f32 cursor_x = GAMEPLAY_WIDTH - text_width - HUD_ENEMIES_COUNTER_PAD;
		f32 cursor_y = GAMEPLAY_HEIGHT - HUD_ENEMY_COUNTER_HEIGH - HUD_ENEMIES_COUNTER_PAD;
		Draw_text_with_height(&GLOBALS.font, text, HUD_ENEMY_COUNTER_HEIGH, cursor_x, cursor_y+3.0f, 1.0f, 1.0f, 1.0f, 1.0f);

		cursor_x = cursor_x - (HUD_ENEMIES_COUNTER_PAD * 0.5f) - ENEMY_PROPS[ENEMY_TYPE_BASIC].ENEMY_WIDTH;
        Draw_sprite_ex(
            cursor_x, 
            cursor_y, 
            16.0f, 
            16.0f, 
            0.0f,
            0.0f, 0.0f,
            &GLOBALS.enemy_icon, 0, V4(1.0f, 1.0f, 1.0f, 1.0f));

	}

}

