
#ifndef SCENE_GAME_H
#define SCENE_GAME_H

bool
Is_player_dead();

bool
Is_level_completed();

f32
Get_item_spawning_time_left(Item *item);

u32
Update_player_animation(f32 dt);

void
Update_player_physics();

void
Update_hammer_physics();

void
Draw_hammer();

void
Update_enemies_physics();

void
Draw_enemies(f32 dt);

void
Enemy_tilemap_collision();

void
Player_falls_off_the_map();

void
Scene_game();

void
Set_shake_screen(Vec2 vel, f32 min_x, f32 min_y, f32 max_x, f32 max_y, u32 bounces);

void
Shake_screen();

void
Init_level();

void
Add_particle_source(const ParticlesDesc *part_desc, f32 x, f32 y);

void
Render_gameplay(f32 dt);

#endif

